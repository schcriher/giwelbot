# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import sys
import enum
import html
import time
import random
import logging
import argparse
import datetime
import functools
import traceback

from telegram import ReplyKeyboardRemove
from telegram.ext import (Updater, Filters, ConversationHandler, run_async,
                          CallbackQueryHandler, CommandHandler, MessageHandler)
from telegram.error import TelegramError, ChatMigrated
from telegram.utils.helpers import mention_html

from giwel.debug import flogger
from giwel.tools import (GREETINGS_RE, ADMINS_RE, STARTS_WITH_SPACE, SPACES_RE,
                         change_seed, get_mention, get_token, get_keyboard,
                         sorted_alphabetically)
from giwel.config import CFG
from giwel.version import VERSION
from giwel.captcha import get_captcha
from giwel.context import Contextualizer
from giwel.control import URL_MAIL_RE, BAN_RULES, simplify_text, get_spam
from giwel.database import (CaptchaStatus, CaptchaLocation,
                            Admission, Captcha, Restriction, Expulsion,
                            clean_database, load_jobs, save_jobs)

logger = logging.getLogger(__name__)
context = Contextualizer()


class UserRestriction(enum.Enum):
    NONE = 0
    TEMP = 1
    FULL = 2


class PrivateStep(enum.IntEnum):
    STOP = ConversationHandler.END
    INIT = 0
    CHAT = 1


class DBDelete(enum.IntFlag):
    SANCTION = 1
    ADMISSION = 2
    RESTRICTION = 4
    EXPULSION = 8
    ADM_RES = ADMISSION | RESTRICTION
    SAN_ADM_RES = SANCTION | ADMISSION | RESTRICTION


KEYBOARD_COMMON = {
    'resize_keyboard': True,
    'one_time_keyboard': True,
}


LOG_MSG_UC = 'user=%d in chat=%d %s: %s'
LOG_MSG_C = 'chat=%d %s: %s'
LOG_MSG_U = 'user=%d %s: %s'
LOG_MSG_P = '[private] user=%d %s: %s'


# ----------------------------------- #
# Decorating functions


def check_cache(func):
    @functools.wraps(func)
    def check_memory_wrapper(ctx):
        if 'wrongs' in ctx.cache.get(ctx.uid, {}):
            return func(ctx)
        ctx.send(text=ctx.txt.WAS_AN_ERROR, reply_markup=None)
        return PrivateStep.STOP
    return check_memory_wrapper


def cancel_greet_group(func):
    @functools.wraps(func)
    def greeting_wrapper(ctx):
        if ctx.is_group:
            ctx.chat.prev_greet_users = None
            ctx.chat.prev_greet_message_id = None
            logger.debug(LOG_MSG_C, ctx.cid, 'group next greeting', False)
        return func(ctx)
    return greeting_wrapper


def only_admin(func):
    @functools.wraps(func)
    def only_admin_wrapper(ctx):
        if ctx.is_admin():
            return func(ctx)
        if func.__name__.endswith('_cb_handler'):
            ctx.answer(text=ctx.txt.ONLY_ADMIN)
        else:
            ctx.reply(text=ctx.txt.ONLY_ADMIN, reply_markup=None)
        return None
    return only_admin_wrapper


def retry_with_new_chat_id(func):
    @functools.wraps(func)
    def retry_with_new_chat_id_wrapper(bot, chat_id, *args):
        try:
            return func(bot, chat_id, *args)
        except ChatMigrated as migrated:
            new_id = migrated.new_chat_id
            # Migration is not done here because the context object (ctx)
            # would keep the old chat info
            logger.debug('group %s SHOULD be migrated to supergroup %s', chat_id, new_id)
            return func(bot, new_id, *args)
    return retry_with_new_chat_id_wrapper


# ----------------------------------- #
# Auxiliary functions


@flogger
@run_async
@retry_with_new_chat_id
def restrict_user(bot, chat_id, user_id, restriction, until=0):

    if restriction is UserRestriction.FULL:
        can_send_messages = False
        can_others = False

    elif restriction is UserRestriction.TEMP:
        can_send_messages = True
        can_others = False

    else:  # UserRestriction.NONE
        can_send_messages = True
        can_others = True

    parameters = {
        'chat_id': chat_id,
        'user_id': user_id,
        'until_date': until,
        'can_send_messages': can_send_messages,
        'can_send_media_messages': can_others,
        'can_send_other_messages': can_others,
        'can_add_web_page_previews': can_others,
    }
    try:
        result = bot.restrict_chat_member(**parameters)
    except ChatMigrated:
        raise
    except TelegramError as tge:
        result = str(tge)
    logger.info(LOG_MSG_UC, user_id, chat_id, restriction, result)


@flogger
@run_async
@retry_with_new_chat_id
def ban_user(bot, chat_id, user_id, reason, until):
    chat = bot.get_chat(chat_id=chat_id)

    all_adm = chat.all_members_are_administrators
    bot_adm = any(adm.user.id == bot.id for adm in chat.get_administrators())

    action = 'can not ban'
    if bot_adm and not all_adm:
        try:
            chat.kick_member(user_id=user_id, until_date=until)
            action = 'ban by'
        except ChatMigrated:
            raise
        except TelegramError as tge:
            reason = str(tge)
    else:
        reason = 'insufficient permissions or not allowed'

    logger.info(LOG_MSG_UC, user_id, chat_id, action, reason)

    # FOR DEBUGGING
    try:
        user = bot.get_chat_member(chat_id=chat_id, user_id=user_id).user
        text = f'{action}: {reason}\nchat: {chat.title}\nuser: {user.full_name}'
        bot.send_message(chat_id=CFG.did, text=text)
    except TelegramError:
        logger.exception('error sending debugging information')


@flogger
def pass_ban_rules(ctx, user_id, names):
    names = [name for name in names if name]
    if names:
        rules = BAN_RULES
    else:
        rules = [(lambda _: True, 'without name and username')]
        names = (None,)

    for rule, reason in rules:
        for name in names:
            if rule(name):
                until = datetime.datetime.now() + CFG.BANNED_RESTRICTION
                ban_user(ctx.bot, ctx.cid, user_id, reason, until)
                delete_from_db(ctx, DBDelete.SAN_ADM_RES, user_id=user_id)
                ctx.dbs.add(Expulsion(chat_id=ctx.cid, user_id=user_id,
                                      reason=reason, until=until))
                return False
    return True


@flogger
def send_captcha(ctx, mention, message_id=None):
    captcha, correct_answer, answers = get_captcha(CFG.CAPTCHA_ANSWERS)

    options = []
    for answer in answers:
        token = get_token()
        options.append((answer, f'captcha:{token}'))
        if answer == correct_answer:
            correct_token = token

    another_token = get_token()

    text = ctx.txt.CAPTCHA.format(user=mention, captcha=html.escape(captcha))
    keyboard = get_keyboard(options, [(ctx.txt.NEW_CAPTCHA, f'captcha:{another_token}')])
    parameters = {'text': text, 'reply_markup': keyboard, 'throw_exc': True}

    if message_id:
        message = ctx.edit(message_id=message_id, **parameters)
    else:
        message = ctx.send(**parameters)
    return correct_token, another_token, message.message_id


@flogger
@run_async
@retry_with_new_chat_id
def delete_message(bot, chat_id, message_id, text):
    if chat_id and message_id:
        try:
            result = bot.delete_message(chat_id=chat_id, message_id=message_id)
            logger.debug('%s, mid=%d deleted: %s', text, message_id, result)
        except ChatMigrated:
            raise
        except TelegramError as tge:
            logger.warning('%s, mid=%d: %s', text, message_id, tge)
    else:
        logger.debug('Nothing to do, chat_id=%s, message_id=%s', chat_id, message_id)


@flogger
def get_from_db(ctx, attr, chat_id, user_id):
    if chat_id or user_id:
        return getattr(ctx, f'get_{attr}s')(chat_id=chat_id or ctx.cid,
                                            user_id=user_id or ctx.uid)
    return getattr(ctx, attr)


@flogger
def delete_from_db(ctx, delete, chat_id=None, user_id=None):
    if isinstance(delete, DBDelete):
        items = []
    else:
        items = [delete]
        delete = None

    group_captcha_mids = []  # the associated messages must be deleted

    if delete:

        if DBDelete.SANCTION in delete:
            items.append(get_from_db(ctx, 'sanction', chat_id, user_id))

        if DBDelete.ADMISSION in delete:
            admission = get_from_db(ctx, 'admission', chat_id, user_id)
            items.append(admission)
            if admission and admission.group_captcha:
                group_captcha_mids.append(admission.group_captcha.message_id)

        if DBDelete.RESTRICTION in delete:
            items.append(get_from_db(ctx, 'restriction', chat_id, user_id))

        if DBDelete.EXPULSION in delete:
            items.append(get_from_db(ctx, 'expulsion', chat_id, user_id))

    for item in items:
        if item:
            ctx.dbs.delete(item)
            logger.debug('Deleted from db %s', item)

    for mid in group_captcha_mids:
        delete_message(ctx.bot, ctx.cid, mid, 'delete captcha message')


# ----------------------------------- #
# Threads


@flogger
@context
def captcha_thread(ctx):
    # Waiting time is over to solve captcha

    if not ctx.admission:
        return

    gcap = ctx.admission.group_captcha
    pcap = ctx.admission.private_captcha

    # Delete group captcha
    delete_message(ctx.bot, ctx.cid, gcap.message_id, 'delete captcha message')

    # Modify private captcha
    if pcap and pcap.status is CaptchaStatus.WAITING:
        result = bool(ctx.edit(chat_id=ctx.uid,  # private_chat_id is user_id
                               message_id=pcap.message_id,
                               text=ctx.txt.CAPTCHA_TIMEOUT_PRIVATE))
        logger.debug(LOG_MSG_U, ctx.uid, 'modified private captcha', result)

    # Need to expulsion?
    if gcap.status is not CaptchaStatus.SOLVED:
        until = datetime.datetime.now() + CFG.BANNED_RESTRICTION
        reason = f'captcha not resolved in time ({gcap.status})'
        ban_user(ctx.bot, ctx.cid, ctx.uid, reason, until)
        delete_from_db(ctx, DBDelete.SAN_ADM_RES)
        ctx.dbs.add(Expulsion(chat_id=ctx.cid, user_id=ctx.uid,
                              reason=reason, until=until))
        delete_message(ctx.bot, ctx.cid, ctx.admission.join_message_id,
                       'delete service message (new user)')

    if ctx.admission.dispose_in_thread == 'captcha':
        delete_from_db(ctx, ctx.admission)


@flogger
def get_admin_list(ctx):
    if ctx.chat.greet_admin_show:
        texts = ['\n', ctx.txt.ADMIN_LIST]

        lst = [get_mention(adm,
                           full=False,
                           preferably_username=True,
                           cite=ctx.chat.greet_admin_cite)
               for adm in ctx.get_admins()]

        if ctx.chat.greet_admin_order == 'Random':
            change_seed()
            random.shuffle(lst)

        elif ctx.chat.greet_admin_order == 'Z-A':
            lst.sort(key=sorted_alphabetically, reverse=True)

        else:  # A-Z
            lst.sort(key=sorted_alphabetically, reverse=False)

        for adm in lst:
            texts.append(f'• {adm}')

        return '\n'.join(texts)
    return ''


@flogger
def send_welcome(ctx, names):
    sep = html.escape(ctx.txt.LIST_SEP)
    end = html.escape(ctx.txt.LIST_END)

    prev = ctx.chat.prev_greet_users or ''
    prev_sep = prev + sep if prev else ''

    num = len(names)
    if prev or num > 1:
        if num == 1:
            text = end.format(prev, names[0])
        else:
            text = end.format(prev_sep + sep.join(names[:-1]), names[-1])
        flag = 2
    else:
        text = names[0]
        flag = 1

    # New welcome
    text = ctx.txt.GREETING(flag).format(mention=text)
    foot = ctx.chat.greet_note + get_admin_list(ctx)
    message = ctx.send(text=text + foot)
    if not message:
        logger.info(LOG_MSG_C, ctx.cid, 'greeting length', len(text + foot))
        ctx.send(text=ctx.txt.GREET_ERROR)
        if foot:
            # Try sending without the foot
            message = ctx.send(text=text)

    # Previous welcome
    delete_message(ctx.bot, ctx.cid, ctx.chat.prev_greet_message_id,
                   'delete previous greeting')

    # Save data
    if message:
        prev_users = prev_sep + sep.join(names)
        message_id = message.message_id
    else:
        logger.critical('chat=%d, unsent greeting', ctx.cid)
        prev_users = None
        message_id = None

    ctx.chat.prev_greet_users = prev_users
    ctx.chat.prev_greet_message_id = message_id

    status = f'mid={message_id}' if message else False
    logger.debug(LOG_MSG_C, ctx.cid, 'send greeting', status)


@flogger
@context
def greeting_thread(ctx):
    # Waiting time is over: greet the users

    names = []
    for admission in ctx.get_admissions(chat_id=ctx.cid):

        if admission.group_captcha.status is not CaptchaStatus.SOLVED:
            continue  # captcha still to be resolved

        chatmember = ctx.tgc.get_member(admission.user_id)  # can change
        if chatmember.status not in (chatmember.LEFT, chatmember.KICKED):
            # Status can by: CREATOR, ADMINISTRATOR, MEMBER, RESTRICTED
            user = chatmember.user
            if pass_ban_rules(ctx, user.id, [user.full_name, user.username]):
                if admission.to_greet:
                    names.append(get_mention(user, full=False))
                    logger.debug(LOG_MSG_UC, user.id, ctx.cid, 'greet', True)
            else:
                delete_message(ctx.bot, ctx.cid, admission.join_message_id,
                               'delete service message (new user)')
                delete_from_db(ctx, DBDelete.RESTRICTION, user_id=user.id)
        else:
            # `join_message_id` not deleted, user is no longer present,
            # and may be caused by several reasons
            delete_from_db(ctx, DBDelete.RESTRICTION, user_id=admission.user_id)

        # `Restriction` is eliminated in group_talk_handler and in expulsions
        if admission.dispose_in_thread == 'greeting':
            delete_from_db(ctx, admission)

    if names:
        send_welcome(ctx, names)


# ----------------------------------- #
# Main handlers


@flogger
@context
def null_handler(_):
    '''This is for updating database.'''


@flogger
@context
@cancel_greet_group
def help_handler(ctx):
    text = ctx.txt.HELP_WAIT if ctx.wait_another(ctx.cid, 'help') else ctx.txt.HELP
    ctx.send(text=text)


@flogger
@context
@cancel_greet_group
def private_only_handler(ctx):
    ctx.send(text=ctx.txt.PRIVATE_ONLY)


@flogger
@context
@cancel_greet_group
def groups_only_handler(ctx):
    ctx.send(text=ctx.txt.GROUPS_ONLY)


@flogger
@context
def new_user_handler(ctx):
    dispose_in_thread = 'greeting' if ctx.chat.greet_timer else 'captcha'
    new = False
    ban = False
    for new_user in ctx.tgm.new_chat_members:
        uid = new_user.id

        if uid == ctx.bot.id:
            continue  # ignore myself

        # It is not necessary to check the expulsions, because Telegram will
        # not allow entry, and if it allows it is because some administrator
        # enabled it

        if pass_ban_rules(ctx, uid, [new_user.full_name, new_user.username]):
            if new_user.is_bot:
                continue  # ignore other bots (in multiple inclusions)

            # Preventively limited to the user
            restrict_user(ctx.bot, ctx.cid, uid, UserRestriction.FULL)

            # Sending the captcha to the group
            mention = get_mention(new_user)
            correct_token, another_token, mid = send_captcha(ctx, mention)
            logger.debug(LOG_MSG_UC, ctx.cid, uid, 'send captcha', bool(mid))

            # Start timer
            ctx.job_queue.run_once(captcha_thread,
                                   CFG.CAPTCHA_TIMER.total_seconds(),
                                   context=(ctx.tgc, new_user))
            # Save data
            # Previous sanctions are not erased
            delete_from_db(ctx, DBDelete.ADM_RES, user_id=uid)
            admission = Admission(user_id=uid,
                                  chat_id=ctx.cid,
                                  join_message_id=ctx.mid,
                                  join_message_date=ctx.date,
                                  dispose_in_thread=dispose_in_thread)
            captcha = Captcha(message_id=mid,
                              status=CaptchaStatus.WAITING,
                              correct_token=correct_token,
                              another_token=another_token,
                              location=CaptchaLocation.GROUP,
                              admission=admission)
            ctx.dbs.add(admission)
            ctx.dbs.add(captcha)
            new = True
        else:
            ban = True

    if ban:
        # If several users entered together, can not be separated in the
        # service message, decided to erase all because it may contain spam
        delete_message(ctx.bot, ctx.cid, ctx.mid,
                       'delete service message (new user)')

    if new and ctx.chat.greet_give and ctx.chat.greet_timer:
        # Throw greeting thread
        ctx.job_queue.run_once(greeting_thread,
                               CFG.GREETING_TIMER.total_seconds(),
                               context=(ctx.tgc, None))


@flogger
@context
def left_user_handler(ctx):
    # User banned by the bot?
    if ctx.from_bot:
        # Cleaning to avoid spam (by name) in service message
        delete_message(ctx.bot, ctx.cid, ctx.mid,
                       'delete service message (left user)')

    user_id = ctx.tgm.left_chat_member.id

    admission = ctx.get_admissions(chat_id=ctx.cid, user_id=user_id)
    if admission:
        # Delete group captcha
        delete_message(ctx.bot, ctx.cid, admission.group_captcha.message_id,
                       'delete captcha message')

        # Modify private captcha
        pcap = admission.private_captcha
        if pcap and pcap.status is not CaptchaStatus.SOLVED:
            result = bool(ctx.edit(chat_id=user_id,  # private_chat_id is user_id
                                   message_id=pcap.message_id,
                                   text=ctx.txt.CAPTCHA_LEAVE_PRIVATE))
            logger.debug(LOG_MSG_U, user_id, 'modified private captcha', result)

    # Delete all info
    delete_from_db(ctx, DBDelete.ADM_RES, user_id=user_id)


@flogger
@context
def group_talk_handler(ctx):

    # Spam is not allowed
    spam = get_spam(ctx.tgm)
    if spam and ctx.chat.max_strikes:
        reason = f'deleted by spam: REmatch={spam.group()}'

        # FOR DEBUGGING
        try:
            text = f'{reason}\nchat: {ctx.tgc.title}\nuser: {ctx.tgu.full_name}'
            ctx.bot.send_message(chat_id=CFG.did, text=text)
            ctx.bot.forward_message(chat_id=CFG.did,
                                    from_chat_id=ctx.cid,
                                    message_id=ctx.mid)
        except TelegramError:
            logger.exception('error sending debugging information')

        delete_message(ctx.bot, ctx.cid, ctx.mid, reason)
        ctx.sanction.strikes += 1
        ctx.send(text=ctx.txt.STRIKE_WARNING.format(user=get_mention(ctx.tgu),
                                                    strike=ctx.sanction.strikes,
                                                    max_strikes=ctx.chat.max_strikes))

        if ctx.sanction.strikes >= ctx.chat.max_strikes:
            until = datetime.datetime.now() + CFG.BANNED_RESTRICTION
            reason = 'spammer'
            ban_user(ctx.bot, ctx.cid, ctx.uid, reason, until)
            delete_from_db(ctx, DBDelete.SAN_ADM_RES)
            ctx.dbs.add(Expulsion(chat_id=ctx.cid, user_id=ctx.uid,
                                  reason=reason, until=until))
        return

    # If can not limit the user (available only for supergroups)
    # must delete their messages until the captcha is resolve
    if ctx.admission and ctx.admission.group_captcha.status is not CaptchaStatus.SOLVED:
        delete_message(ctx.bot, ctx.cid, ctx.mid, 'user needs to resolve captcha')
        return

    text = simplify_text(ctx.text or '', only_word=False)

    # Or until run out of time limitation
    if ctx.restriction:
        if datetime.datetime.now() < ctx.restriction.until:
            if ctx.is_forward or not text or URL_MAIL_RE.search(text):
                # Only text allowed at beginning
                delete_message(ctx.bot, ctx.cid, ctx.mid, 'temporarily limited user')
                return
        else:
            delete_from_db(ctx, DBDelete.RESTRICTION)
            logger.info(LOG_MSG_UC, ctx.uid, ctx.cid, 'unrestricted', True)

    # Cancel greeting grouping
    if ctx.chat.prev_greet_users:
        ctx.chat.prev_greet_users = None
        ctx.chat.prev_greet_message_id = None
        logger.debug(LOG_MSG_C, ctx.cid, 'group next greeting', False)

    # Greeting given by a member of the group
    if ctx.chat.greet_timer and GREETINGS_RE.search(text):
        for admission in ctx.get_admissions(chat_id=ctx.cid):
            admission.to_greet = False
        logger.debug(LOG_MSG_C, ctx.cid, 'next greeting', False)

    # Admin call function
    if ctx.chat.call_admins and ADMINS_RE.search(text):
        params = {'full': False, 'preferably_username': True}
        alist = ctx.get_admins()
        atext = ', '.join(get_mention(user, **params) for user in alist)
        ctx.reply(text=ctx.txt.QUOTE_ADMINS(len(alist)).format(mention=atext))


def captcha_process(func):
    @functools.wraps(func)
    def captcha_process_wrapper(ctx):
        answer = None

        if ctx.is_group:
            if ctx.admission:
                cap = ctx.admission.group_captcha
                if cap.message_id == ctx.mid and cap.status is CaptchaStatus.WAITING:
                    answer = func(ctx, cap, ctx.admission.chat_id)

        elif ctx.is_private:
            for admission in ctx.get_admissions(user_id=ctx.uid, with_pcap=True):
                cap = admission.private_captcha
                if cap.message_id == ctx.mid and cap.status is CaptchaStatus.WAITING:
                    answer = func(ctx, cap, admission.chat_id)

        return answer
    return captcha_process_wrapper


def captcha_handler_answer(func):
    @functools.wraps(func)
    def captcha_handler_answer_wrapper(ctx, captcha, chat_id):
        answer = func(ctx, captcha, chat_id)
        logger.debug(LOG_MSG_UC, ctx.uid, ctx.cid, 'captcha handler answer', answer)
        if answer:
            ctx.answer(text=answer)
        return answer
    return captcha_handler_answer_wrapper


@flogger
@context
@captcha_process
@captcha_handler_answer
def captcha_handler(ctx, captcha, chat_id):

    token = ctx.update.callback_query.data[8:]  # 'captcha:«token»'
    mention = get_mention(ctx.tgu)

    # New captcha
    if captcha.is_another(token):
        if ctx.wait_another(chat_id, 'captcha'):
            return ctx.txt.CAPTCHA_WAIT_ALERT

        logger.debug(LOG_MSG_U, ctx.uid, 'token', 'new')

        correct_token, another_token, mid = send_captcha(ctx, mention,
                                                         captcha.message_id)
        logger.debug(LOG_MSG_UC, ctx.cid, ctx.uid, 'send captcha', bool(mid))
        captcha.correct_token = correct_token
        captcha.another_token = another_token
        return None  # nothing else for now

    # Correct answer
    if captcha.is_correct(token):
        logger.debug(LOG_MSG_U, ctx.uid, 'token', 'correct')

        captcha.status = CaptchaStatus.SOLVED
        text = ctx.txt.CAPTCHA_SOLVED_GROUP.format(user=mention)

        if ctx.is_private:
            # Modify the captcha of the group as well
            g_captcha = captcha.admission.group_captcha
            g_captcha.status = CaptchaStatus.SOLVED
            ctx.edit(chat_id=chat_id, message_id=g_captcha.message_id, text=text)
            text = ctx.txt.CAPTCHA_SOLVED_PRIVATE
            #
            tgc, chat = ctx.get_tgc_and_chat(chat_id)
        else:
            tgc, chat = ctx.tgc, ctx.chat

        ctx.edit(text=text)

        # Accepted but temporarily limited
        until = datetime.datetime.now() + CFG.TEMPORARY_RESTRICTION
        delete_from_db(ctx, DBDelete.RESTRICTION, chat_id=chat_id)
        restrict_user(ctx.bot, chat_id, ctx.uid, UserRestriction.TEMP, until)
        ctx.dbs.add(Restriction(chat_id=chat_id, user_id=ctx.uid, until=until))

        # To be greeted?
        if chat.greet_give and not chat.greet_timer:
            ctx.job_queue.run_once(greeting_thread,
                                   CFG.GAP_NOW_TIME,
                                   context=(tgc, None))

        return ctx.txt.CAPTCHA_SOLVED_ALERT

    # Wrong answer
    logger.debug(LOG_MSG_U, ctx.uid, 'token', 'wrong')
    captcha.status = CaptchaStatus.WRONG
    if ctx.is_group:
        # Link to second opportunity
        url = f't.me/{ctx.bot.username}?start={int(time.time()*1e6)}'
        parameters = {
            'text': ctx.txt.CAPTCHA_WRONG_GROUP.format(user=mention),
            'reply_markup': get_keyboard([(ctx.txt.CHANCE_CAPTCHA, None, url)])
        }
    else:
        parameters = {'text': ctx.txt.CAPTCHA_WRONG_PRIVATE}
    ctx.edit(**parameters)
    return ctx.txt.CAPTCHA_WRONG_ALERT


# ----------------------------------- #
# Configuration handlers


@flogger
def get_menu_config(ctx, item=None):
    if item is None:
        item = ctx.cfg.init

    if item not in ctx.cfg.data:
        text = ctx.txt.STOP_CONFIG
        markup = None
    else:
        value = getattr(ctx.chat, item.lower())
        text, markup = ctx.cfg.get_menu(item, value, ctx.txt)

    return text, markup


@flogger
@context
@cancel_greet_group
@only_admin
def config_handler(ctx):
    text, markup = get_menu_config(ctx)
    ctx.reply(text=text, reply_markup=markup)


@flogger
@context
@only_admin
def config_cb_handler(ctx):
    query = ctx.update.callback_query
    try:
        _, item, value = query.data.split(':')
    except ValueError:
        return

    if not item or not query.message:
        # query.message: will not be available if the message is too old
        return

    if query.message.reply_to_message.from_user.id != query.from_user.id:
        ctx.answer(text=ctx.txt.NOT_STARTED_CONFIG)
        return

    text = ctx.txt.STOP_CONFIG
    markup = None

    if item == 'prev':
        text, markup = get_menu_config(ctx, value)

    elif item != 'stop':
        column = item.lower()
        if value:
            if hasattr(ctx.chat, column):
                try:
                    value = ctx.cfg.mapper(item, value)

                    if item == 'GREET_TIMER' and value and not getattr(ctx.chat, column):
                        # Members who entered with the timer deactivated and still
                        # do not respond to the captcha, may be left unwelcome,
                        # that is why a greeting_thread is activated here
                        ctx.job_queue.run_once(greeting_thread,
                                               CFG.GREETING_TIMER.total_seconds(),
                                               context=(ctx.tgc, None))

                    setattr(ctx.chat, column, value)

                except ValueError as exc:
                    logger.critical('column="%s", value="%s", error="%s"',
                                    column, value, exc)
                    item = None
            else:
                logger.critical('column="%s" not exist in database', column)
                item = None

        if item:
            value = getattr(ctx.chat, column)
            next_item = ctx.cfg.get_next(item, value)
            if next_item:
                text, markup = get_menu_config(ctx, next_item)

    ctx.edit(text=text,
             reply_markup=markup,
             chat_id=query.message.chat_id,
             message_id=query.message.message_id)


@flogger
@context
@cancel_greet_group
@only_admin
def set_greet_note_handler(ctx):
    note = ctx.arg
    note = f'{note} ' if note and not STARTS_WITH_SPACE.match(note) else note
    ctx.chat.greet_note = note
    ctx.send(text=getattr(ctx.txt, 'GREET_NOTE_SET' if note else 'GREET_NOTE_UNSET'))


# ----------------------------------- #
# Private interaction handlers


@flogger
@context
def start_handler(ctx):
    now = datetime.datetime.now()

    wrongs = {}
    for admission in ctx.get_admissions(user_id=ctx.uid):

        still_valid = now - admission.join_message_date < CFG.CAPTCHA_TIMER
        wrong_group = admission.group_captcha.status is CaptchaStatus.WRONG
        not_private = not admission.private_captcha

        if still_valid and wrong_group and not_private:
            title = html.escape(admission.chat.title)
            wrongs[f'{len(wrongs)+1}• {title}'] = admission.chat_id

    if wrongs:
        ctx.cache.setdefault(ctx.uid, {})['wrongs'] = wrongs
        keyboard = get_keyboard((ctx.txt.YES, ctx.txt.NO), **KEYBOARD_COMMON)
        message = ctx.send(text=ctx.txt.MENU_SOLVE, reply_markup=keyboard)
        logger.debug(LOG_MSG_P, ctx.uid, 'start menu', bool(message))
        return PrivateStep.INIT

    waits = set()
    for expulsion in ctx.get_expulsions(user_id=ctx.uid):
        wait = expulsion.until - now
        if wait.total_seconds() > 0:
            waits.add(ctx.txt.LIST_ITEM.format(delta=ctx.txt.sec_to_text(wait),
                                               chat=html.escape(expulsion.chat.title)))
    if waits:
        text = ctx.txt.MENU_WAIT.format(chat_list='\n'.join(sorted(waits)))
        keyboard = ReplyKeyboardRemove()
        message = ctx.send(text=text, reply_markup=keyboard)
        logger.debug(LOG_MSG_P, ctx.uid, 'must wait', bool(message))
        return PrivateStep.STOP

    ctx.send(text=ctx.txt.MENU_NOTHING)
    return PrivateStep.STOP


@flogger
@context
@check_cache
def init_handler(ctx):
    if ctx.text != ctx.txt.YES:
        return stop_process(ctx)

    wrongs = ctx.cache[ctx.uid]['wrongs']
    if len(wrongs) > 1:
        keyboard = get_keyboard(list(wrongs), **KEYBOARD_COMMON)
        message = ctx.send(text=ctx.txt.MENU_CHOOSE, reply_markup=keyboard)
        logger.debug(LOG_MSG_P, ctx.uid, 'select chat', bool(message))
        return PrivateStep.CHAT

    key = next(iter(wrongs))
    chat_id = wrongs[key]
    ctx.send(text=ctx.txt.PROCESSING.format(item=html.escape(key.split('• ')[1])),
             reply_markup=ReplyKeyboardRemove())
    return chat_process(ctx, chat_id)


@flogger
@context
@check_cache
def chat_handler(ctx):
    chat_id = ctx.cache[ctx.uid]['wrongs'].get(ctx.text)
    if chat_id:
        return chat_process(ctx, chat_id)

    return incorrect_process(ctx)


@flogger
def chat_process(ctx, chat_id):
    ctx.cache[ctx.uid].pop('wrongs', None)

    correct_token, another_token, mid = send_captcha(ctx, get_mention(ctx.tgu))
    admission = ctx.get_admissions(chat_id=chat_id, user_id=ctx.uid)
    ctx.dbs.add(Captcha(message_id=mid,
                        status=CaptchaStatus.WAITING,
                        correct_token=correct_token,
                        another_token=another_token,
                        location=CaptchaLocation.PRIVATE,
                        admission=admission))

    logger.debug(LOG_MSG_P, ctx.uid, 'send captcha', bool(mid))
    return PrivateStep.STOP


@flogger
@context
def stop_handler(ctx):
    return stop_process(ctx)


@flogger
def stop_process(ctx):
    ctx.cache.get(ctx.uid, {}).pop('wrongs', None)
    message = ctx.send(text=ctx.txt.CANCELLED, reply_markup=ReplyKeyboardRemove())
    logger.debug(LOG_MSG_P, ctx.uid, 'cancel menu', bool(message))
    return PrivateStep.STOP


@flogger
def incorrect_process(ctx):
    message = ctx.send(text=ctx.txt.INCORRECT)
    logger.debug(LOG_MSG_P, ctx.uid, 'incorrect option', bool(message))


# ----------------------------------- #


@flogger
@context
def debug_handler(ctx):
    from pprint import pformat
    from giwel.config import DEFAULT
    from giwel.database import Chat, Sanction

    configs = []
    for var, val in DEFAULT.items():
        if val.__class__ is not type:  # may contain sensitive data
            configs.append(f'• {var}: {repr(CFG.__dict__.get(var, val))}')

    texts = []
    for model in (Chat, Sanction, Admission, Restriction, Expulsion):
        rows = '\n'.join(f'• {str(row)}' for row in ctx.dbs.query(model).all())
        if rows:
            texts.append(rows)

    logger.debug('DEBUGGING:\n\nCFG:\n%s\n\nMEM:\n%s\n\nDB:\n%s\n',
                 '\n'.join(configs),
                 pformat(ctx.cache),
                 '\n\n'.join(texts))


@flogger
@context
def error_handler(ctx):
    logger.critical(ctx.error)

    if ctx.tgm:
        ctx.tgm.reply_text(text=ctx.txt.ERROR_HAPPENED)

    data = []

    if ctx.tgu:
        data.append(f'with the user {mention_html(ctx.uid, ctx.tgu.name or ctx.uid)}')

    if ctx.tgc:
        name = ctx.tgc.title or ctx.tgc.first_name or ctx.cid
        data.append(f'within the chat <i>{html.escape(name)}</i>')
        if ctx.tgc.username:
            data.append(f'(@{ctx.tgc.username})')

    head_text = f'The error <code>{ctx.error}</code> happened {" ".join(data)}. '
    trace_text = 'The full traceback:\n\n<code>{}</code>'

    trace = traceback.format_tb(sys.exc_info()[2])
    size = len(head_text) + len(trace_text)

    # To avoid exceeding the limit of 4096 characters per message
    for num in range(0, len(trace), 2):
        note = [f'    ...{num} lines were cut...\n'] if num else []
        _trace = ''.join(trace[:2] + note + trace[2 + num:])
        if size + len(_trace) <= 4098:  # +2 by the characters '{}' in trace_text
            trace_text = trace_text.format(_trace)
            break

    text = head_text + trace_text
    ctx.send(chat_id=CFG.fid, text=text, parse_mode='HTML')


# ----------------------------------- #


def add_handlers(dispatcher):
    add = dispatcher.add_handler

    add(CommandHandler('help', help_handler))

    # Config
    add(CommandHandler('config', config_handler, Filters.group))
    add(CommandHandler('set_greet_note', set_greet_note_handler, Filters.group))

    # Update
    add(MessageHandler(Filters.status_update.new_chat_members,
                       new_user_handler,
                       pass_job_queue=True))
    add(MessageHandler(Filters.status_update.left_chat_member,
                       left_user_handler))
    add(MessageHandler(Filters.status_update.chat_created |
                       Filters.status_update.new_chat_title,
                       null_handler))

    # Incorrect uses
    add(CommandHandler('start', private_only_handler, Filters.group))
    add(CommandHandler('config', groups_only_handler, Filters.private))
    add(CommandHandler('set_greet_note', groups_only_handler, Filters.private))

    # Conversation
    add(MessageHandler(Filters.group,
                       group_talk_handler,
                       edited_updates=True))

    # Callback queries
    add(CallbackQueryHandler(captcha_handler,
                             pattern='^captcha:.+$',
                             pass_job_queue=True))
    add(CallbackQueryHandler(config_cb_handler,
                             pattern='^config:.+$',
                             pass_job_queue=True))

    # Private
    add(ConversationHandler(
        entry_points=[CommandHandler('start', start_handler, Filters.private)],
        states={
            PrivateStep.INIT: [MessageHandler(Filters.text, init_handler)],
            PrivateStep.CHAT: [MessageHandler(Filters.text, chat_handler)],
        },
        fallbacks=[CommandHandler('cancel', stop_handler, Filters.private)],
    ))

    # Father
    only_father = Filters.user(CFG.fid) & Filters.private
    add(CommandHandler('debug', debug_handler, only_father))

    # Errors
    dispatcher.add_error_handler(error_handler)


def only_bot_logging():
    logger_names = (
        'urllib3.connectionpool',
        'JobQueue',
        'telegram.bot',
        'telegram.ext.updater',
        'telegram.ext.dispatcher',
        'telegram.ext.conversationhandler',
        'telegram.vendor.ptb_urllib3.urllib3.util.retry',
        'telegram.vendor.ptb_urllib3.urllib3.connectionpool',
    )
    for logger_name in logger_names:
        logging.getLogger(logger_name).setLevel(logging.ERROR)


def run():
    logger.info('Running in Python %s', SPACES_RE.sub(' ', sys.version))

    if CFG.CAPTCHA_TIMER >= CFG.GREETING_TIMER:
        raise ValueError('[CFG] CAPTCHA_TIMER must be less than GREETING_TIMER')

    if CFG.WAIT_FOR_ANOTHER * 3 >= CFG.CAPTCHA_TIMER.total_seconds():
        raise ValueError('[CFG] WAIT_FOR_ANOTHER must be at least three times '
                         'less than CAPTCHA_TIMER')

    parser = argparse.ArgumentParser(
        description='The default operating mode is webhook',
        epilog='Verbose: 0 WARNING, 1 INFO, 2 DEBUG (only bot); 3 Full DEBUG')
    parser.add_argument(
        '-p', '--polling',
        help='start the bot in polling mode',
        action='store_true')
    parser.add_argument(
        '-c', '--clean',
        help='clean any pending updates',
        action='store_true')
    parser.add_argument(
        '-v', '--verbose',
        help='verbose level, repeat up to three times',
        action='count',
        default=0)
    args = parser.parse_args()

    levels = (logging.WARNING, logging.INFO, logging.DEBUG)
    level = levels[min(len(levels) - 1, args.verbose)]
    logging.basicConfig(level=logging.DEBUG,
                        format=CFG.log_format,
                        datefmt=CFG.DATETIME_FORMAT)
    logger.setLevel(level)

    if args.verbose < 3:
        only_bot_logging()

    logger.info('Initializing bot v%s...', VERSION)

    # Init
    updater = Updater(CFG.TELEGRAM_TOKEN)
    add_handlers(updater.dispatcher)
    context.initialize()

    dbs = context.dbsmk()
    try:
        njobs = load_jobs(dbs, updater.job_queue)
        # If there are pending jobs there may be pending telegram updates as well,
        # so the database is not cleaned
        if njobs == 0:
            messages = clean_database(dbs)
            for chat_id, message_id in sorted(set(messages), reverse=True):
                delete_message(updater.bot, chat_id, message_id, 'old admissions')
    finally:
        dbs.close()

    # Mode
    if args.polling:
        updater.start_polling(clean=args.clean)
        logger.debug('Start in polling mode, clean=%s', args.clean)
    else:
        # start_webhook: only set webhook if SSL is handled by library
        updater.bot.set_webhook(f'{CFG.HOST}/{CFG.BOT_PATH}')
        # cleaning updates is not supported if SSL-termination happens elsewhere
        updater.start_webhook(listen=CFG.BIND, port=CFG.PORT, url_path=CFG.BOT_PATH)
        logger.debug('Start in webhook mode')

    # Wait...
    updater.idle()

    # Save jobs
    dbs = context.dbsmk()
    try:
        save_jobs(dbs, updater.job_queue)
    finally:
        dbs.close()

    # Stop.
    logger.info('Stopped Bot.')
    return 0
