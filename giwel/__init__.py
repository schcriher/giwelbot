# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import sys

if sys.hexversion < 0x03060000:
    raise RuntimeError('requires python 3.6 or higher')

from giwel.core import run  # pylint: disable=wrong-import-position
