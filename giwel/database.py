# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán
# pylint: disable=too-few-public-methods

import re
import enum
import time
import zlib
import pickle  # nosec
import logging
import secrets
import datetime

from telegram.ext import Job

from sqlalchemy import (event, create_engine, Column, ForeignKey, UniqueConstraint,
                        BigInteger, Integer, String, Boolean, DateTime, LargeBinary)
from sqlalchemy.sql import and_, exists, func
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm.collections import attribute_mapped_collection

from giwel.debug import flogger
from giwel.tools import signature, verify_signature
from giwel.config import CFG

logger = logging.getLogger(__name__)

SELECT_RE = re.compile(r'SELECT.*')

JOB_DATA = ('callback', 'interval', 'repeat', 'context', 'days', 'name')  # v12: tzinfo
JOB_STATE = ('_remove', '_enabled')


class CaptchaStatus(enum.Enum):
    WAITING = 0
    SOLVED = 1
    WRONG = 2


class CaptchaLocation(enum.Enum):
    GROUP = 0
    PRIVATE = 1


@event.listens_for(Engine, "before_cursor_execute")
def before_cursor_execute(conn, cursor, statement, parameters, context, executemany):
    # pylint: disable=unused-argument,too-many-arguments
    conn.info.setdefault('query_start_time', []).append(time.time())
    logger.debug("→ sql: %s", SELECT_RE.sub(r'\nSELECT *', str(statement)))

@event.listens_for(Engine, "after_cursor_execute")
def after_cursor_execute(conn, cursor, statement, parameters, context, executemany):
    # pylint: disable=unused-argument,too-many-arguments
    total = time.time() - conn.info['query_start_time'].pop(-1)
    logger.debug("← sql, in: %.6fs", total)


class Base:

    @declared_attr
    def __tablename__(cls):
        # pylint: disable=no-member,no-self-argument
        return cls.__name__.lower()

    def __repr__(self):
        # pylint: disable=no-member
        params = ', '.join(f'{column}={self.__value__(column)}'
                           for column in self.__table__.columns.keys())
        return f'{self.__class__.__name__}({params})'

    def __value__(self, column):
        value = getattr(self, column)

        if isinstance(value, datetime.datetime):
            return f'«{value:{CFG.DATETIME_FORMAT}}»'

        return repr(value)

BASE = declarative_base(cls=Base)


class Sanction(BASE):
    id = Column(Integer, primary_key=True)

    chat_id = Column(BigInteger, ForeignKey('chat.id'), nullable=False)
    user_id = Column(BigInteger, nullable=False)

    strikes = Column(Integer, nullable=False, default=0)

    chat = relationship("Chat", foreign_keys=chat_id)
    chat_user_unique = UniqueConstraint(chat_id, user_id)


class Admission(BASE):
    __mapper_args__ = {'confirm_deleted_rows': False}  # for ON DELETE CASCADE

    id = Column(Integer, primary_key=True)

    chat_id = Column(BigInteger, ForeignKey('chat.id'), nullable=False)
    user_id = Column(BigInteger, nullable=False)

    to_greet = Column(Boolean, nullable=False, default=True)
    join_message_id = Column(BigInteger, nullable=False)
    join_message_date = Column(DateTime, nullable=False)
    dispose_in_thread = Column(String, nullable=False)

    # https://docs.sqlalchemy.org/en/13/orm/tutorial.html
    # https://docs.sqlalchemy.org/en/13/orm/collections.html#passive-deletes
    captchas = relationship('Captcha',
                            back_populates='admission',
                            order_by="Captcha.location_id",
                            single_parent=True,
                            lazy='joined',
                            cascade="all, delete-orphan",
                            passive_deletes=True,
                            collection_class=attribute_mapped_collection('location'))

    chat = relationship('Chat', back_populates='admissions')
    chat_user_unique = UniqueConstraint(chat_id, user_id)

    @property
    def group_captcha(self):
        return self.captchas.get(CaptchaLocation.GROUP)

    @property
    def private_captcha(self):
        return self.captchas.get(CaptchaLocation.PRIVATE)


class Captcha(BASE):
    __mapper_args__ = {'confirm_deleted_rows': False}  # for ON DELETE CASCADE

    id = Column(Integer, primary_key=True)

    message_id = Column(BigInteger, nullable=False)
    location_id = Column(Integer, nullable=False)
    status_id = Column(Integer, nullable=False)
    correct_token = Column(String, nullable=False)
    another_token = Column(String, nullable=False)

    admission_id = Column(Integer, ForeignKey('admission.id', ondelete="CASCADE"),
                          nullable=False)
    admission = relationship('Admission', back_populates='captchas')

    @property
    def location(self):
        return CaptchaLocation(self.location_id)

    @location.setter
    def location(self, value):
        self.location_id = CaptchaLocation(value).value

    @property
    def status(self):
        return CaptchaStatus(self.status_id)

    @status.setter
    def status(self, value):
        self.status_id = CaptchaStatus(value).value

    def is_correct(self, token):
        return secrets.compare_digest(token, self.correct_token)

    def is_another(self, token):
        return secrets.compare_digest(token, self.another_token)


class Restriction(BASE):
    id = Column(Integer, primary_key=True)

    chat_id = Column(BigInteger, ForeignKey('chat.id'), nullable=False)
    user_id = Column(BigInteger, nullable=False)

    until = Column(DateTime, nullable=False)

    chat = relationship('Chat', back_populates='restrictions')
    chat_user_unique = UniqueConstraint(chat_id, user_id)


class Expulsion(BASE):
    id = Column(Integer, primary_key=True)

    chat_id = Column(BigInteger, ForeignKey('chat.id'), nullable=False)
    user_id = Column(BigInteger, nullable=False)

    reason = Column(String, nullable=False)
    until = Column(DateTime, nullable=False)

    chat = relationship('Chat', back_populates='expulsions')


class Chat(BASE):
    id = Column(BigInteger, primary_key=True)
    title = Column(String)
    interacts = Column(BigInteger, nullable=False, default=0)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now)
    updated_at = Column(DateTime, nullable=False, default=datetime.datetime.now)

    language = Column(String)
    max_strikes = Column(Integer, nullable=False, default=0)
    call_admins = Column(Boolean, nullable=False, default=True)

    greet_give = Column(Boolean, nullable=False, default=True)
    greet_note = Column(String, nullable=False, default='')
    greet_timer = Column(Boolean, nullable=False, default=True)
    greet_admin_show = Column(Boolean, nullable=False, default=False)
    greet_admin_cite = Column(Boolean, nullable=False, default=False)
    greet_admin_order = Column(String, nullable=False, default='A-Z')

    prev_greet_users = Column(String)
    prev_greet_message_id = Column(BigInteger)

    admissions = relationship('Admission', back_populates='chat')
    restrictions = relationship('Restriction', back_populates='chat')
    expulsions = relationship('Expulsion', back_populates='chat')


class PyObj(BASE):
    id = Column(Integer, primary_key=True)

    name = Column(String, nullable=False)
    data = Column(LargeBinary, nullable=False)
    sign = Column(LargeBinary, nullable=False)

    created_at = Column(DateTime, nullable=False, default=datetime.datetime.now)

    def __init__(self, name, obj):
        # Average compression ratio
        #   zlib   0.83  s=0.005
        #   gzip   0.88  s=0.005
        #   bz2    0.99  s=0.014
        #   lzma   1.05  s=0.008
        # for typical job_queue cases
        self.name = name
        self.data = zlib.compress(pickle.dumps(obj))
        self.sign = signature(self.data,
                              key=CFG.signature_key,
                              size=CFG.SIGN_DIGEST_SIZE)

    @property
    def is_valid(self):
        return verify_signature(self.data,
                                self.sign,
                                key=CFG.signature_key,
                                size=CFG.SIGN_DIGEST_SIZE)

    @property
    def obj(self):
        return pickle.loads(zlib.decompress(self.data))  # nosec


# ---


def get_session_maker(create_all=False, drop_all=False, url=None, echo=False):
    engine = create_engine(url or CFG.DATABASE_URL, echo=echo)
    session_maker = sessionmaker(bind=engine)

    if drop_all:
        BASE.metadata.drop_all(engine)

    if create_all:
        BASE.metadata.create_all(engine)

    return session_maker


# ---


def get_query(model, query, chat_id=None, user_id=None):
    flag = 0

    if chat_id is not None:
        query = query.filter(getattr(model, 'chat_id') == chat_id)
        flag += 1

    if user_id is not None:
        query = query.filter(getattr(model, 'user_id') == user_id)
        flag += 1

    if flag == 2:
        return query.first()
    return query.all()


def get_or_create(dbs, model, attributes):
    obj = dbs.query(model).filter_by(id=attributes['id']).first()
    if obj:
        for var, val in attributes.items():
            if var != 'id':
                setattr(obj, var, val)
    else:
        obj = model(**attributes)
        dbs.add(obj)
        dbs.flush()
    return obj


def get_chat(dbs, **attributes):
    chat = get_or_create(dbs, Chat, attributes)
    chat.interacts += 1
    chat.updated_at = datetime.datetime.now()
    return chat


@flogger
def get_chat_lang(dbs, chat_id):
    chat = dbs.query(Chat).filter_by(id=chat_id).first()
    return chat.language if chat else None


def get_sanctions(dbs, chat_id=None, user_id=None):
    query = dbs.query(Sanction).order_by(Sanction.id.asc())
    # Sanction has a Unique Constraint on chat_id and user_id (unduplicated)
    obj = get_query(Sanction, query, chat_id, user_id)

    # For a single result if it is not in the database it is created
    if chat_id is not None and user_id is not None and not obj:
        obj = Sanction(chat_id=chat_id, user_id=user_id)
        dbs.add(obj)
        dbs.flush()

    return obj


def get_admissions(dbs, chat_id=None, user_id=None, with_pcap=False):
    query = dbs.query(Admission).order_by(Admission.id.asc())
    if with_pcap:
        query = (query
                 .join(Admission.captchas)
                 .filter(Captcha.location_id == CaptchaLocation.PRIVATE.value))
    # Admission has a Unique Constraint on chat_id and user_id (unduplicated)
    return get_query(Admission, query, chat_id, user_id)


def get_restrictions(dbs, chat_id=None, user_id=None):
    query = dbs.query(Restriction).order_by(Restriction.until.asc())
    # Restriction has a Unique Constraint on chat_id and user_id (unduplicated)
    return get_query(Restriction, query, chat_id, user_id)


def get_expulsions(dbs, chat_id=None, user_id=None):
    query = dbs.query(Expulsion).order_by(Expulsion.until.desc())
    # Expulsions are stored for a period of time,
    # chat_id and user_id may appear repeated
    if chat_id is None or user_id is None:
        sub = (dbs
               .query(func.max(Expulsion.id))                    # last expulsion for
               .group_by(Expulsion.chat_id, Expulsion.user_id))  # this user and chat

        # Is more efficient to filter the chat_id or the user_id here

        if chat_id is not None:
            sub = sub.filter_by(chat_id=chat_id)

        if user_id is not None:
            sub = sub.filter_by(user_id=user_id)

        return query.filter(Expulsion.id.in_(sub.distinct())).all()

    return get_query(Expulsion, query, chat_id, user_id)


@flogger
def migrate_chat(dbs, old_id, new_id):
    # Get old chat (group)
    old_chat = dbs.query(Chat).filter_by(id=old_id).first()
    new_chat = dbs.query(Chat).filter_by(id=new_id).first()
    if old_chat and not new_chat:

        # Create new chat (supergroup)
        new_chat = Chat(id=new_id)
        dbs.add(new_chat)

        # Update data
        for var in Chat.__table__.columns.keys():
            if var != 'id':
                setattr(new_chat, var, getattr(old_chat, var))

        for var in ('admissions', 'restrictions', 'expulsions'):
            old = getattr(old_chat, var)
            new = getattr(new_chat, var)
            for obj in list(old):
                old.remove(obj)
                new.append(obj)

        # Remove old chat
        dbs.delete(old_chat)
        dbs.flush()
        logger.debug('group %s migrated to supergroup %s', old_id, new_id)
    else:
        logger.warning('group id=%s was already migrated\nold: %s\nnew: %s',
                       old_id, old_chat, new_chat)


@flogger
def clean_database(dbs):
    messages = []  # messages to be deleted

    now = datetime.datetime.now()
    adm_lim = now - CFG.GREETING_TIMER
    exp_lim = now - CFG.EXPULSION_HISTORY_DAYS

    # Deleting old admissions/captchas message that have not been eliminated
    query = dbs.query(Admission).filter(Admission.join_message_date < adm_lim)
    for adm in query.all():
        kicked = dbs.query(Expulsion).filter(
            and_(
                Expulsion.chat_id == adm.chat_id,
                Expulsion.user_id == adm.user_id,
                Expulsion.until > adm.join_message_date,
            )
        ).count()
        if kicked:
            messages.append((adm.chat_id, adm.join_message_id))

        if adm.group_captcha:
            messages.append((adm.chat_id, adm.group_captcha.message_id))

    # Overall cleaning
    queries = (
        # Empty Sanction
        dbs.query(Sanction).filter(Sanction.strikes == 0),
        # Expired Admissions
        dbs.query(Admission).filter(Admission.join_message_date < adm_lim),
        # Orphan Captchas: passive delete of previous admissions is not executed
        dbs.query(Captcha).filter(
            ~exists().where(Admission.id == Captcha.admission_id)
        ),
        # Expired Restriction
        dbs.query(Restriction).filter(Restriction.until < now),
        # Expired Expulsion
        dbs.query(Expulsion).filter(Expulsion.until < exp_lim),
    )
    for query in queries:
        query.delete(synchronize_session=False)
        dbs.expire_all()
    dbs.commit()

    return messages


@flogger
def clear_jobs(dbs):
    dbs.query(PyObj).filter_by(name='job').delete()


@flogger
def load_jobs(dbs, job_queue):
    # pylint: disable=protected-access
    query = dbs.query(PyObj).filter_by(name='job').order_by(PyObj.created_at.asc())
    num = 0
    for pyobj in query.all():
        if pyobj.is_valid:
            next_t, data, state = pyobj.obj

            # New object with the same data
            job = Job(**{var: val for var, val in zip(JOB_DATA, data)})

            # Restore the state it had
            for var, val in zip(JOB_STATE, state):
                attribute = getattr(job, var)
                getattr(attribute, 'set' if val else 'clear')()

            job.job_queue = job_queue

            # Convert from absolute to relative time
            next_t -= time.time()

            # Delay to allow the entry of telegram updates
            if next_t < CFG.JOB_DELAY_TIME:
                next_t = CFG.JOB_DELAY_TIME

            job_queue._put(job, next_t)
            num += 1

    clear_jobs(dbs)
    dbs.commit()
    return num


@flogger
def save_jobs(dbs, job_queue):
    # pylint: disable=protected-access
    with job_queue._queue.mutex:

        clear_jobs(dbs)

        for next_t, job in sorted(job_queue._queue.queue):

            # Serialize
            data = tuple(getattr(job, var) for var in JOB_DATA)
            state = tuple(getattr(job, var).is_set() for var in JOB_STATE)

            # Store
            dbs.add(PyObj('job', (next_t, data, state)))

        dbs.commit()
