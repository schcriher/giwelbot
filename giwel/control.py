# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import re

from unidecode import unidecode

from giwel.tools import NOVIS_CHARS_RE, NO_WORD_RE, remove_diacritics

CHECKOUT = (
    'caption',
    'forward_signature',
    'forward_from_chat.title',
    'forward_from_chat.username',
    'author_signature',
    'forward_sender_name',
    'text',
)

HOMOGLYPHS_RE = tuple(
    (char, re.compile(f'[{homoglyphs}]'))
    for char, homoglyphs in (
        # Different from unidecode
        ('a', '\u0414\u0e04\u120d\u1972\uff91'
              '\U0001f1e6\U000e0041\U000e0061'),
        ('b', '\u0412\u0432\u044a\u0e52\u130c\u4e43'
              '\U0001f1e7\U000e0042\U000e0062'),
        ('c', '\u0254\u03c2\u0421\u0441\u0480\u122d\u1d04\u1d9c\u2183\u2184'
              '\U0001f1e8\U000e0043\U000e0063'),
        ('d', '\u0e54\u2202'
              '\U0001f1e9\U000e0044\U000e0064'),
        ('e', '\u018e\u01dd\u0401\u0404\u0454\u127f\u1971\u4e47\ufec9'
              '\U0001f1ea\U000e0045\U000e0065'),
        ('f', '\u0493\u093f\u127b\u1da0\ua730\ua7fb\uff77'
              '\U0001f1eb\U000e0046\U000e0066'),
        ('g', '\u1297\ufeed\ufeee'
              '\U0001f1ec\U000e0047\U000e0067'),
        ('h', '\u0402\u041d\u043d\u0452\u12d8\u3093'
              '\U0001f1ed\U000e0048\U000e0068'),
        ('i', '\u0407\u0671\u0e40\u130e\uff89'
              '\U0001f1ee\U000e0049\U000e0069'),
        ('j', '\u027e\u05df\u05e0\u130b\u2c7c\ufedd\uff8c'
              '\U0001f1ef\U000e004a\U000e006a'),
        ('k', '\u040c\u1315\u16d5\u30ba\ua740\ua741'
              '\U0001f1f0\U000e004b\U000e006b'),
        ('l', '\u05df\u1228\u1963\u2143\uff9a'
              '\U0001f1f1\U000e004c\U000e006c'),
        ('m', '\u0e53\u1320\uffb6'
              '\U0001f1f2\U000e004d\U000e006d'),
        ('n', '\u03b7\u0418\u0438\u0e01\u0e20\u12ad\u1952\u5200'
              '\U0001f1f3\U000e004e\U000e006e'),
        ('o', '\u12d5\u03c3\u0424\u0e4f\u12d0'
              '\U0001f1f4\U000e004f\U000e006f'),
        ('p', '\u03c1\u0420\u0440\u05e7\u12e8\u1d29\ua7fc\uff71'
              '\U0001f1f5\U000e0050\U000e0070'),
        ('q', '\u06f9\u12d2\ua756\ua757'
              '\U0001f1f6\U000e0051\U000e0071'),
        ('r', '\u042f\u0433\u044f\u0453\u12ea\u5c3a'
              '\U0001f1f7\U000e0052\U000e0072'),
        ('s', '\u0405\u0455\u0e23\u1290\u4e02\ua644\ua645\ua731'
              '\U0001f1f8\U000e0053\U000e0073'),
        ('t', '\u0413\u0547\u1355\uff72'
              '\U0001f1f9\U000e0054\U000e0074'),
        ('u', '\u0426\u0446\u0aaa\u0e22\u1201\u1d7e'
              '\U0001f1fa\U000e0055\U000e0075'),
        ('v', '\u028c\u03bd\u05e9\u06f7\u1200\u221a\u2c7d'
              '\U00010321\U0001f1fb\U000e0056\U000e0076'),
        ('w', '\u03c9\u0429\u0448\u0e1d\u0e2c\u1220'
              '\U0001f1fc\U000e0057\U000e0077'),
        ('x', '\u03c7\u0416\u0425\u0445\u05d0\u0e0b\u1238\uff92'
              '\U0001f1fd\U000e0058\U000e0078'),
        ('y', '\u040e\u0427\u0443\u04f2\u04f3\u05e5\u1203\uff98'
              '\U0001f1fe\U000e0059\U000e0079'),
        ('z', '\u0579\u130a\u1dbb\u4e59'
              '\U0001f1ff\U000e005a\U000e007a'),
        ('0', '\U000e0030'),
        ('1', '\u07c1\U000e0031'),
        ('2', '\U000e0032'),
        ('3', '\u04df\U000e0033'),
        ('4', '\U000e0034'),
        ('5', '\U000e0035'),
        ('6', '\U000e0036'),
        ('7', '\U000e0037'),
        ('8', '\U000e0038'),
        ('9', '\U000e0039'),
    ))


def simplify_text(text, only_word=True):
    if text:
        # Space
        text = NOVIS_CHARS_RE.sub(' ', text)
        # Homoglyph
        for char, homoglyphs_re in HOMOGLYPHS_RE:
            text = homoglyphs_re.sub(char, text)
        text = unidecode(text)
        # Only word
        if only_word:
            text = NO_WORD_RE.sub(' ', text)
        # Without Mark and Nonspacing characters
        text = remove_diacritics(text)
        return text.lower()
    return ''


# ---


# Most common for spam
TLD = (r'(?i:com|net|io|me|org|red|info|tools|mobi|xyz|biz|pro|blog|zip|link|to|kim|'
       r'review|country|cricket|science|work|party|g[dql]|jobs|c[co]|i[en]|ly|name)')

URL_MAIL = fr'(?P<I>(?i:w+\.|[/@]))?WORD\.(?(I)WORD|TLD)'
URL_MAIL = URL_MAIL.replace('WORD', r'[^\s.]+').replace('TLD', TLD)
URL_MAIL_RE = re.compile(URL_MAIL)

FAKE_NAME = (r'(?i:cuenta\s*eliminada|deleted\s*account|marketing|website|'
             r'p[o0]rn[o0]?|promos?\s*agent|telegram|tg(vip)?member|^$)')
FAKE_NAME_RE = re.compile(FAKE_NAME)

BAN_RULES = (
    (lambda name: len(name or '') > 39, 'long name'),
    (lambda name: URL_MAIL_RE.search(simplify_text(name, False)), 'name with uri'),
    (lambda name: FAKE_NAME_RE.search(simplify_text(name)), 'fake name'),
)


# ---


SPACE_RE = re.compile(' +')

SHORT_WORDS = (r'(?i:\s*\b(to|a(nd?)?|of|i[ts]?|m[ey]|the(ir)?|your?|her?|she|'
               r'with(out)?|yo?|el(l[ao])?s?|l[aeo]s?|est?[aeo]|con|de)\b\s*)')
SHORT_WORDS_RE = re.compile(SHORT_WORDS)

# Work in progress
SPAMMER = '|'.join(('tg(vip)?member',
                    'telegram marketing',
                    '(unete comparte|be part hentai|join hentai|join share)'#…
                    '((super)?gro?upo?|ch?an(ne|a)l)',
                    'ful+ chapters?(hentai|ahegao|yuri|p[o0]rn)',
                    'еr0tic ph0t0s? vide0s?'))

SPAMMER_RE = re.compile(SPACE_RE.sub('', f'({SPAMMER})'), re.IGNORECASE)


def get_spam(message):
    for checkout in CHECKOUT:
        try:
            attrs = iter(checkout.split('.'))
            obj = message
            while True:
                try:
                    obj = getattr(obj, next(attrs))
                except StopIteration:
                    break

            text = SHORT_WORDS_RE.sub('', simplify_text(obj))
            match = SPAMMER_RE.search(SPACE_RE.sub('', text))
            if match:
                return match

        except AttributeError:
            pass
    return None
