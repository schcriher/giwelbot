# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import random
import secrets
import operator

from giwel.tools import ZW_SPACE_CHARS, SPACE_CHARS, INVISIBLE_CHARS, change_seed
from giwel.debug import flogger
from giwel.config import CFG


OPERATORS = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul,
    '/': operator.truediv,
}


def get_space():                                        # Chars
    spaces = []
    spaces.append(secrets.choice(SPACE_CHARS))          # 1
    for _ in range(secrets.randbelow(3)):               # 0~2
        spaces.append(secrets.choice(ZW_SPACE_CHARS))
    return ''.join(spaces)                              # min: 1, max: 3


def get_invisible():                                    # Chars
    return secrets.choice(INVISIBLE_CHARS)              # min: 1, max: 1


def get_captcha_text(*items):                           # Chars
    chars = []
    chars.append(get_invisible())                       # 1
    for item in items:
        chars.append(get_space())                       # 1~3 * n
        chars.append(get_invisible())                   # 1   * n
        chars.append(str(item))                         # N
    chars.append(get_space())                           # 1~3
    chars.append(get_invisible())                       # 1
    return ''.join(chars)                               # min: 3+2*n+N, max: 5+4*n+N


@flogger
def get_captcha(num_answers):
    change_seed()

    # Operation
    operator_sym = secrets.choice(list(OPERATORS))
    operator_func = OPERATORS[operator_sym]
    while True:
        num_a = secrets.randbelow(CFG.CAPTCHA_MAX_NUM)
        num_b = secrets.randbelow(CFG.CAPTCHA_MAX_NUM)
        try:
            answer = operator_func(num_a, num_b)
            abs_answer = abs(answer)
            int_answer = int(answer)
            unrepeated = int_answer not in (num_a, num_b)
            if abs_answer < CFG.CAPTCHA_MAX_NUM and int_answer == answer and unrepeated:
                break
        except ZeroDivisionError:
            pass

    # Obfuscation
    captcha = get_captcha_text(num_a, operator_sym, num_b)

    # Correct answers
    correct_answer = str(int_answer)

    # Fake answers
    answers = []
    if num_answers > 4:
        for num in (f'{num_a}', f'{num_a or ""}{num_b}', f'{num_b}'):
            if num not in answers and num != correct_answer:
                answers.append(num)

    limit = num_answers - 1  # correct is inserted at the end
    while len(answers) < limit:
        num = secrets.randbelow(2 * CFG.CAPTCHA_MAX_NUM + 1) - CFG.CAPTCHA_MAX_NUM
        if abs(abs_answer - abs(num)) > CFG.CAPTCHA_MIN_SEP:
            num = str(num)
            if num not in answers and num != correct_answer:
                answers.append(num)

    random.shuffle(answers)
    # Never put the correct_answer at the beginning or ending
    answers.insert(secrets.randbelow(limit - 1) + 1, correct_answer)

    return captcha, correct_answer, answers
