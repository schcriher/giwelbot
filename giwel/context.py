# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import time
import pathlib
import logging
import functools
import threading
import collections

from telegram import Update
from telegram.ext import Job
from telegram.error import TelegramError, ChatMigrated

from giwel.debug import flogger
from giwel.tools import LANG_RE, mapper, get_arg, get_keyboard
from giwel.texts import Translator
from giwel.config import CFG
from giwel.database import (get_session_maker, get_chat, get_chat_lang,
                            get_sanctions, get_admissions, get_restrictions,
                            get_expulsions, migrate_chat)

FORWARDED = ('from', 'from_chat', 'from_message_id', 'signature', 'sender_name', 'date')

HTML_NO_PREVIEW = {'parse_mode': 'HTML', 'disable_web_page_preview': True}

logger = logging.getLogger(__name__)


def adm_cache(func):
    cache = {}

    @functools.wraps(func)
    def adm_cache_wrapper(bot, chat_id):
        now = time.time()
        try:
            ret, age = cache[chat_id]
            if age + CFG.CACHING_ADM_TIME < now:
                raise KeyError
        except KeyError:
            ret = func(bot, chat_id)
            cache[chat_id] = ret, now
        return ret

    adm_cache_wrapper.clear = cache.clear
    return adm_cache_wrapper


@flogger
@adm_cache
def get_admins(bot, chat_id):
    """Returns a list of user admin for a given chat_id."""
    return [admin.user for admin in bot.get_chat_administrators(chat_id)]


@flogger
@adm_cache
def all_members_are_administrators(bot, chat_id):
    """Returns true if all members are administrators in the given chat_id."""
    return bool(bot.get_chat(chat_id).all_members_are_administrators)


@flogger
def get_available_langs():
    '''Tuple of available translations.'''
    mofs = pathlib.Path(CFG.I18N_LOCALE).glob(f'*/LC_MESSAGES/{CFG.I18N_DOMAIN}.mo')
    langs = []
    for mof in mofs:
        obj = LANG_RE.match(mof.parts[1])
        if obj:
            langs.append(obj.group('lll_CC'))
    return langs



class Context:
    # pylint: disable=too-many-instance-attributes

    def __init__(self, kwargs):

        self.bot = None
        self.update = None
        self.job = None
        self.error = None
        self.tgc = None
        self.tgu = None
        self.tgm = None

        self.dbs = None
        self.txt = None
        self.cfg = None

        self.__dict__.update(kwargs)

        if self.update:
            self.tgc = self.update.effective_chat
            self.tgu = self.update.effective_user
            self.tgm = self.update.effective_message

        elif self.job:
            self.tgc = self.job.context[0]
            self.tgu = self.job.context[1]

        self.set_aliases()


    def set_aliases(self):
        params = HTML_NO_PREVIEW.copy()

        if self.tgc:
            params['chat_id'] = self.cid

        self.send = self._define(self.bot.send_message, **params)

        if self.tgm:
            if self.tgc.type == self.tgc.PRIVATE:
                self.reply = self._define(self.bot.send_message, **params)
            else:
                self.reply = self._define(self.bot.send_message, **params,
                                          reply_to_message_id=self.mid)
            params['message_id'] = self.mid

        self.edit = self._define(self.bot.edit_message_text, **params)

        if self.update and self.update.callback_query:
            params = {'callback_query_id': self.update.callback_query.id,
                      'show_alert': True}
            self.answer = self._define(self.bot.answer_callback_query, **params)


    def __repr__(self):
        return f'«{self.__class__.__name__}»'


    def _define(self, func, **init_params):
        @flogger
        @functools.wraps(func)
        def _define_wrapper(**call_params):
            params = {**init_params, **call_params}
            throw_exc = params.pop('throw_exc', False)
            result = None
            try:
                result = func(**params)

            except ChatMigrated as migrated:
                old_id = params['chat_id']
                new_id = migrated.new_chat_id
                self.migrate_chat(old_id, new_id)
                params['chat_id'] = new_id
                try:
                    result = func(**params)
                except TelegramError as tge:
                    logger.warning('%s: [after migration] %s', func.__name__, tge)
                    if throw_exc:
                        raise

            except TelegramError as tge:
                logger.warning('%s: %s', func.__name__, tge)
                if throw_exc:
                    raise

            return result
        return _define_wrapper


    @flogger
    def migrate_chat(self, old_id, new_id):
        migrate_chat(self.dbs, old_id, new_id)
        # Update the data of this object (ctx) to the new chat
        self.tgc = self.bot.get_chat(chat_id=new_id)
        self.reset_cache()
        self.set_aliases()


    @flogger
    def __getattr__(self, var):

        if var in ('cid', 'uid', 'mid'):
            tgx = getattr(self, f'tg{var[0]}')
            value = getattr(tgx, 'message_id' if var == 'mid' else 'id')

        elif var in ('text', 'date'):
            value = getattr(self.tgm, var)

        elif var == 'arg':
            value = get_arg(self.text, ' ')

        elif var == 'from_bot':
            value = self.uid == self.bot.id

        elif var in ('is_group', 'is_private'):
            value = self.tgc.type in ((self.tgc.GROUP, self.tgc.SUPERGROUP)
                                      if var == 'is_group' else
                                      (self.tgc.PRIVATE,))

        elif var == 'is_forward':
            value = False
            for name in FORWARDED:
                if getattr(self.tgm, f'forward_{name}', None):
                    value = True
                    break

        elif var == 'chat':
            value = (get_chat(self.dbs, id=self.cid, title=self.tgc.title)
                     if self.tgc and self.is_group else
                     None)

        elif var in ('sanction', 'admission', 'restriction', 'expulsion'):
            method = getattr(self, f'get_{var}s')
            value = (method(chat_id=self.cid, user_id=self.uid)
                     if self.tgc and self.is_group and self.tgu else
                     None)

        else:
            raise AttributeError(f"'{self.__class__.__name__}' "
                                 f"object has no attribute '{var}'")

        self.__dict__[var] = value
        return value


    @flogger
    def reset_cache(self):
        for var in ('cid', 'uid', 'mid', 'text', 'date', 'arg', 'from_bot',
                    'is_group', 'is_private', 'chat',
                    'sanction', 'admission', 'restriction', 'expulsion', 'is_forward'):
            self.__dict__.pop(var, None)


    @flogger
    def is_admin(self, chat_id=None, user_id=None):
        if chat_id:
            all_adm = None
        else:
            chat_id = self.cid
            all_adm = bool(self.tgc.all_members_are_administrators)
        user_id = user_id or self.uid
        try:
            if any(user_id == user.id for user in get_admins(self.bot, chat_id)):
                return True
            if all_adm is None:
                return all_members_are_administrators(self.bot, chat_id)
            return all_adm

        except TelegramError as tge:
            logger.warning('is_admin(cid=%d, uid=%d): %s', chat_id, user_id, tge)
            return False


    @flogger
    def get_admins(self, chat_id=None, with_bots=False):
        chat_id = chat_id or self.cid
        try:
            adms = get_admins(self.bot, chat_id)
            if with_bots:
                return adms
            return [adm for adm in adms if not adm.is_bot]

        except TelegramError as tge:
            logger.warning('get_admins(cid=%d): %s', chat_id, tge)
            return []


    @flogger
    def get_tgc_and_chat(self, chat_id):
        tgc = self.bot.get_chat(chat_id=chat_id)
        chat = get_chat(self.dbs, id=tgc.id, title=tgc.title)
        return tgc, chat


    @flogger
    def wait_another(self, chat_id, context):
        now = time.time()
        another_at = (self.cache
                      .setdefault(self.uid, {})
                      .setdefault(chat_id, {})
                      .setdefault('another_at', {})
                      .get(context))
        if another_at is None or another_at + CFG.WAIT_FOR_ANOTHER < now:
            self.cache[self.uid][chat_id]['another_at'][context] = now
            return False
        return True


    def get_sanctions(self, chat_id=None, user_id=None):
        return get_sanctions(self.dbs, chat_id, user_id)


    def get_admissions(self, chat_id=None, user_id=None, with_pcap=False):
        return get_admissions(self.dbs, chat_id, user_id, with_pcap)


    def get_restrictions(self, chat_id=None, user_id=None):
        return get_restrictions(self.dbs, chat_id, user_id)


    def get_expulsions(self, chat_id=None, user_id=None):
        return get_expulsions(self.dbs, chat_id, user_id)



class ConfigMenu:


    def __init__(self):
        langs = tuple(get_available_langs())
        strikes = tuple(CFG.STRIKES_LIST)

        logger.debug('available languages: %s', langs)
        logger.debug('available strikes limits: %s', strikes)

        enabler = (True, False)
        organizer = ('A-Z', 'Z-A', 'Random')
        greet_give_next = lambda value: 'GREET_TIMER' if value else None
        greet_admin_show_next = lambda value: 'GREET_ADMIN_CITE' if value else None

        item = collections.namedtuple('Item', 'opts prev next')

        self.data = {
            'LANGUAGE': item(langs, None, 'MAX_STRIKES'),
            'MAX_STRIKES': item(strikes, 'LANGUAGE', 'CALL_ADMINS'),
            'CALL_ADMINS': item(enabler, 'MAX_STRIKES', 'GREET_GIVE'),
            'GREET_GIVE': item(enabler, 'CALL_ADMINS', greet_give_next),
            'GREET_TIMER': item(enabler, 'GREET_GIVE', 'GREET_ADMIN_SHOW'),
            'GREET_ADMIN_SHOW': item(enabler, 'GREET_TIMER', greet_admin_show_next),
            'GREET_ADMIN_CITE': item(enabler, 'GREET_ADMIN_SHOW', 'GREET_ADMIN_ORDER'),
            'GREET_ADMIN_ORDER': item(organizer, 'GREET_ADMIN_CITE', None),
        }
        self.null = item((), None, None)
        self.init = next(iter(self.data))


    def __repr__(self):
        return f'«{self.__class__.__name__}»'


    @flogger
    def get_prev(self, item):
        return self.data.get(item, self.null).prev


    @flogger
    def get_next(self, item, value=None):
        next_item = self.data.get(item, self.null).next
        if next_item:
            if callable(next_item):
                next_item = next_item(value)
            if next_item:
                return next_item
        return None


    @flogger
    def get_menu(self, item, value, txt):
        text = txt.CURRENT.format(subject=txt[item],
                                  value=self.to_text(value, txt))

        options = self.data[item].opts
        main_kb = [(self.to_text(i, txt), f'config:{item}:{i}') for i in options]

        footer_kb = []

        prev_item = self.get_prev(item)
        if prev_item:
            footer_kb.append(('⬅️', f'config:prev:{prev_item}'))

        footer_kb.append(('🆗', 'config:stop:'))

        next_item = self.get_next(item, value)
        if next_item:
            footer_kb.append(('➡️', f'config:{item}:'))

        markup = get_keyboard(main_kb, footer_kb)
        return text, markup


    @flogger
    def mapper(self, item, value):
        options = self.data[item].opts
        value = mapper(options[0], value)
        if value in options:
            return value
        raise ValueError(f'{value} is not valid for {item}')


    @staticmethod
    def to_text(value, txt):
        if isinstance(value, bool):
            return txt.YES if value else txt.NO

        if value == 0:
            return txt.DISABLED

        if value is None:
            return '—'

        return str(value)



class Contextualizer:


    def __init__(self):
        self._locks = collections.defaultdict(threading.Lock)
        self.config = ConfigMenu()
        self.dbsmk = None
        self.trans = {}
        self.cache = {}


    def __repr__(self):
        return f'«{self.__class__.__name__}»'


    def __call__(self, func):
        @functools.wraps(func)
        def __call___wrapper(*args, **kwargs):
            result = None

            # In case of error args[1] can be None
            update = None
            job = None
            cid = None
            lang = None

            if isinstance(args[1], Update):
                update = args[1]
                cid = update.effective_chat.id
                lang = update.effective_user.language_code

            elif isinstance(args[1], Job):
                job = args[1]
                cid = job.context[0].id

            start = time.time()
            with self._locks[cid]:
                logger.debug('%s wait %.3f seconds', func.__name__, time.time() - start)
                logger.debug('db open')
                dbs = self.dbsmk()
                try:
                    if cid and not lang:
                        lang = get_chat_lang(dbs, cid)

                    kwargs['bot'] = args[0]
                    kwargs['update'] = update
                    kwargs['job'] = job
                    kwargs['dbs'] = dbs
                    kwargs['txt'] = self.get_translator(lang)
                    kwargs['cfg'] = self.config
                    kwargs['cache'] = self.cache
                    kwargs['error'] = args[2] if len(args) > 2 else None
                    ctx = Context(kwargs)

                    logger.debug('go to %s', func.__name__)
                    result = func(ctx)
                except:
                    logger.exception('db rollback')
                    dbs.rollback()
                    raise
                else:
                    logger.debug('db commit')
                    dbs.commit()
                finally:
                    logger.debug('db close')
                    dbs.close()
            return result
        return __call___wrapper


    @flogger
    def initialize(self, create_all=True, url=None):
        self.dbsmk = get_session_maker(create_all=create_all, url=url)

        try:
            trans = Translator(CFG.DEFAULT_LANG)
            self.trans[CFG.DEFAULT_LANG] = trans

        except FileNotFoundError:
            logger.warning('default language (%s) not found', CFG.DEFAULT_LANG)

            trans = Translator('en')
            self.trans['en'] = trans

        self.trans['default'] = trans


    @flogger
    def get_translator(self, lang):
        try:
            return self.trans[lang]
        except KeyError:
            try:
                trans = Translator(lang)
            except (FileNotFoundError, AttributeError):
                logger.debug('language (%s) not found', lang)
                trans = self.trans['default']

            self.trans[lang] = trans
            return trans
