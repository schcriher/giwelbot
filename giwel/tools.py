# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import os
import re
import math
import html
import time
import hmac
import base64
import string
import random
import hashlib
import secrets
import itertools
import unicodedata

from unidecode import unidecode

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup
from telegram.utils.helpers import mention_html


CHARACTERS = string.digits + string.ascii_letters
SECRET_PHRASE = ''.join(secrets.choice(CHARACTERS) for _ in range(9)).encode()
BASE64_ALTCHARS = ''.join(secrets.choice(CHARACTERS) for _ in range(2)).encode()

AT = '@\N{ZERO WIDTH NO-BREAK SPACE}'

SPACE_CHARS = ('\u0020\u00a0\u2002\u2003\u2004\u2005\u2006'
               '\u2007\u2008\u2009\u200a\u202f\u205f\u3000')

ZW_SPACE_CHARS = '\u200b\ufeff'  # ZERO WIDTH SPACE, ZERO WIDTH NO-BREAK SPACE

INVISIBLE_CHARS = '\u2061\u2062\u2063\u2064'

# string.whitespace
#    0x0020
# \t 0x0009
# \n 0x000a
# \v 0x000b
# \f 0x000c
# \r 0x000d
NOVIS_CHARS = string.whitespace + INVISIBLE_CHARS + ZW_SPACE_CHARS + SPACE_CHARS[1:]

GREETINGS_RE = re.compile(r'''
\b(
    [bv]i?[eo]n[bv]en·|     # Spanish, Italian, French, Esperanto
    w[ei]l+[ck]om+·|        # English, German, Dutch
    greetings?|             # English
    begr.[sß]+[eu]·|        # German
    ac+oglien[zs]a|         # Italian
    salutiamo|              # Italian
    b[eou]·v[ei]n[id]·|     # Portuguese, Romanian    
    acolhimento|            # Portuguese
    wita·|                  # Polish
    pozhalovat|             # Russian
    ·rivetstvui·|           # Russian
    schastlivo|             # Russian
    kalos|                  # Greek
    vitejte                 # Czech
)\b
'''.replace('·', r'[^\s]+?'), re.VERBOSE | re.IGNORECASE)

STARTS_WITH_SPACE = re.compile(f'^[{string.whitespace}{SPACE_CHARS[1:]}].*$', re.DOTALL)
NOVIS_CHARS_RE = re.compile(f'[{NOVIS_CHARS}]+')
NO_WORD_RE = re.compile(r'[^\w]+')
ADMINS_RE = re.compile(r'(^|\s+)@admins?(\s+|$)', re.IGNORECASE)
SPACES_RE = re.compile(r'\s+')
LANG_RE = re.compile(r'^(?P<lll_CC>[a-z]{2,3}(_[A-Z]{2})?)([.:@].*)?$')
SLIM_RE = re.compile(r'[fiIjlrt\s()[\]{},.;:·]+')
LIST_RE = re.compile(r'\s*;\s*')


def get_type(obj):
    return obj if obj.__class__ is type else obj.__class__


def to_int(val):
    return int(float(val))  # allow the use of floats 1.0 → 1


def mapper(obj, val):
    decode = get_type(obj)

    if decode is bool:
        val = val.lower()
        if val in ('true', '1', 'yes'):
            return True
        if val in ('false', '0', 'no'):
            return False
        raise ValueError(f'Incorrect value for a boolean variable: {val}')

    if decode is int:
        return to_int(val)

    if decode is list:
        decode = get_type(obj[0])  # all items must be of the same type
        if decode is int:
            decode = to_int
        return [decode(item) for item in LIST_RE.split(val)]

    return decode(val)


def _name(name):
    return (name or '').strip(NOVIS_CHARS)


def _name_gt(name, min_length):
    name = _name(name)
    return name if len(name) >= min_length else ''


def get_full_name(user):
    return f'{_name(user.first_name)} {_name(user.last_name)}'.strip()


def get_part_name(user, min_length):
    name = _name_gt(user.first_name, min_length) or _name_gt(user.last_name, min_length)
    if len(name) >= min_length:
        return name
    return get_full_name(user)


def get_mention(user, full=True, preferably_username=False, cite=True):
    name = get_full_name(user) if full else get_part_name(user, 2)
    username = _name(user.username)

    if preferably_username and username:
        mention = f'{AT}{username}'
    else:
        if full and name and username:
            mention = f'{name} ({AT}{username})'

        elif name:
            mention = f'{name}'

        elif username:
            mention = f'{AT}{username}'

        else:
            mention = f'ID:{user.id}'

    return mention_html(user.id, mention) if cite else html.escape(mention)


def get_arg(text, cut=None):
    if text:
        parts = text.split(cut, 1)
        if len(parts) == 2:
            cmd, arg = parts
            if cmd.startswith('/') and len(cmd) > 1 and arg.strip() != '':
                return arg.lstrip(' ').rstrip()
    return ''


def get_token():
    '''Generate a token ensuring that it does not repeat.'''
    stamp = int(time.time() * 1e9).to_bytes(9, byteorder='big')
    digest = hashlib.blake2b(stamp, key=SECRET_PHRASE, digest_size=15).digest()
    token = base64.b64encode(digest, altchars=BASE64_ALTCHARS).decode()
    return token.rstrip('=')


def change_seed(num=None):
    '''Random seed change, improves randomness.'''
    seed = time.time() + int.from_bytes(os.urandom(4), byteorder='big')
    random.seed(seed + float(num or 0))


def signature(data, key, size):
    '''Calculates the signature of the data.'''
    obj = hashlib.blake2b(key=key, digest_size=size)
    obj.update(data)
    return obj.digest()


def verify_signature(data, sign, key, size):
    '''Verify the signature.'''
    data_sign = signature(data, key, size)
    return hmac.compare_digest(data_sign, sign)


def remove_diacritics(text):
    '''Remove the Mark and Nonspacing characters from the text.'''
    nfkd = unicodedata.normalize('NFKD', text)
    return ''.join(c for c in nfkd if unicodedata.category(c) != 'Mn')


def sorted_alphabetically(text):
    '''Sorted alphabetically.'''
    return unidecode(remove_diacritics(text.lower()))


def pseudo_text_length(text):
    '''Counts the number of characters without considering mark and nonspacing,
       and only ~50% of the slim letters: space, "f", "i", "I", "j", "l", "r", "t"
       "(", ")", "[", "]", "{", "}", ",", ".", ";", ":", "·"
    '''
    text = unicodedata.normalize('NFKD', text).strip()
    slim = sum(o.end() - o.start() for o in SLIM_RE.finditer(text)) // 2
    norm = len([1 for c in SLIM_RE.sub('', text) if unicodedata.category(c) != 'Mn'])
    return slim + norm


def get_indexes(texts, kb_wide, kb_isep):
    '''Find the best item layout for the texts on the keyboard, does not reorder.'''
    # pylint: disable=too-many-locals
    kb_wide_limit = 0.86 * kb_wide

    all_lengths = tuple(pseudo_text_length(t) for t in texts)
    num_all = len(all_lengths)

    if sum(all_lengths) + kb_isep * (num_all - 1) < kb_wide_limit:
        # All enters in a single row
        return [slice(0, num_all)]

    kb_factor = math.inf
    kb_indexes = None

    # Without long item
    lengths = tuple(i for i in all_lengths if i < kb_wide_limit)
    num_len = len(lengths)
    lengths_span = (max(lengths) - min(lengths)) / num_len

    num_long = num_all - num_len

    # Empirical estimation of the number of optimal rows (without long item)
    target_rows = math.ceil(1.1 * (sum(lengths) + kb_isep * (num_all - 1)) / kb_wide)

    # Estimation of the maximum number of items per row
    lim_items = math.ceil(num_len / target_rows + 1.1) + 1

    # Add the number of long elements
    target_rows += num_long

    for num_row in (target_rows - 1, target_rows, target_rows + 1):

        # Search from 1 item per row, to support very long items
        for item_per_row in itertools.product(range(1, lim_items + 1), repeat=num_row):

            # All items must be on the keyboard (no more, no less)
            if sum(item_per_row) != num_all:
                continue

            # Start and end indexes for each row
            indexes = [slice(end - lot, end)
                       for lot, end in zip(item_per_row,
                                           itertools.accumulate(item_per_row))]

            chars_list = []
            for idx, lot in zip(indexes, item_per_row):
                chars = sum(all_lengths[idx])
                excess = kb_wide - chars - kb_isep * (lot - 1)
                if excess < 0:
                    if lot > 1:
                        # Invalid combination: very long item must be alone in a row
                        chars_list = None
                        break
                else:
                    chars_list.append(chars)

            if chars_list:

                chars_span = max(chars_list) - min(chars_list)
                lot_span = max(item_per_row) - min(item_per_row)
                lot_importance = 3 * lengths_span + 0.01
                factor = num_row * (9 + chars_span + lot_span / lot_importance)

                if factor < kb_factor:
                    kb_factor = factor
                    kb_indexes = indexes

    return kb_indexes


def set_inlinekeyboardbutton(items, texts, objs):
    for item in items or []:
        text = item[0]
        data = item[1]
        url = item[2] if len(item) > 2 else None
        texts.append(text)
        objs.append(InlineKeyboardButton(text=text, callback_data=data, url=url))


def get_keyboard(main, footer=None, **kwargs):
    '''Generates the keyboard object for the items in main and footer.'''

    if isinstance(main[0], tuple):
        kb_wide = 30
        kb_isep = 5
        main_texts = []
        main_objs = []
        footer_texts = []
        footer_objs = []
        set_inlinekeyboardbutton(main, main_texts, main_objs)
        set_inlinekeyboardbutton(footer, footer_texts, footer_objs)
        make = InlineKeyboardMarkup
    else:
        kb_wide = 38
        kb_isep = 6
        main_texts = list(main)
        main_objs = main_texts
        footer_texts = list(footer or [])
        footer_objs = footer_texts
        make = ReplyKeyboardMarkup

    indexes = get_indexes(main_texts, kb_wide, kb_isep)
    keyboard = [main_objs[idx] for idx in indexes]

    if footer_objs:
        indexes = get_indexes(footer_texts, kb_wide, kb_isep)
        keyboard.extend(footer_objs[idx] for idx in indexes)

    return make(keyboard, **kwargs)
