# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import os
import hashlib
import datetime

from giwel.tools import mapper

# ℹ️ Item separator in lists: ";"

DEFAULT = {
    'TELEGRAM_USERNAME': str,   # bot username
    'TELEGRAM_TOKEN': str,      # bot token
    'TELEGRAM_FID': int,        # identified bot maintainer (father)
    'TELEGRAM_DID': str,        # chat to inform actions (debugging)

    'DATABASE_URL': str,        # dialect[+driver]://user:pass@host/dbname[?key=value..]

    'HOST': str,                # host server
    'PORT': int,                # typical: 443
    'BIND': str,                # typical: '0.0.0.0'

    'BOT_PATH': 'bot',          # telegram requests go to HOST:PORT/BOT_PATH

    'DATETIME_IN_LOG': True,
    'DATETIME_FORMAT': '%Y%m%d.%H%M%S',
    'LOG_TIME': '%(asctime)s.%(msecs)03d',
    'LOG_LINE': '%(levelname)-8s %(threadName)-10.10s %(name)-15s %(lineno)-4d '
                '%(message)s',

    'I18N_LOCALE': 'locale',    # \ «LOCALE_DIR»/«LANG»/LC_MESSAGES/«DOMAIN».{po,mo}
    'I18N_DOMAIN': 'giwel',     # / «LOCALE_DIR»/«DOMAIN».pot
    'DEFAULT_LANG': 'en',       # used in case the group does not have one set

    'CAPTCHA_ANSWERS': 6,       # number of answers to be displayed
    'CAPTCHA_MAX_NUM': 9,       # minimum and maximum number to be used
    'CAPTCHA_MIN_SEP': 2,       # minimal separation between random and correct answers

    'SIGN_DIGEST_SIZE': 32,     # size in byte of signature
    'WAIT_FOR_ANOTHER': 10.0,   # wait in seconds to get something (e.g. new captcha)

    'GAP_NOW_TIME': 3.0,                # [ 3s] time to do "now" (but not really now)
    'JOB_DELAY_TIME': 30.0,             # [30s] time delay in the execution of job load
    'CACHING_ADM_TIME': 600.0,          # [10m] time of cache for admin information

    'CAPTCHA_TIMER': 300.0,             # [ 5m] time solve the captcha
    'GREETING_TIMER': 600.0,            # [10m] time greetings are given
    'TEMPORARY_RESTRICTION': 1800.0,    # [30m] time restriction of text only
    'BANNED_RESTRICTION': 172800.0,     # [ 2d] time restriction on reattempting entry

    'EXPULSION_HISTORY_DAYS': 90.0,     # days of storage of expulsion

    'STRIKES_LIST': [1, 2, 3, 4, 5, 0], # strike limits
}


class Configuration:

    @property
    def log_format(self):
        if self.DATETIME_IN_LOG:
            return f'{self.LOG_TIME} {self.LOG_LINE}'
        return self.LOG_LINE

    @property
    def bot_url(self):
        return f'https://t.me/{self.TELEGRAM_USERNAME}'

    @property
    def bid(self):
        try:
            return self.TELEGRAM_BID
        except KeyError:
            bid = int(self.TELEGRAM_TOKEN.split(':')[0])
            self.__dict__['TELEGRAM_BID'] = bid
            return bid

    @property
    def fid(self):
        return self.TELEGRAM_FID

    @property
    def did(self):
        return self.TELEGRAM_DID

    @property
    def signature_key(self):
        try:
            return self.SIGNATURE_KEY
        except KeyError:
            key = f'§{self.TELEGRAM_TOKEN}§{self.bot_url}§'.encode()
            key = hashlib.blake2b(key, digest_size=64).digest()
            self.__dict__['SIGNATURE_KEY'] = key
            return key

    def __getattr__(self, var):
        default = DEFAULT.get(var)
        environ = os.getenv(var)

        if default is None:
            raise KeyError(f'No default value or variable type for {var}')

        if environ is None:
            if default.__class__ is type:
                raise KeyError(f'{var} must be set in the environment variables')
            value = default
        else:
            value = mapper(default, environ)

        if var.endswith('_TIMER') or var.endswith('_RESTRICTION'):
            value = datetime.timedelta(seconds=value)

        if var.endswith('_DAYS'):
            value = datetime.timedelta(days=value)

        self.__dict__[var] = value
        return value

CFG = Configuration()
