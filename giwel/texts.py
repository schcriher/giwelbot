# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import string
import gettext
import datetime

from giwel.debug import flogger
from giwel.config import CFG

_ = lambda message: message                         # gettext
__ = lambda singular, plural: (singular, plural)    # ngettext


TIMES = {
    'CAPTCHA_TIMER': CFG.CAPTCHA_TIMER,
    'GREETING_TIMER': CFG.GREETING_TIMER,
    'TEMPORARY_RESTRICTION': CFG.TEMPORARY_RESTRICTION,
    'BANNED_RESTRICTION': CFG.BANNED_RESTRICTION,
}

TEXTS = {
    # TRANSLATORS: here is the name of the language of the translation
    # written in that language (is not the translation of "english")
    'LANG': _('english'),

    # TRANSLATORS: this is a generic list delimit char, e.g.: "foo, bar"
    'LIST_SEP': _(', '),
    # TRANSLATORS: this is the last part of a list, e.g.: "foo, bar and baz"
    'LIST_END': _('{} and {}'),

    # TRANSLATORS: to make a list of times to wait to interact with a chat
    # e.g.: "• 1 minute and 30 seconds for “Python Group”"
    'LIST_ITEM': _('• {delta:s} for “{chat:s}”'),

    'YES': _('Yes'),
    'NO': _('No'),

    'ENABLED': _('Enabled'),
    'DISABLED': _('Disabled'),

    # TRANSLATORS: to set the number of days of a time lapse,
    # e.g.: "1 day, 2 hours, 3 minutes and 4 seconds"
    'DAYS': __('{:g} day', '{:g} days'),
    # TRANSLATORS: to set the number of hours of a time lapse,
    # e.g.: "1 day, 2 hours, 3 minutes and 4 seconds"
    'HOURS': __('{:g} hour', '{:g} hours'),
    # TRANSLATORS: to set the number of minutes of a time lapse,
    # e.g.: "1 day, 2 hours, 3 minutes and 4 seconds"
    'MINUTES': __('{:g} minute', '{:g} minutes'),
    # TRANSLATORS: to set the number of seconds of a time lapse,
    # e.g.: "1 day, 2 hours, 3 minutes and 4 seconds"
    'SECONDS': __('{:g} second', '{:g} seconds'),

    'HELP': _('<b>GiWelBot: Give Welcome Bot</b>\n'
              '\n'
              'I am a welcome bot and have basic anti-spam tools such as captcha '
              'resolution and keyword identification in names and texts.\n'
              '\n'
              'You can configure (/config) the following parameters:\n'
              '\n'
              '• Welcome language, the captcha and the configuration menu will be '
              'displayed in the user\'s language if available.\n'
              '• Maximum number of spam penalties for banning a user.\n'
              '• Cite the administrators when someone writes @admin or @admins.\n'
              '\n'
              '• Welcome new users.\n'
              '• Delay of {GREETING_TIMER:s} in welcoming to allow cancellation '
              'if another user gives the greeting.\n'
              '\n'
              '• Show the list of administrators at the welcome.\n'
              '• Cite the administrators at the welcome.\n'
              '• Order of the list of administrators in the welcome.\n'
              '\n'
              'You can also add a note (/set_greet_note <i>text</i>) to the welcome. '
              'Please note that the welcome text plus the note text and the '
              'administrator list text (if enabled) cannot exceed 4096 characters.\n'
              '\n'
              'Welcome are grouped if there is no activity in the group. '
              'An accepted user will have to wait {TEMPORARY_RESTRICTION:s} to be able '
              'to share audios, documents, photos, videos, GIFs, stickers, '
              'web page previews and texts with URLs. '
              '\n'
              'If a captcha is answered incorrectly, one more captcha can be solved '
              'in private. '
              'A banned user must wait {BANNED_RESTRICTION:s} before they can try to '
              're-enter unless an administrator manually unbanned them in the group '
              '(a telegram management function).\n'
              '\n'
              '⚠️ The bot must have permissions to delete messages and suspend users.\n'
              '\n'
              'By @schcriher'),

    'HELP_WAIT': _('⛔️ Wait a few seconds to show the help again.'),

    'GROUPS_ONLY': _('⛔️ This is a function for private use only '
                     '(not in groups or supergroups).'),

    'PRIVATE_ONLY': _('⛔️ This is a function to be used only in groups or supergroups '
                      '(not in private).'),

    # TRANSLATORS: "mention" is the link of one or more members,
    # the number "n" -singular/plural- is only used to choose the text
    # but not used within the text
    'GREETING': __('¡We welcome you, {mention:s}!',
                   '¡We welcome you, {mention:s}!'),

    # Texts for the configuration menu
    'CURRENT': _('{subject:s}\nCurrent: {value:s}'),
    'STOP_CONFIG': _('Finished configuration.'),
    'NOT_STARTED_CONFIG': _('⛔️ You have not started this configuration.'),

    'LANGUAGE': _('Choose the language for the bot:'),
    'MAX_STRIKES': _('Choose the limit of strikes for expulsion:'),
    'CALL_ADMINS': _('Activate the call to the administrators with @admin?'),
    'GREET_GIVE': _('Giving a welcome to new members?'),
    'GREET_TIMER': _('Activate the welcome delay? (allows canceling the greeting).'),
    'GREET_ADMIN_SHOW': _('Show the list of administrators at the end of the welcome?'),
    'GREET_ADMIN_CITE': _('Citing administrators on the welcome list?'),
    'GREET_ADMIN_ORDER': _('In what order do I show the administrators at the welcome?'),

    'GREET_NOTE_SET': _('Welcome note established.'),
    'GREET_NOTE_UNSET': _('Welcome note removed.'),

    'ADMIN_LIST': _('List of administrators:'),

    'GREET_ERROR': _('⛔️ Error sending the welcome, check the configured note '
                     '(may be too long or contain html errors).'),

    # TRANSLATORS: "mention" is the link of one or more admin,
    # the number "n" -singular/plural- is only used to choose the text
    # but not used within the text
    'QUOTE_ADMINS': __('The presence of the admin is required here: {mention:s}',
                       'The presence of the admins is required here: {mention:s}'),

    'NEW_CAPTCHA': _('Get another captcha'),

    'CHANCE_CAPTCHA': _('Private chat'),

    'CAPTCHA': _('Please {user:s}, solve the following captcha '
                 '(a simple math operation!):\n'
                 '\n'
                 '{captcha:s}\n'
                 '\n'
                 'The result is:'),

    'CAPTCHA_SOLVED_GROUP': _('✅ Captcha correct {user:s}. '
                              'Now you can send only text without URLs for '
                              '{TEMPORARY_RESTRICTION:s}.'),

    'CAPTCHA_SOLVED_PRIVATE': _('✅ Captcha correct.'),

    'CAPTCHA_SOLVED_ALERT': _('Correct answer'),

    'CAPTCHA_WAIT_ALERT': _('Wait a few seconds to order another captcha.'),

    # TRANSLATORS: this is {CAN_NOT_USE}
    'CAN_NOT_USE': _('You will not be able to use the group, '
                     'contact an administrator if you feel it is a mistake. '
                     'You can try again in {BANNED_RESTRICTION:s}.'),

    'CAPTCHA_WRONG_GROUP': _('⛔️ Wrong Captcha {user:s}. In private with '
                             '<code>/start</code> you can solve another captcha.'),

    'CAPTCHA_WRONG_PRIVATE': _('⛔️ Wrong Captcha. {CAN_NOT_USE:s}'),

    'CAPTCHA_WRONG_ALERT': _('Wrong answer'),

    'CAPTCHA_TIMEOUT_PRIVATE': _('⛔️ Time to solve the captcha is over. '
                                 '{CAN_NOT_USE:s}'),

    'CAPTCHA_LEAVE_PRIVATE': _('⛔️ You are no longer a member of this group. '
                               '{CAN_NOT_USE:s}'),

    'MENU_SOLVE': _('You want to solve a captcha?'),

    'MENU_WAIT': _('You have groups with failing captchas, '
                   'but you must wait to retry:\n{chat_list:s}'),

    'MENU_NOTHING': _("You don't have any captcha pending."),

    'MENU_CHOOSE': _('Choose the group to solve the captcha. '
                     'You can stop the process with /cancel.'),

    'PROCESSING': _('Processing: {item:s}'),

    'CANCELLED': _('⚠️ Operation cancelled, you can start again with /start.'),

    'ONLY_ADMIN': _('🛡 This is a function for administrators only.'),

    'INCORRECT': _('⛔️ Wrong option, retry (use keyboard).'),

    'WAS_AN_ERROR': _('⛔️ An error occurred, restart the process with /start.'),

    'ERROR_HAPPENED': _('⛔️ An error has occurred trying to reply to you 😭 '
                        'my developer will be notified 😊 sorry for the inconvenience.'),

    'STRIKE_WARNING': _('⛔️ {user:s} I deleted his message because it contains spam '
                        '[strike {strike:d} of {max_strikes:d}].'),
}


class InexistentFieldUnreplacementFormatter(string.Formatter):

    def __init__(self, get_attr):
        super().__init__()
        self.inexistent = False
        self._get_attr = get_attr

    def __repr__(self):
        return '«Formatter»'

    @flogger
    def get_value(self, key, args, kwargs):
        self.inexistent = False
        if isinstance(key, int):
            if key < len(args):
                return args[key]
        else:
            value = self._get_attr(key)
            if value is not None:
                return value
        self.inexistent = True
        return key

    @flogger
    def format_field(self, value, format_spec):
        if self.inexistent:
            if format_spec:
                format_spec = ':' + format_spec
            return f'{{{value}{format_spec}}}'
        return super().format_field(value, format_spec)

    @flogger
    def convert_field(self, value, conversion):
        if self.inexistent:
            if conversion is None:
                return value
            self.inexistent = False
            if conversion:
                conversion = '!' + conversion
            return f'{{{value}{conversion}}}'
        return super().convert_field(value, conversion)


class Translator:

    # https://www.gnu.org/software/gettext/manual/gettext.html
    # https://mail.gnome.org/archives/gnome-i18n/2004-July/msg00073.html

    lang = None

    def __init__(self, lang):
        self.lang = lang
        self.cache = {}
        self.trans = gettext.translation(domain=CFG.I18N_DOMAIN,
                                         localedir=CFG.I18N_LOCALE,
                                         languages=[lang])

    def __repr__(self):
        return f'«{self.__class__.__name__}:{self.lang}»'

    def __getitem__(self, var):
        return getattr(self, var)

    @flogger
    def __getattr__(self, var):
        text = TEXTS[var]

        if isinstance(text, tuple):
            # For plurals
            value = lambda n: self.pre_format(self.trans.ngettext(*text, int(n)), n)
        else:
            # For singular
            value = self.pre_format(self.trans.gettext(text))

        self.__dict__[var] = value
        return value

    @flogger
    def get_attr(self, key):

        if key in TEXTS:
            val = getattr(self, key)  # request the possibly cached value
            if callable(val):
                raise TypeError(f'Cannot nest plurals expressions ({key})')

        elif key in self.cache:
            val = self.cache[key]

        elif key in TIMES:
            val = self.sec_to_text(TIMES[key])
            self.cache[key] = val

        else:
            val = None

        return val

    @flogger
    def pre_format(self, tex, num=None):
        formatter = InexistentFieldUnreplacementFormatter(self.get_attr)
        return formatter.vformat(tex, (num,) if num is not None else (), {})

    @flogger
    def sec_to_text(self, sec):
        # May not be correct in all languages

        if isinstance(sec, datetime.timedelta):
            sec = sec.total_seconds()

        if abs(sec) < 1:
            return self.SECONDS(sec)

        # Decimals are discarded
        minutes, seconds = divmod(int(sec), 60)
        hours, minutes = divmod(minutes, 60)
        days, hours = divmod(hours, 24)

        phrase = []
        if days:
            phrase.append(self.DAYS(days))
        if hours:
            phrase.append(self.HOURS(hours))
        if minutes:
            phrase.append(self.MINUTES(minutes))
        if seconds:
            phrase.append(self.SECONDS(seconds))

        if len(phrase) == 1:
            return phrase[0]
        return self.LIST_END.format(self.LIST_SEP.join(phrase[:-1]), phrase[-1])
