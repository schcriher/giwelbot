# GiWelBot: Give Welcome Bot

bot that greets new users and have basic anti-spam tools such as captcha resolution
and keyword identification in names and texts.

You can configure (via `/config`) the following parameters:

* Welcome language, the captcha and the configuration menu will be displayed
  in the user's language if available.
* Maximum number of spam penalties for banning a user.
* Cite the administrators when someone writes `@admin` or `@admins`.

* Welcome new users.
* Delay (`GREETING_TIMER`) in welcoming to allow cancellation
  if another user gives the greeting.

* Show the list of administrators at the welcome.
* Cite the administrators at the welcome.
* Order of the list of administrators in the welcome.

You can also add a note (via `/set_greet_note text`) to the welcome.
Please note that the welcome text plus the note text and the
administrator list text (if enabled) cannot exceed 4096 characters.

Welcome are grouped if there is no activity in the group.
An accepted user will have wait a period of time (`TEMPORARY_RESTRICTION`)
to be able to share audios, documents, photos, videos, GIFs, stickers,
web page previews and texts with URLs.

If a captcha is answered incorrectly, one more captcha can be solved in private.
A banned user must wait a period of time (`BANNED_RESTRICTION`) before they can
try to re-enter unless an administrator manually unbanned them in the group
(a telegram management function).

The bot must have permissions to delete messages and suspend users.

Available as [@giwelbot](https://t.me/giwelbot)
