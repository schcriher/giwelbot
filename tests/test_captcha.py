# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import collections

from environ import unittest, mock, captcha, tools, mocked_get_captcha_text


class TestCaptcha(unittest.TestCase):


    def test_get_space(self):
        all_chars = tools.SPACE_CHARS + tools.ZW_SPACE_CHARS
        for index in range(1000):
            chars = captcha.get_space()
            scode = ''.join(f'\\u{ord(c):0>4x}' for c in chars)
            with self.subTest(index=index, chars=scode):
                length = len(chars)
                self.assertGreaterEqual(length, 1)
                self.assertLessEqual(length, 3)
                self.assertTrue(all(c in all_chars for c in chars))
                if length == 1:
                    self.assertNotIn(chars, tools.ZW_SPACE_CHARS)


    def test_get_invisible(self):
        for index in range(100):
            with self.subTest(index=index):
                char = captcha.get_invisible()
                self.assertIn(char, tools.INVISIBLE_CHARS, f'char: 0x{ord(char):0>4x}')


    def test_get_captcha_text(self):
        get_min = lambda items: 3 + 2 * len(items) + sum(len(i) for i in items)
        get_max = lambda items: 5 + 4 * len(items) + sum(len(i) for i in items)
        data = (
            ((), 3, 5),
            (('',), 5, 9),
            (('', ''), 7, 13),
            (('a', '+', 'b'), 12, 20),
            (('10', '*', '123'), 15, 23),
        )
        for items, min_len, max_len in data:
            for index in range(100):
                with self.subTest(items=items, index=index):
                    tex = captcha.get_captcha_text(*items)
                    length = len(tex)
                    self.assertGreaterEqual(length, min_len)
                    self.assertLessEqual(length, max_len)
                    self.assertEqual(min_len, get_min(items))
                    self.assertEqual(max_len, get_max(items))
                    self.assertTrue(all(c in tex for c in items))


    @mock.patch.object(captcha, 'get_captcha_text', mocked_get_captcha_text)
    def test_get_captcha(self):
        # pylint: disable=eval-used,too-many-locals

        positions = []
        rounds = 100
        na_min = 3      # with this value all answers are in second place
        na_max = 8

        for num_answers in range(na_min, na_max + 1):
            for index in range(rounds):
                with self.subTest(num_answers=num_answers, index=index):
                    captex, correct_answer, answers = captcha.get_captcha(num_answers)

                    num_a, operation, num_b = captex.split()
                    num_a = int(num_a)
                    num_b = int(num_b)
                    real_answer = captcha.OPERATORS[operation](num_a, num_b)

                    self.assertNotEqual(num_a, real_answer)
                    self.assertNotEqual(num_b, real_answer)
                    self.assertNotEqual(answers[0], correct_answer)     # never first
                    self.assertNotEqual(answers[-1], correct_answer)    # never last
                    self.assertEqual(len(set(answers)), len(answers))   # no duplicates
                    self.assertEqual(int(real_answer), real_answer)
                    self.assertEqual(str(int(real_answer)), correct_answer)
                    self.assertEqual(len(answers), num_answers)
                    self.assertEqual(real_answer, eval(captex))
                    self.assertIn(correct_answer, answers)

                    positions.append(answers.index(correct_answer))

        counter = collections.Counter(positions)
        self.assertEqual(counter[0], 0)
        for num_answers in range(1, na_max - 1):
            self.assertGreater(counter[num_answers], 0.05 * rounds)
        self.assertEqual(counter[na_max], 0)
