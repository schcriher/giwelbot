# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import sys
import datetime

from telegram import Bot, Chat, User, ChatMember
from telegram.error import BadRequest, ChatMigrated

from environ import unittest, mock, core, texts, CFG, mocked_datetime_now


NOW = datetime.datetime.now()


class FakeBot:
    # pylint: disable=too-few-public-methods

    def __init__(self, error, chat_id):
        self.error = error
        self.chat_id = chat_id
        self.parameters = None

    def restrict_chat_member(self, **parameters):
        if self.error and parameters['chat_id'] == self.chat_id:
            raise self.error
        self.parameters = parameters


class TestCore(unittest.TestCase):


    def test_check_cache(self):
        ctx = mock.Mock()
        lst = []

        @core.check_cache
        def func(ctx):
            lst.append(ctx.unittest_id)

        ctx.uid = 1

        ctx.cache = {1: {'wrongs': {}}}
        ctx.unittest_id = 1
        func(ctx)
        self.assertEqual(lst, [1])
        self.assertEqual(ctx.send.call_args_list, [])

        ctx.cache = {2: {'wrongs': {}}}
        ctx.unittest_id = 2
        func(ctx)
        self.assertEqual(lst, [1])
        self.assertEqual(ctx.send.call_args_list, [mock.call(text=ctx.txt.WAS_AN_ERROR,
                                                             reply_markup=None)])


    def test_cancel_greet_group(self):
        ctx = mock.Mock()
        run = False

        @core.cancel_greet_group
        def func(_):
            nonlocal run
            run = True

        data = (
            (True, 'a', 10),
            (True, None, None),
            (False, 'b', 11),
            (False, None, None),
        )
        for is_group, pgu, pgm in data:
            with self.subTest(is_group=is_group, pgu=pgu, pgm=pgm):
                run = False
                ctx.is_group = is_group
                ctx.chat.prev_greet_users = pgu
                ctx.chat.prev_greet_message_id = pgm

                func(ctx)

                self.assertTrue(run)  # function is always executed
                if is_group and pgu:
                    pgu = None
                    pgm = None
                self.assertEqual(ctx.chat.prev_greet_users, pgu)
                self.assertEqual(ctx.chat.prev_greet_message_id, pgm)


    def test_only_admin(self):
        ctx = mock.Mock()
        lst = []

        call_answer = [mock.call(text=ctx.txt.ONLY_ADMIN)]
        call_reply = [mock.call(text=ctx.txt.ONLY_ADMIN, reply_markup=None)]

        @core.only_admin
        def func_handler(ctx):
            lst.append(ctx.unittest_id)

        @core.only_admin
        def func_cb_handler(ctx):
            lst.append(ctx.unittest_id)

        data = (
            (1, True, func_handler, [], []),
            (2, True, func_cb_handler, [], []),
            (3, False, func_handler, call_reply, []),
            (4, False, func_cb_handler, [], call_answer),
        )
        for tid, admin, func, reply, answer in data:
            with self.subTest(tid=tid, admin=admin, func=func):
                lst.clear()
                ctx.reset_mock()
                ctx.unittest_id = tid
                ctx.is_admin.return_value = admin

                func(ctx)

                self.assertEqual(lst, [tid] if admin else [])
                self.assertEqual(ctx.is_admin.call_args_list, [mock.call()])
                self.assertEqual(ctx.answer.call_args_list, answer)
                self.assertEqual(ctx.reply.call_args_list, reply)


    def test_retry_with_new_chat_id(self):
        lst = []

        @core.retry_with_new_chat_id
        def func(_bot, chat_id, *_args):
            if chat_id == 2:
                raise ChatMigrated(3)
            lst.append(chat_id)

        func(None, 1, 'a')
        self.assertEqual(lst, [1])

        func(None, 2, 'b')
        self.assertEqual(lst, [1, 3])


    def test_restrict_user(self):
        chat_id = 1
        user_id = 2
        until = 3

        data = (
            (core.UserRestriction.FULL, False, False, None),
            (core.UserRestriction.TEMP, True, False, None),
            (core.UserRestriction.NONE, True, True, None),
            (core.UserRestriction.NONE, True, True, BadRequest('error')),
            (core.UserRestriction.NONE, True, True, ChatMigrated(chat_id + 10)),
        )
        for restriction, can_send_messages, can_others, error in data:
            with self.subTest(restriction=restriction, error=error):

                bot = FakeBot(error, chat_id)
                core.restrict_user(bot, chat_id, user_id, restriction, until)

                if isinstance(error, BadRequest):
                    parameters = None
                else:
                    if isinstance(error, ChatMigrated):
                        chat_id = error.new_chat_id

                    parameters = {'chat_id': chat_id, 'user_id': user_id,
                                  'until_date': until,
                                  'can_send_messages': can_send_messages,
                                  'can_send_other_messages': can_others,
                                  'can_send_media_messages': can_others,
                                  'can_add_web_page_previews': can_others}

                self.assertEqual(bot.parameters, parameters)


    @mock.patch('giwel.core.logger')
    def test_ban_user(self, logger):
        # pylint: disable=too-many-locals

        log = 'user=%d in chat=%d %s: %s'
        chat_id = 1
        user_id = 2
        ban_reason = 'reason'
        until = 0

        bot = mock.Mock(spec=Bot)
        chat = mock.Mock(spec=Chat)
        usr1 = mock.Mock(spec=User)
        usr2 = mock.Mock(spec=User)
        adm1 = mock.Mock(spec=ChatMember)
        adm2 = mock.Mock(spec=ChatMember)

        bot.id = 3
        bot.get_chat.return_value = chat
        usr1.id = 3
        usr1.full_name = 'name3'
        usr2.id = 4
        usr2.full_name = 'name4'
        chat.title = 'title'
        adm1.user = usr1
        adm2.user = usr2

        new_chat_id = chat_id + 10
        can_not_ban = 'insufficient permissions or not allowed'

        data = (
            (False, [adm1, adm2], 'ban by', ban_reason, [True]),
            #
            (False, [adm2], 'can not ban', can_not_ban, [True]),
            (True, [adm1], 'can not ban', can_not_ban, [True]),
            (True, [adm2], 'can not ban', can_not_ban, [True]),
            #
            (False, [adm1], 'can not ban', 'bad·req', [BadRequest('bad·req')]),
            (False, [adm1], 'ban by', ban_reason, [ChatMigrated(new_chat_id), True]),
        )
        for all_adm, admins, action, reason, kick in data:
            with self.subTest(all_adm=all_adm, action=action, reason=reason, kick=kick):
                logger.reset_mock()

                chat.all_members_are_administrators = all_adm
                chat.get_administrators.return_value = admins

                chat.kick_member.side_effect = kick

                core.ban_user(bot, chat_id, user_id, ban_reason, until)

                if isinstance(kick[0], ChatMigrated):
                    chat_id = kick[0].new_chat_id

                lst1 = [mock.call(log, user_id, chat_id, action, reason)]
                self.assertEqual(logger.info.call_args_list, lst1)

        logger.reset_mock()
        chat.kick_member.side_effect = [True]
        bot.send_message.side_effect = [BadRequest('bad·req')]
        self.assertEqual(logger.exception.call_count, 0)
        core.ban_user(bot, chat_id, user_id, ban_reason, until)
        self.assertEqual(logger.exception.call_count, 1)


    @mocked_datetime_now(datetime, NOW)
    @mock.patch('giwel.core.ban_user')
    @mock.patch('giwel.core.delete_from_db')
    @mock.patch('giwel.core.Expulsion')
    def test_pass_ban_rules(self, expulsion, delete_from_db, ban_user):

        ctx = mock.Mock()
        user_id = 2
        data = (
            (['test'], True, ''),
            ([], False, 'without name and username'),
            (['Test', 'a' * 40], False, 'long name'),
            (['test.xyz.link'], False, 'name with uri'),
        )
        for names, expected, reason in data:
            with self.subTest(names=names, passed=expected, reason=reason):
                delete_from_db.reset_mock()
                ban_user.reset_mock()
                ctx.reset_mock()

                self.assertEqual(core.pass_ban_rules(ctx, user_id, names), expected)

                if reason:
                    until = datetime.datetime.now() + CFG.BANNED_RESTRICTION
                    lst1 = [mock.call(ctx.bot, ctx.cid, user_id, reason, until)]
                    lst2 = [mock.call(ctx, core.DBDelete.SAN_ADM_RES, user_id=user_id)]
                    lst3 = [mock.call(expulsion(chat_id=ctx.cid,
                                                user_id=user_id,
                                                reason=reason,
                                                until=until))]
                else:
                    lst1 = []
                    lst2 = []
                    lst3 = []
                self.assertEqual(ban_user.call_args_list, lst1)
                self.assertEqual(delete_from_db.call_args_list, lst2)
                self.assertEqual(ctx.dbs.add.call_args_list, lst3)


    def test_send_captcha(self):
        ctx = mock.Mock()
        ctx.txt = texts.Translator('en')
        ctx.txt.trans = texts.gettext.NullTranslations()
        mid = 1
        data = (
            ('a', None, ctx.send),
            ('b', 1, ctx.edit),
        )
        for mention, mid, method in data:
            with self.subTest(mention=mention, message_id=mid):
                ctx.reset_mock()

                correct, another, message_id = core.send_captcha(ctx, mention, mid)
                self.assertNotEqual(correct, another)
                self.assertEqual(message_id, method.return_value.message_id)

                kwparams = method.call_args_list[0][1]
                inline_keyboard = kwparams['reply_markup'].to_dict()['inline_keyboard']

                for token in (correct, another):
                    data = f'captcha:{token}'
                    self.assertTrue(any(any(data == c['callback_data'] for c in row)
                                        for row in inline_keyboard))


    @mock.patch('giwel.core.logger')
    def test_delete_message(self, logger):
        log1 = '%s, mid=%d deleted: %s'
        log2 = '%s, mid=%d: %s'
        log3 = 'group %s SHOULD be migrated to supergroup %s'
        log4 = 'Nothing to do, chat_id=%s, message_id=%s'

        bot = mock.Mock()
        message_id = 3
        text = 'text'
        data = (
            (1, [True], logger.debug),
            (1, [False], logger.debug),
            (1, [BadRequest('bad·req')], logger.warning),
            (1, [ChatMigrated(2), True], logger.debug),
            (None, [True], logger.debug),
        )
        for chat_id, delete, log in data:
            with self.subTest(chat_id=chat_id, delete=delete):
                logger.reset_mock()
                bot.reset_mock()

                bot.delete_message.side_effect = delete
                core.delete_message(bot, chat_id, message_id, text)

                lst = []
                if not chat_id:
                    lst.append(mock.call(log4, chat_id, message_id))
                elif isinstance(delete[0], ChatMigrated):
                    lst.append(mock.call(log3, chat_id, chat_id + 1))
                    lst.append(mock.call(log1, text, message_id, delete[1]))
                elif isinstance(delete[0], BadRequest):
                    lst.append(mock.call(log2, text, message_id, delete[0]))
                else:
                    lst.append(mock.call(log1, text, message_id, delete[0]))

                self.assertEqual(log.call_args_list, lst)


    def test_get_from_db(self):
        ctx = mock.Mock()
        attr_name = 'attr'
        method_name = f'get_{attr_name}s'
        attr = getattr(ctx, attr_name)
        method = getattr(ctx, method_name)
        data = (
            (1, 2, method(chat_id=1, user_id=2)),
            (None, 2, method(chat_id=ctx.cid, user_id=2)),
            (1, None, method(chat_id=1, user_id=ctx.uid)),
            (None, None, attr),
        )
        for chat_id, user_id, ret_expected in data:
            with self.subTest(chat_id=chat_id, user_id=user_id):
                ctx.reset_mock()
                ret = core.get_from_db(ctx, attr_name, chat_id, user_id)
                self.assertEqual(ret, ret_expected)


    @mock.patch('giwel.core.delete_message')
    def test_delete_from_db(self, delete_message):
        # 'chat_id' and 'user_id' are passed to get_from_db()
        # which was already tested in another unit test.
        ctx = mock.Mock()

        data = (
            core.DBDelete.SAN_ADM_RES,
            core.DBDelete.SANCTION,
            core.DBDelete.ADMISSION,
            core.DBDelete.RESTRICTION,
            core.DBDelete.EXPULSION,
            'db_object',
        )
        for delete in data:
            with self.subTest(delete=delete):
                delete_message.reset_mock()
                ctx.reset_mock()

                core.delete_from_db(ctx, delete)

                items = []
                mids = []
                if isinstance(delete, core.DBDelete):
                    if delete is core.DBDelete.SAN_ADM_RES:
                        items.append(getattr(ctx, 'sanction'))
                        items.append(getattr(ctx, 'admission'))
                        items.append(getattr(ctx, 'restriction'))
                    else:
                        items.append(getattr(ctx, delete.name.lower()))

                    if core.DBDelete.ADMISSION in delete:
                        mids.append(getattr(ctx, 'admission').group_captcha.message_id)

                else:
                    items.append(delete)

                self.assertEqual(ctx.dbs.delete.call_args_list,
                                 [mock.call(i) for i in items])

                tex = 'delete captcha message'
                self.assertEqual(delete_message.call_args_list,
                                 [mock.call(ctx.bot, ctx.cid, i, tex) for i in mids])


    @mock.patch('giwel.core.get_mention')
    def test_get_admin_list(self, get_mention):
        lst = ['b', 'a', 'Z', 'c']
        tex = 'ADMIN LIST:'
        ctx = mock.Mock()
        ctx.txt.ADMIN_LIST = tex
        ctx.get_admins.return_value = lst
        data = (
            ('Random', None),
            ('Z-A', '\n\nADMIN LIST:\n• Z\n• c\n• b\n• a'),
            ('A-Z', '\n\nADMIN LIST:\n• a\n• b\n• c\n• Z'),
        )
        for order, expected in data:
            for admin in (True, False):
                with self.subTest(order=order, expected=expected, admin=admin):
                    ctx.chat.greet_admin_show = admin
                    ctx.chat.greet_admin_order = order
                    get_mention.side_effect = lst

                    text = core.get_admin_list(ctx)

                    if not admin:
                        self.assertEqual(text, '')
                    elif expected:
                        self.assertEqual(text, expected)
                    else:
                        self.assertTrue(text.startswith(f'\n\n{tex}\n'))
                        self.assertEqual(text.count('\n'), len(lst) + 2)
                        for item in lst:
                            self.assertIn(item, text)


    @mock.patch('giwel.core.Updater')
    @mock.patch('giwel.core.load_jobs')
    @mock.patch('giwel.core.clean_database')
    @mock.patch('giwel.core.delete_message')
    def test_run(self, delete_message, clean_database, load_jobs, class_updater):
        data = (
            # argv, start_polling, start_webhook
            ([], False, True),
            (['-v'], False, True),
            (['-vv'], False, True),
            (['-vvv'], False, True),
            (['-v', '-v'], False, True),
            (['-v', '-c'], False, True),
            (['-p'], True, False),
            (['-p', '-v'], True, False),
            (['-p', '-c'], True, False),
            (['-p', '-c', '-v'], True, False),
            (['-p', '-c', '-v', '-v'], True, False),
            (['-p', '-c', '-vvvv'], True, False),
        )
        updater = class_updater.return_value
        for argv, start_polling, start_webhook in data:
            with self.subTest(argv=argv):
                with mock.patch.object(sys, 'argv', ['python3'] + argv):
                    updater.reset_mock()
                    exit_status = core.run()
                    self.assertTrue(updater.dispatcher.add_handler.called)
                    self.assertTrue(updater.dispatcher.add_error_handler.called)
                    self.assertEqual(updater.start_polling.called, start_polling)
                    self.assertEqual(updater.start_webhook.called, start_webhook)
                    self.assertTrue(updater.idle.called)
                    self.assertEqual(exit_status, 0)

        with self.subTest(case='load_jobs and clean_database'):
            with mock.patch.object(sys, 'argv', ['python3']):
                data = (
                    (2, [(1, 2), (1, 2), (3, 4)]),
                    (0, [(1, 2), (1, 2), (3, 4)]),
                )
                for njobs, clean in data:
                    with self.subTest(njobs=njobs, clean=clean):

                        updater.reset_mock()
                        load_jobs.reset_mock()
                        clean_database.reset_mock()
                        delete_message.reset_mock()

                        load_jobs.return_value = njobs
                        clean_database.return_value = clean

                        core.run()

                        if njobs:
                            deleted = []
                        else:
                            deleted = [
                                mock.call(updater.bot, 3, 4, 'old admissions'),
                                mock.call(updater.bot, 1, 2, 'old admissions'),
                            ]
                        self.assertEqual(delete_message.call_args_list, deleted)

        CFG.__dict__.clear()

        with self.subTest(case='CAPTCHA_TIMER >= GREETING_TIMER'):
            with self.assertRaises(ValueError):
                CFG.CAPTCHA_TIMER = datetime.timedelta(seconds=2)
                CFG.GREETING_TIMER = datetime.timedelta(seconds=1)
                core.run()

        CFG.__dict__.clear()

        with self.subTest(case='WAIT_FOR_ANOTHER * 3 >= CAPTCHA_TIMER'):
            with self.assertRaises(ValueError):
                CFG.WAIT_FOR_ANOTHER = 1
                CFG.CAPTCHA_TIMER = datetime.timedelta(seconds=3)
                core.run()

        CFG.__dict__.clear()
