# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import string
import secrets

from unidecode import unidecode

from environ import unittest, EmptyGenericClass, control


class TestControl(unittest.TestCase):


    def test_homoglyphs_re(self):
        for key, homoglyphs_re in control.HOMOGLYPHS_RE:
            for index, char in enumerate(homoglyphs_re.pattern[1:-1]):
                with self.subTest(key=key, index=index):
                    self.assertNotEqual(key, unidecode(char))


    def test_simplify_text(self):
        data = (
            (None, True, ''),
            ('Hernán', True, 'hernan'),
            ('schön', True, 'schon'),
            ('пожаловат', True, 'pozhalobat'),
            ('Приветствую', True, 'ppnbetctbyiu'),
            ('Поприветствуйте', True, 'poppnbetctbyite'),
            ('Счастливо', True, 'cchactlnbo'),
            ('Καλώς', True, 'kaloc'),
            ('Test!!!', True, 'test '),
            ('tesᴛ@exampℓe.com', True, 'test example com'),
            ('tesᴛ@exampℓe.com', False, 'test@example.com'),
        )
        for entry, only_word, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(control.simplify_text(entry, only_word), expected)


    def test_ban_rules(self):
        chars = ' ' * 8 + string.ascii_letters
        data = (
            ('ok name', ''),
            ('Name @test', ''),
            ('Name #test cool!', ''),
            ('a' * 39, ''),
            ('a' * 40, 'long name'),
            (''.join(secrets.choice(chars) for _ in range(50)), 'long name'),
            ('marketing', 'fake name'),
            ('𝙈𝙖𝙧𝙠𝙚𝙩𝙞𝙣𝙜!', 'fake name'),
            ('tgmember vip', 'fake name'),
            ('wωw.abc.d', 'name with uri'),
            ('test@abc.d', 'name with uri'),
            ('abc.com', 'name with uri'),
            ('test.xyz.link', 'name with uri'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                for rule, reason in control.BAN_RULES:
                    if rule(entry):
                        self.assertEqual(reason, expected)
                        break
                else:
                    self.assertFalse(bool(expected))


    def test_short_words_re(self):
        data = (
            'i', 'it', 'is',
            'to',
            'a', 'an', 'and',
            'of',
            'me', 'my',
            'the', 'their',
            'you', 'your',
            'he', 'her',
            'she',
            'with', 'without',
            'y', 'yo',
            'el', 'els', 'ella', 'ello', 'ellas', 'ellos',
            'la', 'le', 'lo', 'las', 'les', 'los',
            'esa', 'ese', 'eso', 'esta', 'este', 'esto',
            'con',
            'de',
        )
        for lim in ('', ' ', '-'):
            for entry in data:
                entry = f'{lim}{entry}{lim}'
                with self.subTest(entry=entry):
                    match = control.SHORT_WORDS_RE.search(entry)
                    self.assertTrue(bool(match))


    def test_spammer_re(self):
        data = (
            'tgmember',
            'tgvipmember',
            'telegram marketing',
            'unete comparte grup',
            'unete comparte grupo',
            'unete comparte group',
            'unete comparte groupo',
            'unete comparte supergrup',
            'unete comparte supergrupo',
            'unete comparte supergroup',
            'unete comparte supergroupo',
            'unete comparte channel',
            'unete comparte chanal',
            'unete comparte cannel',
            'unete comparte canal',
            'be part hentai grup',
            'be part hentai grupo',
            'be part hentai group',
            'be part hentai groupo',
            'be part hentai supergrup',
            'be part hentai supergrupo',
            'be part hentai supergroup',
            'be part hentai supergroupo',
            'be part hentai channel',
            'be part hentai chanal',
            'be part hentai cannel',
            'be part hentai canal',
            'join hentai grup',
            'join hentai grupo',
            'join hentai group',
            'join hentai groupo',
            'join hentai supergrup',
            'join hentai supergrupo',
            'join hentai supergroup',
            'join hentai supergroupo',
            'join hentai channel',
            'join hentai chanal',
            'join hentai cannel',
            'join hentai canal',
            'join share grup',
            'join share grupo',
            'join share group',
            'join share groupo',
            'join share supergrup',
            'join share supergrupo',
            'join share supergroup',
            'join share supergroupo',
            'join share channel',
            'join share chanal',
            'join share cannel',
            'join share canal',
            'full chapter hentai',
            'full chapter ahegao',
            'full chapter yuri',
            'full chapter porn',
            'full chapter p0rn',
            'full chapters hentai',
            'full chapters ahegao',
            'full chapters yuri',
            'full chapters porn',
            'full chapters p0rn',
            'full chapters porno',
            'full chapters p0rno',
            'full chapters porn0',
            'full chapters p0rn0',
            'ful chapter hentai',
            'ful chapter ahegao',
            'ful chapter yuri',
            'ful chapter porn',
            'ful chapter p0rn',
            'ful chapters hentai',
            'ful chapters ahegao',
            'ful chapters yuri',
            'ful chapters porn',
            'ful chapters p0rn',
        )
        for entry in data:
            with self.subTest(entry=entry):
                match = control.SPAMMER_RE.search(control.SPACE_RE.sub('', entry))
                self.assertTrue(bool(match))


    def test_get_spam(self):
        ffc1 = EmptyGenericClass(title='title', username='username')
        ffc2 = EmptyGenericClass(title='Join in 𝐓𝐞𝐥𝐞𝐠𝐫𝐚𝐦 𝙈𝙖𝙧𝙠𝙚𝙩𝙞𝙣𝙜', username=None)

        data = (
            (EmptyGenericClass(caption='caption'),
             ''),
            (EmptyGenericClass(caption='✨JOIN AND SHARE THE GROUP✨'),
             'joinsharegroup'),

            (EmptyGenericClass(forward_signature='forward_signature'),
             ''),
            (EmptyGenericClass(forward_signature='fullll chapters of porn!!!'),
             'fullllchaptersporn'),

            (EmptyGenericClass(forward_from_chat=ffc1, text='text'),
             ''),
            (EmptyGenericClass(forward_from_chat=ffc1, text='to be a tgvipmember?'),
             'tgvipmember'),
            (EmptyGenericClass(forward_from_chat=ffc2, text='text'),
             'telegrammarketing'),
            (EmptyGenericClass(forward_from_chat=ffc1, text='to be a tgmember?'),
             'tgmember'),
            (EmptyGenericClass(forward_from_chat=ffc2, text='to be a tgmember?'),
             'telegrammarketing'),

            (EmptyGenericClass(author_signature='author_signature'),
             ''),
            (EmptyGenericClass(author_signature='únete y comparte el canal mas grande'),
             'unetecompartecanal'),

            (EmptyGenericClass(forward_sender_name='forward_sender_name'),
             ''),
            (EmptyGenericClass(forward_sender_name='»»» unete y comparte el canal «««'),
             'unetecompartecanal'),

            (EmptyGenericClass(text='text'),
             ''),
            (EmptyGenericClass(text='full chapters p0rn!!!'),
             'fullchaptersp0rn'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry.__dict__, expected=expected):
                match = control.get_spam(entry)
                self.assertEqual(bool(match), bool(expected))
                if match:
                    self.assertEqual(match.group(), expected)
