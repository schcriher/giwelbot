# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

# pylint: disable=too-many-lines

import re
import gettext
import datetime
import functools
import collections

from telegram.ext import CommandHandler
from telegram.error import TelegramError, ChatMigrated

from environ import (logging, unittest, mock, skipIf, UNTESTED_BOT, AttributeDict,
                     BotIOAdapter, CFG, core, tools, texts, captcha, database)

from tgs import (extract_entities_html, USERS, TelegramServer, USRID, USR1ID, USR2ID,
                 G1ID, G2ID, G3ID, S1ID, S2ID, S3ID)

TXT = texts.Translator('en')
TXT.trans = gettext.NullTranslations()

CAPTCHA = '1 + 2'
ANSWER = '3'
MARKUP = ['1', '2', '3', '4', '5', '6', TXT.NEW_CAPTCHA]

BAN = '{}\nchat: {}\nuser: {}'
BAN_BY = 'ban by: {}\nchat: {}\nuser: {}'
DELETED_BY_SPAM = 'deleted by spam: REmatch={}\nchat: {}\nuser: {}'

WRONG = 'ban by: captcha not resolved in time (CaptchaStatus.WRONG)'
WAITING = 'ban by: captcha not resolved in time (CaptchaStatus.WAITING)'
INSUF_PERMS = 'can not ban: insufficient permissions or not allowed'
UNEXPECTED_ERROR = 'can not ban: Unexpected Error'

DATA = AttributeDict()

for _row in USERS.values():
    if not _row['is_bot']:

        _u = AttributeDict(_row)
        _n = tools.get_mention(_u, full=False)
        _c = tools.get_mention(_u, full=False, preferably_username=True)
        _m = tools.get_mention(_u)
        _f = f'{_u.first_name} {_u.last_name}'

        DATA[_u.username] = AttributeDict()
        DATA[_u.username].n = _n
        DATA[_u.username].c = _c
        DATA[_u.username].m = _m
        DATA[_u.username].f = _f
        DATA[_u.username].captcha = TXT.CAPTCHA.format(user=_m, captcha=CAPTCHA)
        DATA[_u.username].solved = TXT.CAPTCHA_SOLVED_GROUP.format(user=_m)
        DATA[_u.username].g_wrong = TXT.CAPTCHA_WRONG_GROUP.format(user=_m)
        DATA[_u.username].p_wrong = TXT.CAPTCHA_WRONG_PRIVATE
        DATA[_u.username].greeting = TXT.GREETING(1).format(mention=_n)
        DATA[_u.username].ban_wrong = BAN.format(WRONG, '{}', _f)
        DATA[_u.username].ban_waiting = BAN.format(WAITING, '{}', _f)
        DATA[_u.username].ban_insuf_perms = BAN.format(INSUF_PERMS, '{}', _f)

NAV = ['⬅️', '🆗', '➡️']
YES_NO_NAV = [TXT.YES, TXT.NO, *NAV]

MENU = AttributeDict({
    'LANGUAGE': ['es', 'en', *NAV[1:]],
    'MAX_STRIKES': ['1', '2', '3', '4', '5', TXT.DISABLED, *NAV],
    'CALL_ADMINS': YES_NO_NAV,
    'GREET_GIVE': YES_NO_NAV,
    'GREET_TIMER': YES_NO_NAV,
    'GREET_ADMIN_SHOW': YES_NO_NAV,
    'GREET_ADMIN_CITE': YES_NO_NAV,
    'GREET_ADMIN_ORDER': ['A-Z', 'Z-A', 'Random', *NAV[:-1]],
})

S1_ADM_LST = f'\n\n{TXT.ADMIN_LIST}\n• @\ufeffadm'

G3_MENU_WAIT = TXT.MENU_WAIT.format(chat_list=TXT.LIST_ITEM.format(
    delta='1 day, 23 hours, 59 minutes and 59 seconds', chat='G3'))

PLURAL_GREET = lambda text: TXT.GREETING(2).format(mention=text)


# ---------------------------------


def mocked_get_captcha(num_answers):
    captcha.get_captcha(num_answers)  # run this line for coverage
    return CAPTCHA, ANSWER, MARKUP[:-1]


def get_menu(var, val):
    try:
        val = getattr(TXT, val)
    except KeyError:
        pass
    return TXT.CURRENT.format(subject=getattr(TXT, var), value=val)


# ---------------------------------


RESET = object()


def delete_cache(**kwargs):
    core.context.cache.pop(kwargs['uid'])


def check_status(**kwargs):
    test = kwargs.pop('test')
    tgs = kwargs.pop('tgs')
    cid = kwargs.pop('cid')
    uid = kwargs.pop('uid')
    status = kwargs.pop('status')
    test.assertEqual(tgs.status[cid][uid], status)


def check_message(**kwargs):
    test = kwargs.pop('test')
    tgs = kwargs.pop('tgs')
    cid = kwargs.pop('cid')
    text = kwargs.pop('text', None)
    media = kwargs.pop('media', None)
    exist = kwargs.pop('exist', True)

    if text:
        text = extract_entities_html(text)[0]
        cmp = lambda m: m.get('text') == text
    else:
        cmp = lambda m: media in m and (media != 'document' or 'animation' not in m)

    presence = any(cmp(message) for message in tgs.messages.get(cid, {}))

    if exist:
        msg = 'the message does not exist'
        okey = presence
    else:
        msg = 'the message exists'
        okey = not presence
    test.assertTrue(okey, msg)


_CFG_CACHE = collections.defaultdict(list)

def edit_cfg(**kwargs):
    kwargs.pop('test')
    kwargs.pop('tgs')
    for var, val in kwargs.items():

        if val == RESET:
            val = _CFG_CACHE[var].pop()
        else:
            _CFG_CACHE[var].append(getattr(CFG, var))

            if var.endswith('_TIMER') or var.endswith('_RESTRICTION'):
                val = datetime.timedelta(seconds=val)

        setattr(CFG, var, val)


_USER_CACHE = collections.defaultdict(lambda: collections.defaultdict(list))

def edit_user(**kwargs):
    kwargs.pop('test')
    tgs = kwargs.pop('tgs')
    uid = kwargs.pop('uid')
    for var, val in kwargs.items():

        if val == RESET:
            val = _USER_CACHE[uid][var].pop()
        else:
            _USER_CACHE[uid][var].append(tgs.users[uid].get(var, RESET))

        if val == RESET:
            del tgs.users[uid][var]
        else:
            tgs.users[uid][var] = val


def check_database(**kwargs):
    test = kwargs.pop('test')
    model = kwargs.pop('model')
    model_id = kwargs.pop('model_id')
    expected = kwargs.pop('data')
    dbs = core.context.dbsmk()
    data = {}
    try:
        obj = dbs.query(model).filter_by(id=model_id).first()
        for var in expected:
            data[var] = getattr(obj, var)
    finally:
        dbs.close()
    test.assertEqual(data, expected)


def del_chat(**kwargs):
    cid = kwargs.pop('cid')
    dbs = core.context.dbsmk()
    try:
        dbs.query(database.Chat).filter_by(id=cid).delete()
        dbs.commit()
    finally:
        dbs.close()


def set_except(**kwargs):
    tgs = kwargs.pop('tgs')
    exc = kwargs.pop('exc')
    tick = kwargs.pop('tick', None)
    cmd = kwargs.pop('cmd', None)
    tgs.add_exception_throwing(exception=exc, tick=tick, cmd=cmd)


FATAL_ERROR_RE = re.compile(r'The error <code>.+?</code> happened'
                            r' with the user <a href="tg://user\?id=\d+">.+?</a>'
                            r' within the chat <i>.+?</i> \(@.+?\)'
                            r'. '
                            r'The full traceback:\n\n'
                            r'<code>'
                            r'(  File ".+?", line \d+, in .+\n    .+\n)+'
                            r'</code>')

USR_M_1 = DATA.usr.m.replace('Usr Usr', 'a b')
USR_N_1 = DATA.usr.n.replace('Usr', 'a b')

USR_M_2 = DATA.usr.m.replace('Usr Usr (@\ufeffusr)', '@\ufeffusr')
USR_M_2 = DATA.usr.n.replace('Usr', '@\ufeffusr')


# ------------------------------------------------------

# a: administrator
# c: creator
# m: member
# l: left
# k: kicked
# r: restricted
STATUS_TABLE = '''
     g1 g2 g3 s1 s2 s3
bot   a  a  a  m  a  -
dev   m  m  m  m  m  m
adm   a  a  a  a  a  a
adm1  -  -  -  -  a  -
usr   l  l  l  l  l  l
usr1  -  m  -  -  m  -
'''

# Format: header, content[, caption][, markup]
#  header:  STR   'user:chat'             # user username and/or chat username
#  content: STR   'photo'                 # sendPhoto
#                 'sticker'               # sendSticker
#                 'document'              # sendDocument
#                 'animation'             # sendAnimation
#                 'title:text'            # new_chat_title
#                 'members:[-]UID1,UID2'  # left_chat_member, new_chat_members
#                 'migrate:CID'           # migrate_to_chat_id, migrate_from_chat_id
#                 'forward:CID:MID'       # forwardMessage
#                 'reply:MID:text'        # sendMessage (reply_to_message)
#                 'edit:MID:text'         # editMessageText
#                 'text'                  # sendMessage
#                 # callback is automatic except if the content start with "¢:",
#                 # to force a data value for callback use "ð:" (in inline_keyboard)
#  caption: STR   'text'
#  markup:  LIST  ['options']

RUN = functools.partial

INTERACTIONS = (
    ['dev', 'to allow messages from the bot'],  # dev is TELEGRAM_DID
    ['usr1:g2', 'message to be forwarded'],

    0,

    ['usr', '/start'],
    ['bot', TXT.MENU_NOTHING],

    1,

    ['usr:g1', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=G1ID, text=DATA.usr.captcha, exist=True),
    ['usr:g1', ANSWER],
    ['bot', DATA.usr.solved],
    RUN(check_message, cid=G1ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr.solved, exist=True),
    'tick',  # captcha_thread
    RUN(check_message, cid=G1ID, text=DATA.usr.solved, exist=False),
    'tick',  # greeting_thread
    ['bot:g1', DATA.usr.greeting],
    RUN(check_status, cid=G1ID, uid=USRID, status='member'),

    ['usr:g1', 'text without url'],
    RUN(check_message, cid=G1ID, text='text without url', exist=True),

    # The following two tests are repeated after the time TEMPORARY_RESTRICTION

    ['usr:g1', 'www.test.io'],
    RUN(check_message, cid=G1ID, text='www.test.io', exist=False),

    ['usr:g1', 'forward:g2:1'],
    RUN(check_message, cid=G1ID, text='message to be forwarded', exist=False),

    2,

    ['usr', '/help'],
    ['bot', TXT.HELP],

    ['usr:g1', '/help'],
    ['bot:g1', TXT.HELP],

    ['adm', '/help'],
    ['bot', TXT.HELP],

    ['adm:g1', '/help'],
    ['bot:g1', TXT.HELP],

    3,

    ['usr:g2', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=G2ID, text=DATA.usr.captcha, exist=True),
    ['usr:g2', '1'],
    ['bot', DATA.usr.g_wrong, [TXT.CHANCE_CAPTCHA]],
    RUN(check_message, cid=G2ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G2ID, text=DATA.usr.g_wrong, exist=True),

    # Unresolved Captcha, messages are deleted, but user is not kicked out yet
    RUN(check_status, cid=G2ID, uid=USRID, status='member'),
    ['usr:g2', '¢:text2a'],
    RUN(check_message, cid=G2ID, text='text2a', exist=False),

    ['usr', '/start'],
    ['bot', TXT.MENU_SOLVE, [TXT.YES, TXT.NO]],
    ['usr', TXT.YES],
    ['bot', TXT.PROCESSING.format(item='G2')],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr', ANSWER],
    ['bot:g2', DATA.usr.solved],
    ['bot', TXT.CAPTCHA_SOLVED_PRIVATE],
    'tick',  # captcha_thread
    RUN(check_message, cid=G2ID, text=DATA.usr.g_wrong, exist=False),
    'tick',  # greeting_thread
    ['bot:g2', DATA.usr.greeting],

    RUN(check_status, cid=G2ID, uid=USRID, status='member'),
    ['usr:g2', 'text2b'],
    RUN(check_message, cid=G2ID, text='text2b', exist=True),

    4,

    ['usr:g3', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=True),

    ['usr:g3', TXT.NEW_CAPTCHA],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr:g3', TXT.NEW_CAPTCHA],  # CAPTCHA_WAIT_ALERT is sent as a callback
    ['usr:g3', TXT.NEW_CAPTCHA],  # CAPTCHA_WAIT_ALERT is sent as a callback

    ['usr:g3', '1'],
    ['bot', DATA.usr.g_wrong, [TXT.CHANCE_CAPTCHA]],
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G3ID, text=DATA.usr.g_wrong, exist=True),

    ['usr', '/start'],
    ['bot', TXT.MENU_SOLVE, [TXT.YES, TXT.NO]],
    ['usr', TXT.NO],
    ['bot', TXT.CANCELLED],

    ['usr', '/start'],
    ['bot', TXT.MENU_SOLVE, [TXT.YES, TXT.NO]],
    ['usr', TXT.YES],
    ['bot', TXT.PROCESSING.format(item='G3')],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr', '2'],
    ['bot', DATA.usr.p_wrong],
    'tick',  # captcha_thread
    RUN(check_message, cid=G3ID, text=DATA.usr.g_wrong, exist=False),
    'tick',  # greeting_thread
    ['bot:dev', DATA.usr.ban_wrong.format('G3')],

    ['usr', '/start'],
    ['bot', G3_MENU_WAIT],
    RUN(check_status, cid=G3ID, uid=USRID, status='kicked'),

    5,

    # The bot has no permissions
    ['usr:s1', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=S1ID, text=DATA.usr.captcha, exist=True),
    ['usr:s1', TXT.NEW_CAPTCHA],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr:s1', '1'],
    ['bot', DATA.usr.g_wrong, [TXT.CHANCE_CAPTCHA]],
    RUN(check_message, cid=S1ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=S1ID, text=DATA.usr.g_wrong, exist=True),

    ['usr', '/start'],
    ['bot', TXT.MENU_SOLVE, [TXT.YES, TXT.NO]],
    ['usr', TXT.YES],
    ['bot', TXT.PROCESSING.format(item='S1')],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr', '2'],
    ['bot', DATA.usr.p_wrong],

    'tick',  # captcha_thread
    RUN(check_message, cid=S1ID, text=DATA.usr.g_wrong, exist=False),
    'tick',  # greeting_thread
    ['bot:dev', DATA.usr.ban_insuf_perms.format('S1')],
    RUN(check_status, cid=S1ID, uid=USRID, status='member'),

    6,

    ['usr:s2', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=S2ID, text=DATA.usr.captcha, exist=True),
    'tick',  # captcha_thread
    RUN(check_message, cid=S2ID, text=DATA.usr.captcha, exist=False),
    'tick',  # greeting_thread
    ['bot:dev', DATA.usr.ban_waiting.format('S2')],
    RUN(check_status, cid=S2ID, uid=USRID, status='kicked'),

    7,

    # For usr1 threads to run first than usr2 threads
    RUN(edit_cfg, CAPTCHA_TIMER=1, GREETING_TIMER=1),

    ['usr1:s2', 'members:usr1'],
    ['bot', DATA.usr1.captcha, MARKUP],
    RUN(check_message, cid=S2ID, text=DATA.usr1.captcha, exist=True),
    ['usr1:s2', ANSWER],
    ['bot', DATA.usr1.solved],
    RUN(check_message, cid=S2ID, text=DATA.usr1.captcha, exist=False),
    RUN(check_message, cid=S2ID, text=DATA.usr1.solved, exist=True),

    ['usr2:s2', 'members:usr2'],
    ['bot', DATA.usr2.captcha, MARKUP],
    RUN(check_message, cid=S2ID, text=DATA.usr2.captcha, exist=True),

    'tick',  # usr1: captcha_thread
    RUN(check_message, cid=S2ID, text=DATA.usr1.solved, exist=False),
    'tick',  # usr1: greeting_thread
    ['bot:s2', DATA.usr1.greeting],

    'tick',  # usr2: captcha_thread
    RUN(check_message, cid=S2ID, text=DATA.usr2.captcha, exist=False),
    'tick',  # usr2: greeting_thread
    ['bot:dev', DATA.usr2.ban_waiting.format('S2')],

    RUN(check_status, cid=S2ID, uid=USR1ID, status='member'),
    RUN(check_status, cid=S2ID, uid=USR2ID, status='kicked'),
    RUN(edit_cfg, CAPTCHA_TIMER=RESET, GREETING_TIMER=RESET),

    8,

    ['usr:g3', 'members:usr'],
    RUN(check_status, cid=G3ID, uid=USRID, status='member'),
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=True),
    ['usr:g3', '¢:members:-usr'],
    'tick2',
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=False),
    RUN(check_status, cid=G3ID, uid=USRID, status='left'),

    9,

    ['usr:g3', 'members:usr'],
    RUN(check_status, cid=G3ID, uid=USRID, status='member'),
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=True),
    ['usr:g3', ANSWER],
    ['bot', DATA.usr.solved],
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G3ID, text=DATA.usr.solved, exist=True),
    'tick',
    RUN(check_message, cid=G3ID, text=DATA.usr.solved, exist=False),
    ['usr:g3', 'members:-usr'],
    'tick',
    RUN(check_status, cid=G3ID, uid=USRID, status='left'),

    10,

    ['usr:s1', 'members:-usr'],
    RUN(check_status, cid=S1ID, uid=USRID, status='left'),
    ['usr:s1', 'members:usr'],
    RUN(check_status, cid=S1ID, uid=USRID, status='member'),
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=S1ID, text=DATA.usr.captcha, exist=True),
    ['usr:s1', ANSWER],
    ['bot', DATA.usr.solved],
    RUN(check_message, cid=S1ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=S1ID, text=DATA.usr.solved, exist=True),
    'tick',
    RUN(check_message, cid=S1ID, text=DATA.usr.solved, exist=False),
    ['usr:s1', 'members:-usr'],
    'tick',
    RUN(check_status, cid=S1ID, uid=USRID, status='left'),

    11,

    # Continuation of a previous test, after the TEMPORARY_RESTRICTION time passes

    ['usr:g1', 'www.test.io'],
    RUN(check_message, cid=G1ID, text='www.test.io', exist=True),

    ['usr:g1', 'forward:g2:1'],
    RUN(check_message, cid=G1ID, text='message to be forwarded', exist=True),

    12,

    ['usr:g1', 'members:-usr'],
    ['usr:g2', 'members:-usr'],

    13,

    ['usr:g1', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=G1ID, text=DATA.usr.captcha, exist=True),
    ['usr:g1', '1'],
    ['bot', DATA.usr.g_wrong, [TXT.CHANCE_CAPTCHA]],
    RUN(check_message, cid=G1ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr.g_wrong, exist=True),

    ['usr:g2', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=G2ID, text=DATA.usr.captcha, exist=True),
    ['usr:g2', '1'],
    ['bot', DATA.usr.g_wrong, [TXT.CHANCE_CAPTCHA]],
    RUN(check_message, cid=G2ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G2ID, text=DATA.usr.g_wrong, exist=True),

    RUN(check_status, cid=G1ID, uid=USRID, status='member'),
    RUN(check_status, cid=G2ID, uid=USRID, status='member'),

    ['usr', '/start'],
    ['bot', TXT.MENU_SOLVE, [TXT.YES, TXT.NO]],
    ['usr', TXT.YES],
    ['bot', TXT.MENU_CHOOSE, ['1• G1', '2• G2']],
    ['usr', '1• xx'],
    ['bot', TXT.INCORRECT],
    ['usr', '¢:/cancel'],
    ['bot', TXT.CANCELLED],

    'tick4',

    RUN(check_message, cid=G1ID, text=DATA.usr.g_wrong, exist=False),
    RUN(check_message, cid=G2ID, text=DATA.usr.g_wrong, exist=False),

    ['bot:dev', DATA.usr.ban_wrong.format('G1')],
    ['bot:dev', DATA.usr.ban_wrong.format('G2')],

    RUN(check_status, cid=G1ID, uid=USRID, status='kicked'),
    RUN(check_status, cid=G2ID, uid=USRID, status='kicked'),

    14,

    ['usr:g1', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr:g1', '1'],
    ['bot', DATA.usr.g_wrong, [TXT.CHANCE_CAPTCHA]],

    ['usr:g2', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr:g2', '1'],
    ['bot', DATA.usr.g_wrong, [TXT.CHANCE_CAPTCHA]],

    RUN(check_status, cid=G1ID, uid=USRID, status='member'),
    RUN(check_status, cid=G2ID, uid=USRID, status='member'),

    ['usr', '/start'],
    ['bot', TXT.MENU_SOLVE, [TXT.YES, TXT.NO]],
    RUN(delete_cache, uid=USRID),
    ['usr', TXT.YES],
    ['bot', TXT.WAS_AN_ERROR],
    ['usr', '/start'],
    ['bot', TXT.MENU_SOLVE, [TXT.YES, TXT.NO]],
    ['usr', TXT.YES],

    ['bot', TXT.MENU_CHOOSE, ['1• G1', '2• G2']],  # database: Admission.id.asc()
    ['usr', '1• G1'],
    ['bot', DATA.usr.captcha, MARKUP],

    'tick4',

    ['bot:usr', TXT.CAPTCHA_TIMEOUT_PRIVATE],
    ['bot:dev', DATA.usr.ban_wrong.format('G1')],
    ['bot:dev', DATA.usr.ban_wrong.format('G2')],
    RUN(check_message, cid=G1ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr.g_wrong, exist=False),
    RUN(check_message, cid=G2ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G2ID, text=DATA.usr.g_wrong, exist=False),
    RUN(check_status, cid=G1ID, uid=USRID, status='kicked'),
    RUN(check_status, cid=G2ID, uid=USRID, status='kicked'),

    15,

    RUN(edit_user, uid=USRID, language_code='xxx'),
    ['usr:s1', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr:s1', ANSWER],
    ['bot', DATA.usr.solved],
    'tick2',
    ['bot:s1', DATA.usr.greeting],
    RUN(check_message, cid=S1ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=S1ID, text=DATA.usr.solved, exist=False),
    RUN(edit_user, uid=USRID, language_code=RESET),

    ['usr', '/config'],
    ['bot', TXT.GROUPS_ONLY],

    ['usr:s1', '/config'],
    ['bot', TXT.ONLY_ADMIN],

    ['usr:s1', '/start'],
    ['bot', TXT.PRIVATE_ONLY],

    16,

    RUN(check_database, model=database.Chat, model_id=S1ID, data={
        'language': None,
        'max_strikes': 0,
        'call_admins': True,
        'greet_give': True,
        'greet_timer': True,
        'greet_admin_show': False,
        'greet_admin_cite': False,
        'greet_admin_order': 'A-Z',
    }),

    ['adm:s1', '/config'],
    ['bot', get_menu('LANGUAGE', '—'), MENU.LANGUAGE],
    ['adm:s1', 'en'],
    ['bot', get_menu('MAX_STRIKES', 'DISABLED'), MENU.MAX_STRIKES],
    ['adm:s1', '1'],
    ['bot', get_menu('CALL_ADMINS', 'YES'), MENU.CALL_ADMINS],
    ['adm:s1', TXT.NO],
    ['bot', get_menu('GREET_GIVE', 'YES'), MENU.GREET_GIVE],
    ['adm:s1', TXT.YES],
    ['bot', get_menu('GREET_TIMER', 'YES'), MENU.GREET_TIMER],
    ['adm:s1', TXT.NO],
    ['bot', get_menu('GREET_ADMIN_SHOW', 'NO'), MENU.GREET_ADMIN_SHOW[:-1]],
    ['adm:s1', TXT.YES],
    ['bot', get_menu('GREET_ADMIN_CITE', 'NO'), MENU.GREET_ADMIN_CITE],
    ['adm:s1', TXT.YES],
    ['bot', get_menu('GREET_ADMIN_ORDER', 'A-Z'), MENU.GREET_ADMIN_ORDER],
    ['adm:s1', 'Z-A'],
    ['bot', TXT.STOP_CONFIG],

    RUN(check_database, model=database.Chat, model_id=S1ID, data={
        'language': 'en',
        'max_strikes': 1,
        'call_admins': False,
        'greet_give': True,
        'greet_timer': False,
        'greet_admin_show': True,
        'greet_admin_cite': True,
        'greet_admin_order': 'Z-A',
    }),

    17,

    ['adm:s1', '/config'],
    ['bot', get_menu('LANGUAGE', 'en'), MENU.LANGUAGE],
    ['adm:s1', '➡️'],
    ['bot', get_menu('MAX_STRIKES', '1'), MENU.MAX_STRIKES],
    ['adm:s1', '⬅️'],
    ['bot', get_menu('LANGUAGE', 'en'), MENU.LANGUAGE],
    ['adm:s1', '➡️'],
    ['bot', get_menu('MAX_STRIKES', '1'), MENU.MAX_STRIKES],
    ['adm:s1', '3'],
    ['bot', get_menu('CALL_ADMINS', 'NO'), MENU.CALL_ADMINS],
    ['adm:s1', TXT.YES],
    ['bot', get_menu('GREET_GIVE', 'YES'), MENU.GREET_GIVE],
    ['adm:s1', '➡️'],
    ['bot', get_menu('GREET_TIMER', 'NO'), MENU.GREET_TIMER],
    ['adm:s1', TXT.YES],
    ['bot', get_menu('GREET_ADMIN_SHOW', 'YES'), MENU.GREET_ADMIN_SHOW],
    ['adm:s1', '➡️'],
    ['bot', get_menu('GREET_ADMIN_CITE', 'YES'), MENU.GREET_ADMIN_CITE],
    ['adm:s1', TXT.NO],
    ['bot', get_menu('GREET_ADMIN_ORDER', 'Z-A'), MENU.GREET_ADMIN_ORDER],
    ['adm:s1', '🆗'],
    ['bot', TXT.STOP_CONFIG],

    'tick',  # greeting_thread, GREET_TIMER → YES

    RUN(check_database, model=database.Chat, model_id=S1ID, data={
        'language': 'en',
        'max_strikes': 3,
        'call_admins': True,
        'greet_give': True,
        'greet_timer': True,
        'greet_admin_show': True,
        'greet_admin_cite': False,
        'greet_admin_order': 'Z-A',
    }),

    18,

    ['adm:s2', '/config'],
    ['bot', get_menu('LANGUAGE', '—'), MENU.LANGUAGE],
    ['adm:s2', '➡️'],
    ['bot', get_menu('MAX_STRIKES', 'DISABLED'), MENU.MAX_STRIKES],
    ['adm:s2', '2'],
    ['bot', get_menu('CALL_ADMINS', 'YES'), MENU.CALL_ADMINS],
    ['adm:s2', '🆗'],
    ['bot', TXT.STOP_CONFIG],

    19,

    ['adm:s1', '/set_greet_note \nGreet note'],
    ['bot', TXT.GREET_NOTE_SET],

    ['usr:s1', 'members:-usr'],
    RUN(check_status, cid=S1ID, uid=USRID, status='left'),

    ['usr:s1', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr:s1', ANSWER],
    ['bot', DATA.usr.solved],
    'tick2',
    ['bot:s1', f'{DATA.usr.greeting}\nGreet note{S1_ADM_LST}'],
    RUN(check_message, cid=S1ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=S1ID, text=DATA.usr.solved, exist=False),

    ['dev:s1', 'members:dev'],
    ['bot', DATA.dev.captcha, MARKUP],
    ['dev:s1', ANSWER],
    ['bot', DATA.dev.solved],
    'tick2',
    ['bot:s1', f'{DATA.usr.greeting[:-1]} and {DATA.dev.n}!\nGreet note{S1_ADM_LST}'],
    RUN(check_message, cid=S1ID, text=DATA.dev.captcha, exist=False),
    RUN(check_message, cid=S1ID, text=DATA.dev.solved, exist=False),

    20,

    # Spam

    ['usr:s1', 'tg member 1'],
    ['bot:dev', DELETED_BY_SPAM.format('tgmember', 'S1', DATA.usr.f)],
    ['bot:dev', 'tg member 1'],
    ['bot:s1', TXT.STRIKE_WARNING.format(user=DATA.usr.m, strike=1, max_strikes=3)],
    RUN(check_message, cid=S1ID, text='tg member 1', exist=False),

    21,

    RUN(edit_cfg, TELEGRAM_DID=0),
    ['usr:s1', 'tg member 2'],
    ['bot:s1', TXT.STRIKE_WARNING.format(user=DATA.usr.m, strike=2, max_strikes=3)],
    RUN(edit_cfg, TELEGRAM_DID=RESET),
    RUN(check_message, cid=S1ID, text='tg member 2', exist=False),

    22,

    ['usr:s1', 'tg member 3'],
    ['bot:dev', DELETED_BY_SPAM.format('tgmember', 'S1', DATA.usr.f)],
    ['bot:dev', 'tg member 3'],
    ['bot:s1', TXT.STRIKE_WARNING.format(user=DATA.usr.m, strike=3, max_strikes=3)],
    ['bot:dev', DATA.usr.ban_insuf_perms.format('S1')],
    RUN(check_message, cid=S1ID, text='tg member 3', exist=False),

    23,

    # Setup usr in S2
    ['adm:s2', 'text'],
    ['usr:s2', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=S2ID, text=DATA.usr.captcha, exist=True),
    ['usr:s2', ANSWER],
    ['bot', DATA.usr.solved],
    RUN(check_message, cid=S2ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=S2ID, text=DATA.usr.solved, exist=True),
    'tick',
    RUN(check_message, cid=S2ID, text=DATA.usr.solved, exist=False),
    'tick',
    ['bot:s2', DATA.usr.greeting],

    24,

    ['usr:s2', 'tg member'],
    ['bot:dev', DELETED_BY_SPAM.format('tgmember', 'S2', DATA.usr.f)],
    ['bot:dev', 'tg member'],
    ['bot:s2', TXT.STRIKE_WARNING.format(user=DATA.usr.m, strike=1, max_strikes=2)],
    RUN(check_message, cid=S2ID, text='tg member', exist=False),

    ['usr:s2', 'telegram marketing'],
    ['bot:dev', DELETED_BY_SPAM.format('telegrammarketing', 'S2', DATA.usr.f)],
    ['bot:dev', 'telegram marketing'],
    ['bot:s2', TXT.STRIKE_WARNING.format(user=DATA.usr.m, strike=2, max_strikes=2)],
    ['bot:dev', BAN_BY.format('spammer', 'S2', DATA.usr.f)],
    RUN(check_message, cid=S2ID, text='telegram marketing', exist=False),

    25,

    RUN(edit_user, uid=USRID, first_name='deleted', last_name='account'),
    ['usr:g1', 'members:usr'],
    ['bot:dev', BAN_BY.format('fake name', 'G1', 'deleted account')],
    RUN(edit_user, uid=USRID, first_name=RESET, last_name=RESET),

    26,

    ['usr:g1', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr:g1', ANSWER],
    ['bot', DATA.usr.solved],
    RUN(edit_user, uid=USRID, first_name='deleted', last_name='account'),
    'tick2',  # captcha_thread, greeting_thread
    ['bot:dev', BAN_BY.format('fake name', 'G1', 'deleted account')],
    RUN(edit_user, uid=USRID, first_name=RESET, last_name=RESET),
    RUN(check_message, cid=G1ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr.solved, exist=False),

    27,

    RUN(edit_user, uid=USRID, first_name=None, last_name=None, username=None),
    ['usr:g1', 'members:usr'],
    ['bot:dev', BAN_BY.format('without name and username', 'G1', 'None')],
    RUN(edit_user, uid=USRID, first_name=RESET, last_name=RESET, username=RESET),

    28,

    ['adm:g1', 'members:-bot'],
    ['adm:g1', 'members:bot,bot1'],

    29,

    RUN(check_status, cid=G1ID, uid=USRID, status='kicked'),

    # Error with DEBUGGING
    RUN(edit_cfg, TELEGRAM_DID=0),
    ['usr:g1', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    'tick2',  # captcha_thread, greeting_thread, kicked
    RUN(edit_cfg, TELEGRAM_DID=RESET),
    RUN(check_message, cid=G1ID, text=DATA.usr.captcha, exist=False),

    30,

    ['adm:g1', f'/set_greet_note {"-" * 4096}'],
    ['bot', TXT.GREET_NOTE_SET],

    ['adm:g1', 'members:-usr'],
    ['adm:g1', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    ['usr:g1', ANSWER],
    ['bot', DATA.usr.solved],
    'tick2',
    ['bot:g1', TXT.GREET_ERROR],
    ['bot:g1', DATA.usr.greeting],

    RUN(check_message, cid=G1ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr.solved, exist=False),
    RUN(check_status, cid=G1ID, uid=USRID, status='member'),

    31,

    RUN(check_database, model=database.Chat, model_id=G1ID, data={
        'greet_note': '-' * 4096 + ' ',  # one space is added
    }),

    ['adm:g1', f'/set_greet_note'],
    ['bot', TXT.GREET_NOTE_UNSET],

    RUN(check_database, model=database.Chat, model_id=G1ID, data={
        'greet_note': '',
    }),

    32,

    ['usr1:g1', 'members:usr1'],
    ['bot', DATA.usr1.captcha, MARKUP],
    ['usr1:g1', ANSWER],
    ['bot', DATA.usr1.solved],
    'tick',  # captcha_thread
    RUN(check_message, cid=G1ID, text=DATA.usr1.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr1.solved, exist=False),

    ['usr2:g1', 'members:usr2'],
    ['bot', DATA.usr2.captcha, MARKUP],
    ['usr2:g1', ANSWER],
    ['bot', DATA.usr2.solved],
    'tick',  # captcha_thread
    RUN(check_message, cid=G1ID, text=DATA.usr2.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr2.solved, exist=False),

    ['usr3:g1', 'members:usr3'],
    ['bot', DATA.usr3.captcha, MARKUP],
    ['usr3:g1', ANSWER],
    ['bot', DATA.usr3.solved],
    'tick',  # captcha_thread
    RUN(check_message, cid=G1ID, text=DATA.usr3.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr3.solved, exist=False),

    'tick',  # greeting_thread
    ['bot:g1', PLURAL_GREET(f'{DATA.usr1.n}, {DATA.usr2.n} and {DATA.usr3.n}')],
    'tick2',  # greeting_thread

    33,

    ['usr1:g1', 'text'],  # cancel greeting grouping, the previous three

    ['usr1:g1', 'members:-usr1'],
    ['usr2:g1', 'members:-usr2'],
    ['usr3:g1', 'members:-usr3'],

    ['usr1:g1', 'members:usr1'],
    ['bot', DATA.usr1.captcha, MARKUP],
    ['usr1:g1', ANSWER],
    ['bot', DATA.usr1.solved],
    'tick2',  # captcha_thread, greeting_thread
    ['bot:g1', DATA.usr1.greeting],
    RUN(check_message, cid=G1ID, text=DATA.usr1.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr1.solved, exist=False),

    RUN(check_message, cid=G1ID, text=DATA.usr1.greeting, exist=True),  # first greeting

    ['usr2:g1', 'members:usr2'],
    ['bot', DATA.usr2.captcha, MARKUP],
    ['usr2:g1', ANSWER],
    ['bot', DATA.usr2.solved],
    'tick2',  # captcha_thread, greeting_thread
    ['bot:g1', PLURAL_GREET(f'{DATA.usr1.n} and {DATA.usr2.n}')],
    RUN(check_message, cid=G1ID, text=DATA.usr2.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr2.solved, exist=False),

    RUN(check_message, cid=G1ID, text=DATA.usr1.greeting, exist=False),  # first greeting

    RUN(check_database, model=database.Chat, model_id=G1ID, data={
        'prev_greet_users': f'{DATA.usr1.n}, {DATA.usr2.n}',
    }),

    ['usr3:g1', 'members:usr3'],
    ['bot', DATA.usr3.captcha, MARKUP],
    ['usr3:g1', ANSWER],
    ['bot', DATA.usr3.solved],
    'tick',   # captcha_thread
    ['adm:g1', 'welcome to all!'],
    'tick',   # greeting_thread
    RUN(check_message, cid=G1ID, text=DATA.usr3.captcha, exist=False),
    RUN(check_message, cid=G1ID, text=DATA.usr3.solved, exist=False),

    RUN(check_database, model=database.Chat, model_id=G1ID, data={
        'prev_greet_users': None,
    }),

    34,

    ['adm:s3', 'members:bot'],

    35,

    RUN(check_status, cid=G3ID, uid=USRID, status='left'),
    ['usr:g3', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=True),

    ['usr:g3', '¢:photo'],
    RUN(check_message, cid=G3ID, media='photo', exist=False),

    ['usr:g3', '1'],
    ['bot', DATA.usr.g_wrong, [TXT.CHANCE_CAPTCHA]],
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G3ID, text=DATA.usr.g_wrong, exist=True),

    ['usr', '/start'],
    ['bot', TXT.MENU_SOLVE, [TXT.YES, TXT.NO]],
    ['usr', TXT.YES],
    ['bot', TXT.PROCESSING.format(item='G3')],
    ['bot', DATA.usr.captcha, MARKUP],

    ['usr:g3', '¢:members:-usr'],
    ['bot:usr', TXT.CAPTCHA_LEAVE_PRIVATE],
    'tick',   # captcha_thread
    RUN(check_message, cid=G3ID, text=DATA.usr.g_wrong, exist=False),
    'tick',   # greeting_thread

    ['adm:g3', 'photo'],
    RUN(check_message, cid=G3ID, media='photo', exist=True),

    36,

    RUN(check_status, cid=G1ID, uid=USRID, status='member'),
    ['usr:g1', 'calling the @admins'],
    ['bot', TXT.QUOTE_ADMINS(1).format(mention=DATA.adm.c)],

    37,

    ['adm:s2', '/config'],
    ['bot', get_menu('LANGUAGE', '—'), MENU.LANGUAGE],
    ['adm:s2', 'en'],
    ['bot', get_menu('MAX_STRIKES', '2'), MENU.MAX_STRIKES],
    ['adm:s2', 'ð:config:?'],               # incorrect callback data
    ['adm:s2', 'ð:config::'],               # incorrect callback data
    ['adm1:s2', 'ð:config:stop:'],          # another admin started it
    ['usr1:s2', 'ð:config:stop:'],          # is not admin
    ['adm:s2', 'ð:config:prev:?'],          # incorrect callback data, stop config
    ['bot', TXT.STOP_CONFIG],

    38,

    ['adm:s2', '/config'],
    ['bot', get_menu('LANGUAGE', 'en'), MENU.LANGUAGE],
    ['adm:s2', 'ð:config:MAX_STRIKES:a'],   # incorrect callback data, stop config
    ['bot', TXT.STOP_CONFIG],

    39,

    ['adm:s2', '/config'],
    ['bot', get_menu('LANGUAGE', 'en'), MENU.LANGUAGE],
    ['adm:s2', 'ð:config:TEST:b'],          # incorrect callback data, stop config
    ['bot', TXT.STOP_CONFIG],

    40,

    ['adm:s2', '/config'],
    ['bot', get_menu('LANGUAGE', 'en'), MENU.LANGUAGE],
    ['adm:s2', '➡️'],
    ['bot', get_menu('MAX_STRIKES', '2'), MENU.MAX_STRIKES],
    ['adm:s2', '➡️'],
    ['bot', get_menu('CALL_ADMINS', 'YES'), MENU.CALL_ADMINS],
    ['adm:s2', '➡️'],
    ['bot', get_menu('GREET_GIVE', 'YES'), MENU.GREET_GIVE],
    ['adm:s2', '➡️'],
    ['bot', get_menu('GREET_TIMER', 'YES'), MENU.GREET_TIMER],
    ['adm:s2', TXT.NO],
    ['bot', get_menu('GREET_ADMIN_SHOW', 'NO'), MENU.GREET_ADMIN_SHOW[:-1]],
    ['adm:s2', '🆗'],
    ['bot', TXT.STOP_CONFIG],

    ['usr:s2', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=S2ID, text=DATA.usr.captcha, exist=True),

    ['usr:s2', ANSWER],
    ['bot', DATA.usr.solved],
    RUN(check_message, cid=S2ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=S2ID, text=DATA.usr.solved, exist=True),

    'tick',  # greeting_thread (with GAP_NOW_TIME, WARNING! This one runs first)
    'tick',  # captcha_thread
    RUN(check_message, cid=S2ID, text=DATA.usr.solved, exist=False),

    ['bot:s2', DATA.usr.greeting],

    41,

    ['adm', '/debug'],
    ['adm:g1', '/debug'],
    ['dev:g1', '/debug'],
    ['dev', '/debug'],

    42,

    ['adm', '/erroneous'],
    ['bot:adm', TXT.ERROR_HAPPENED],
    ['bot:dev', FATAL_ERROR_RE],

    43,

    # In all cases the G3 group must be migrated
    # but the migration is not executed in these functions,
    # so it is used as a new id "G3".

    RUN(set_except, exc=ChatMigrated(G3ID), tick=None, cmd='restrictchatmember'),
    ['usr1:g3', 'members:usr1'],
    ['bot', DATA.usr1.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=True),
    ['usr1:g3', ANSWER],
    ['bot', DATA.usr1.solved],
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=False),
    RUN(check_message, cid=G3ID, text=DATA.usr1.solved, exist=True),
    'tick',  # captcha_thread
    RUN(check_message, cid=G3ID, text=DATA.usr1.solved, exist=False),
    'tick',  # greeting_thread
    ['bot', DATA.usr1.greeting],
    ['usr1:g3', 'members:-usr1'],

    44,

    ['usr1:g3', 'members:usr1'],
    ['bot', DATA.usr1.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=True),
    RUN(set_except, exc=ChatMigrated(G3ID), tick=None, cmd='deletemessage'),
    'tick',  # captcha_thread
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=False),
    'tick',  # greeting_thread
    ['bot:dev', DATA.usr1.ban_waiting.format('G3')],

    45,

    ['usr1:g3', 'members:usr1'],
    ['bot', DATA.usr1.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=True),
    RUN(set_except, exc=ChatMigrated(G3ID), tick=4, cmd='kickchatmember'),
    'tick',  # captcha_thread
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=False),
    'tick',  # greeting_thread
    ['bot:dev', DATA.usr1.ban_waiting.format('G3')],

    46,

    ['usr1:g3', 'members:usr1'],
    ['bot', DATA.usr1.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=True),
    RUN(set_except, exc=TelegramError('Unexpected Error'), tick=4, cmd='kickchatmember'),
    'tick',  # captcha_thread
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=False),
    'tick',  # greeting_thread
    ['bot:dev', BAN.format(UNEXPECTED_ERROR, 'G3', DATA.usr1.f)],

    47,

    # The greeting cannot be sent because of errors
    ['usr1:g3', 'members:usr1'],
    ['bot', DATA.usr1.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=True),
    ['usr1:g3', ANSWER],
    ['bot', DATA.usr1.solved],
    RUN(check_message, cid=G3ID, text=DATA.usr1.captcha, exist=False),
    RUN(check_message, cid=G3ID, text=DATA.usr1.solved, exist=True),
    'tick',  # captcha_thread
    RUN(check_message, cid=G3ID, text=DATA.usr1.solved, exist=False),
    RUN(set_except, exc=TelegramError('error'), tick=2, cmd='sendmessage'),
    RUN(set_except, exc=TelegramError('error'), tick=3, cmd='sendmessage'),
    'tick',  # greeting_thread

    48,

    ['usr:g3', 'members:usr'],
    ['bot', DATA.usr.captcha, MARKUP],
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=True),
    ['usr:g3', ANSWER],
    ['bot', DATA.usr.solved],
    RUN(check_message, cid=G3ID, text=DATA.usr.captcha, exist=False),
    RUN(check_message, cid=G3ID, text=DATA.usr.solved, exist=True),
    'tick',  # captcha_thread
    RUN(check_message, cid=G3ID, text=DATA.usr.solved, exist=False),

    # tick=1 → getchatmember
    RUN(set_except, exc=ChatMigrated(S3ID), tick=2, cmd='sendmessage'),
    # tick=2 → getchat → to update the ctx object
    RUN(set_except, exc=TelegramError('error'), tick=4, cmd='sendmessage'),

    'tick',  # greeting_thread
    ['bot:s3', TXT.GREET_ERROR],

    49,

    RUN(del_chat, cid=S2ID),

    ['adm:g2', 'text'],
    RUN(set_except, exc=ChatMigrated(S2ID), tick=1, cmd='sendmessage'),
    # tick=2 → getchat     → to update the ctx object
    # tick=3 → sendmessage → finally send the message to the supergroup
    RUN(set_except, exc=ChatMigrated(S2ID), tick=4, cmd='sendmessage'),
    ['adm:g2', '/g2_to_s2'],
    ['bot:s2', 'text1'],
    ['bot:s2', 'text2'],

    50,

    RUN(edit_user, uid=USRID, first_name='a', last_name='b'),
    ['usr:g1', 'members:-usr'],
    ['usr:g1', 'members:usr'],
    ['bot', TXT.CAPTCHA.format(user=USR_M_1, captcha=CAPTCHA), MARKUP],
    ['usr:g1', ANSWER],
    ['bot', TXT.CAPTCHA_SOLVED_GROUP.format(user=USR_M_1)],
    'tick2',  # captcha_thread, greeting_thread
    ['bot:g1', TXT.GREETING(1).format(mention=USR_N_1)],
    RUN(edit_user, uid=USRID, first_name=RESET, last_name=RESET),

    51,

    RUN(edit_user, uid=USRID, first_name=None, last_name=None),
    ['usr:g1', 'to avoid grouping the next greeting'],
    ['usr:g1', 'members:-usr'],
    ['usr:g1', 'members:usr'],
    ['bot', TXT.CAPTCHA.format(user=USR_M_2, captcha=CAPTCHA), MARKUP],
    ['usr:g1', ANSWER],
    ['bot', TXT.CAPTCHA_SOLVED_GROUP.format(user=USR_M_2)],
    'tick2',  # captcha_thread, greeting_thread
    ['bot:g1', TXT.GREETING(1).format(mention=USR_M_2)],
    RUN(edit_user, uid=USRID, first_name=RESET, last_name=RESET),
)


# Function that sends text to a chat that doesn't exist,
# throwing the error so that the error handler of the telegram library can process it
@core.flogger
@core.context
def erroneous_handler(ctx):
    ctx.send(chat_id=1, text='text', throw_exc=True)


# Function that sends two messages to a group that migrated,
# the first message triggers the migration,
# both messages are finally sent to the supergroup
@core.flogger
@core.context
def g2_to_s2_handler(ctx):
    ctx.send(chat_id=G2ID, text='text1')
    ctx.send(chat_id=G2ID, text='text2')


class TestBot(unittest.TestCase):


    def setUp(self):
        self.maxDiff = None     # pylint: disable=invalid-name
        CFG.__dict__.clear()    # pylint: disable=protected-access

        # Logging enabled and configured
        logging.disable(0)
        logging.basicConfig(filename='unittest.testbot.log',
                            level=logging.DEBUG,
                            format=CFG.log_format,
                            datefmt=CFG.DATETIME_FORMAT)
        core.only_bot_logging()
        #logging.getLogger('tgs').setLevel(logging.ERROR)

        # Bot and Server Initialization
        tgs = TelegramServer()
        tgs.set_states(STATUS_TABLE)
        tgs.add_handlers(core.add_handlers)
        core.context.initialize()
        self.bio = BotIOAdapter(self, tgs)

        # Extra handlers
        data = (
            ('erroneous', erroneous_handler),
            ('g2_to_s2', g2_to_s2_handler),
        )
        for cmd, handler in data:
            tgs.dispatcher.add_handler(CommandHandler(cmd, handler), group=-1)


    def tearDown(self):
        # Shutdown and disabled
        logging.disable()


    @skipIf(UNTESTED_BOT, 'untested bot')
    @mock.patch.object(core, 'get_captcha', mocked_get_captcha)
    def test_bot(self):
        # Telegram resolution for dates is 1 second, so TGS offers the same resolution,
        # this can generate false errors using the /start command
        # that's why CAPTCHA_TIMER must be greater than one second,
        # as a precaution, it is set at a minimum of two seconds
        self.assertGreater(CFG.CAPTCHA_TIMER, datetime.timedelta(seconds=2))
        # Run test
        self.bio.run(INTERACTIONS)
