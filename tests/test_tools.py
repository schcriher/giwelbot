# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import math
import random       # pylint: disable=unused-import
import itertools
import unicodedata

from environ import unittest, mock, tools, EmptyGenericClass


class TestToolsRegexes(unittest.TestCase):


    def test_starts_with_space(self):
        data = (
            ' ',
            '  ',
            ' test',
            '  test',
            '\t',
            '\t\t',
            '\n',
            '\n\n',
            '\n\nTest',
            '\r',
            '\x0b',
            '\x0c',
            '\xa0',
            '\u2002',
            '\u2003',
            '\u2004',
            '\u2005',
            '\u2006',
            '\u2007',
            '\u2008',
            '\u2009',
            '\u200a',
            '\u202f',
            '\u205f',
            '\u3000',
            '\u3000Test',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertTrue(bool(tools.STARTS_WITH_SPACE.search(entry)))
        data = (
            'test',
            '@test',
            '[test]',
            '-test-',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.STARTS_WITH_SPACE.search(entry)))


    def test_no_word_re(self):
        data = (
            ('test', 'test'),
            ('(test)', ' test '),
            ('test-test-test', 'test test test'),
            ('test || test', 'test test'),
            ('1.test', ' test'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertTrue(tools.NO_WORD_RE.sub(' ', entry), expected)


    def test_greetings_re(self):
        data = (
            'Bienvenido', 'Bienvenidos', 'Bienvenida', 'Bienvenidas', 'Bonvenon',
            'Welcome', 'Greetings', 'Willkommen', 'Begrüßung', 'Begrüßen',
            'Benvenuti', 'Benvenuto', 'Benvenuta', 'Accoglienza', 'Salutiamo',
            'Bienvenu', 'Bienvenue', 'Bienvenues', 'Boas-vindas', 'Bem-Vindo',
            'Acolhimento', 'Witajcie', 'Witamy', 'Vitejte',
            'pozhalovat', 'Privetstvuiu', 'Poprivetstvuite', 'Schastlivo', 'Kalos',
            #'пожаловать', 'Приветствую', 'Поприветствуйте', 'Счастливо', 'Καλώς',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertTrue(bool(tools.GREETINGS_RE.search(entry)))
        data = (
            'pregunta', 'respuesta', 'problema', 'víboras', 'juegos',
            'query', 'reply', 'problem', 'snakes', 'games',
            'Abfrage', 'Antwort', 'Problem', 'Vipern', 'Spiele',
            'question', 'répliquer', 'problématique', 'vipères', 'jeux',
            'vraag', 'repliek', 'probleem', 'slangen', 'spellen',
            'спрашивать', 'отзыв', 'проблема', 'гадюки', 'игры',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.GREETINGS_RE.search(entry)))


    def test_admins_re(self):
        data = (
            '@admin',
            '@admin test',
            'test @admin',
            'test @admin test',
            '@admins',
            '@admins test',
            'test @admins',
            'test @admins test',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertTrue(bool(tools.ADMINS_RE.search(entry)))
        data = (
            '"@admin"',
            '"@admins"',
            "'@admin'",
            "'@admins'",
            '-@admin-',
            '-@admins-',
            '|@admin|',
            '|@admins|',
            'use @admin?',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.ADMINS_RE.search(entry)))


    def test_spaces_re(self):
        data = (
            ' ',
            '\n',
            '  ',
            '\n ',
            '   ',
            '\n\f',
            '\t\t',
            ' \t',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertTrue(bool(tools.SPACES_RE.search(entry)))
        data = (
            '',
            'abc',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.SPACES_RE.search(entry)))


    def test_lang_re(self):
        # ISO 639 language codes
        # ISO 3166 country codes
        data = (
            ('es', 'es'),
            ('es_AR', 'es_AR'),
            ('es_AR.UTF-8', 'es_AR'),
            ('en', 'en'),
            ('en_US', 'en_US'),
            ('en_US@piglatin', 'en_US'),
            ('en_GB', 'en_GB'),
            ('sr_RS@latin', 'sr_RS'),
            ('fil', 'fil'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                obj = tools.LANG_RE.search(entry)
                self.assertEqual(obj.group('lll_CC'), expected)
        data = (
            '@piglatin', '.UTF-8', 'spanish_AR', 'es_ARGENTINA', 'ES_ar'
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.LANG_RE.match(entry)))


    def test_list_re(self):
        data = (
            ('', ['']),
            ('test', ['test']),
            ('te,st', ['te,st']),
            ('a;b', ['a', 'b']),
            ('a; b', ['a', 'b']),
            ('a ; b', ['a', 'b']),
            ('a ; b;c', ['a', 'b', 'c']),
            ('a ; b; c', ['a', 'b', 'c']),
            ('a ; b ; c', ['a', 'b', 'c']),
            ('a\n;\vb\f;c', ['a', 'b', 'c']),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                lst = tools.LIST_RE.split(entry)
                self.assertEqual(lst, expected)


class TestTools(unittest.TestCase):


    def test_novis_var(self):
        valid = ('SPACE', 'INVISIBLE', 'FUNCTION APPLICATION')
        chars = tools.INVISIBLE_CHARS + tools.ZW_SPACE_CHARS + tools.SPACE_CHARS
        for char in chars:
            with self.subTest(char=f'\\u{ord(char):0>4x}'):
                name = unicodedata.name(char)
                self.assertTrue(any(c in name for c in valid))


    def test_get_type(self):
        data = (
            (bool, bool),
            (True, bool),
            (False, bool),

            (str, str),
            ('abc', str),

            (int, int),
            (1, int),
            (10, int),

            (float, float),
            (1.0, float),
            (10., float),

            (list, list),
            ([], list),
            ([1, 'a'], list),
            ([2], list),
            ([3, 4], list),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools.get_type(entry), expected)


    def test_to_int(self):
        data = (
            ('1', 1),
            ('2.', 2),
            ('3.', 3),
            ('1.0', 1),
            ('1.1', 1),
            ('1.1', 1),
            ('10.1', 10),
        )
        for val, expected in data:
            with self.subTest(val=val, expected=expected):
                self.assertEqual(tools.to_int(val), expected)


    def test_mapper(self):
        data = (
            (bool, 'true', True),
            (True, 'true', True),
            (True, 'True', True),
            (True, 'TRUE', True),
            (False, 'TRUE', True),
            (True, '1', True),
            (False, '1', True),
            (True, 'yes', True),
            (False, 'yes', True),

            (bool, 'false', False),
            (True, 'False', False),
            (True, 'FALSE', False),
            (True, 'FALSE', False),
            (True, '0', False),
            (False, '0', False),
            (True, 'no', False),
            (False, 'no', False),

            (int, '1.1', 1),
            (1, '1', 1),
            (1, '1.', 1),
            (1, '1.0', 1),
            (1, '1.1', 1),

            # Is necessary to pass a list of something
            # to know the type of data of the elements
            #(list, '1;2', [1, 2]),  # TypeError
            ([0], '1;2', [1, 2]),
            ([int], '1;2', [1, 2]),
            (['0'], '1;2', ['1', '2']),
            ([str], '1;2', ['1', '2']),

            (float, '5', 5.0),
            (1.1, '5', 5.0),

            (str, 'abc', 'abc'),
            ('xyz', 'abc', 'abc'),
            ('abc', 'abc', 'abc'),
            ('abc', 'aBc', 'aBc'),
        )
        for obj, val, expected in data:
            with self.subTest(obj=obj, val=val, expected=expected):
                value = tools.mapper(obj, val)
                self.assertEqual(value, expected)
                typ = obj if obj.__class__ is type else obj.__class__
                self.assertIsInstance(value, typ)

        data = (
            (bool, '2'),
            (bool, 'ja'),
            (bool, 'test'),
        )
        for obj, val in data:
            with self.subTest(obj=obj, val=val):
                with self.assertRaises(ValueError):
                    tools.mapper(obj, val)


    def test__name(self):
        # pylint: disable=protected-access
        data = (
            (None, ''),
            ('', ''),
            (' ', ''),
            ('name', 'name'),
            ('  name  ', 'name'),
            ('\t name\v\n', 'name'),
            ('  na me  ', 'na me'),
            ('\t na me\v\n', 'na me'),
            ('  na  me  ', 'na  me'),
            ('\t na \fme\v\n', 'na \fme'),
            ('\u2063\u2064\u205f', ''),
            ('\u2063\u2064F\u205f', 'F'),
            ('\u2063F\u2064C\u205f', 'F\u2064C'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools._name(entry), expected)


    def test__name_gt(self):
        # pylint: disable=protected-access
        data = (
            (' ', 0, ''),
            (' ', 1, ''),
            ('name', 0, 'name'),
            ('name', 1, 'name'),
            ('name', 4, 'name'),
            ('name', 5, ''),
            ('  name  ', 1, 'name'),
            ('  name  ', 4, 'name'),
            ('  name  ', 5, ''),
            ('\t name\v\n', 3, 'name'),
            ('\t name\v\n', 4, 'name'),
            ('\t name\v\n', 5, ''),
            ('  na me  ', 5, 'na me'),
            ('  na me  ', 6, ''),
            ('\u2063\u2064\u205f', 2, ''),
            ('\u2063\u2064\u205f', 3, ''),
            ('\u2063\u2064\u205f', 4, ''),
        )
        for entry, min_length, expected in data:
            with self.subTest(entry=entry, min_length=min_length, expected=expected):
                self.assertEqual(tools._name_gt(entry, min_length), expected)


    def test_get_full_name(self):
        data = (
            ('', '', ''),
            ('first_name', '', 'first_name'),
            ('', 'last_name', 'last_name'),
            ('first_name', 'last_name', 'first_name last_name'),
            ('f', 'last_name', 'f last_name'),
            ('f  ', 'l  ', 'f l'),
            ('fi  ', 'la  ', 'fi la'),
            ('\u2003\u2004\u2005', '\f\f\f', ''),
            ('\u2003\u2004\u2005f  ', 'l  ', 'f l'),
            ('\u2003\u2004\u2005fi  ', 'la  ', 'fi la'),
        )
        for first_name, last_name, expected in data:
            with self.subTest(expected=expected):
                user = EmptyGenericClass(first_name=first_name,
                                         last_name=last_name)
                self.assertEqual(tools.get_full_name(user), expected)


    def test_get_part_name(self):
        data = (
            ('', '', ''),
            ('first_name', '', 'first_name'),
            ('', 'last_name', 'last_name'),
            ('first_name', 'last_name', 'first_name'),
            ('f', 'last_name', 'last_name'),
            ('f  ', 'l  ', 'f l'),
            ('fi  ', 'la  ', 'fi'),
            ('\u2003\u2004\u2005', '\f\f\f', ''),
            ('\u2003\u2004\u2005f  ', 'l  ', 'f l'),
            ('\u2003\u2004\u2005fi  ', 'la  ', 'fi'),
        )
        for first_name, last_name, expected in data:
            with self.subTest(expected=expected):
                user = EmptyGenericClass(first_name=first_name,
                                         last_name=last_name)
                self.assertEqual(tools.get_part_name(user, 2), expected)


    def test_get_mention(self):
        uid = 1
        data = (
            (None, None, None, True, False, False,
             f'ID:{uid}'),

            (None, None, None, True, False, True,
             f'<a href="tg://user?id={uid}">ID:{uid}</a>'),

            (None, None, None, True, True, False,
             f'ID:{uid}'),

            (None, None, None, True, True, True,
             f'<a href="tg://user?id={uid}">ID:{uid}</a>'),

            ('first_name', '', '', True, False, False,
             f'first_name'),

            ('first_name', '', '', True, False, True,
             f'<a href="tg://user?id={uid}">first_name</a>'),

            ('first_name', '', '', True, True, True,
             f'<a href="tg://user?id={uid}">first_name</a>'),

            ('', 'last_name', '', True, False, True,
             f'<a href="tg://user?id={uid}">last_name</a>'),

            ('', 'last_name', '', True, True, True,
             f'<a href="tg://user?id={uid}">last_name</a>'),

            ('', '', 'username', True, False, True,
             f'<a href="tg://user?id={uid}">@\ufeffusername</a>'),

            ('', '', 'username', True, True, True,
             f'<a href="tg://user?id={uid}">@\ufeffusername</a>'),

            ('first_name', 'last_name', 'username', True, False, True,
             f'<a href="tg://user?id={uid}">first_name last_name (@\ufeffusername)</a>'),

            ('first_name', 'last_name', 'username', False, False, True,
             f'<a href="tg://user?id={uid}">first_name</a>'),

            ('first_name', 'last_name', 'username', False, True, False,
             '@\ufeffusername'),

            ('first_name', 'last_name', 'username', False, True, True,
             f'<a href="tg://user?id={uid}">@\ufeffusername</a>'),

            ('first_name', 'last_name', 'username', True, True, True,
             f'<a href="tg://user?id={uid}">@\ufeffusername</a>'),

            ('f', 'last_name', '', True, False, True,
             f'<a href="tg://user?id={uid}">f last_name</a>'),

            ('f  ', 'l  ', 'username', True, False, False,
             'f l (@\ufeffusername)'),

            ('f  ', 'l  ', 'username', True, False, True,
             f'<a href="tg://user?id={uid}">f l (@\ufeffusername)</a>'),

            ('f  ', 'l  ', '', True, False, True,
             f'<a href="tg://user?id={uid}">f l</a>'),

            ('fi  ', 'la  ', '', True, False, True,
             f'<a href="tg://user?id={uid}">fi la</a>'),

            ('\u2003\u2004\u2005', '\f\f\f', '', True, False, True,
             f'<a href="tg://user?id={uid}">ID:{uid}</a>'),
        )
        for first, last, username, full, preferably_username, cite, expected in data:
            with self.subTest(expected=expected):
                user = EmptyGenericClass(id=uid,
                                         first_name=first,
                                         last_name=last,
                                         username=username)
                code = tools.get_mention(user, full, preferably_username, cite)
                self.assertEqual(code, expected)


    def test_get_arg(self):
        cut = ' '
        data = (
            ('', ''),
            (None, ''),
            ('text', ''),
            ('/', ''),
            ('/cmd', ''),
            ('/cmd ', ''),
            ('/ cmd ', ''),
            ('/cmd a', 'a'),
            ('/cmd text', 'text'),
            ('/cmd text ', 'text'),
            ('/cmd text for testing ', 'text for testing'),
            ('/cmd  \n\ftext for testing \n', '\n\ftext for testing'),
            ('/cmd    \ftext for\n  testing \n', '\ftext for\n  testing'),
            ('cmd text for testing ', ''),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools.get_arg(entry, cut), expected)


    def test_get_token(self):
        # All must be different at maximum generation speed
        lst = [tools.get_token() for _ in range(1000)]
        self.assertEqual(sorted(lst), sorted(set(lst)))
        self.assertTrue(all(isinstance(token, str) for token in lst))
        self.assertTrue(all(len(token) > 3 for token in lst))


    @mock.patch('random.seed')
    def test_change_seed(self, seed):
        self.assertFalse(seed.called)

        tools.change_seed()
        args, kwargs = seed.call_args_list[0]

        self.assertTrue(seed.called)
        self.assertIsInstance(args[0], float)


    def test_signature_and_verify_signature(self):
        data = b'test'
        key = b'key'
        size = 32

        sign = tools.signature(data, key, size)

        self.assertEqual(len(sign), size)
        self.assertIsInstance(sign, bytes)
        self.assertTrue(tools.verify_signature(data, sign, key, size))

        self.assertFalse(tools.verify_signature(data[1:], sign, key, size))
        self.assertFalse(tools.verify_signature(data, sign[1:], key, size))


    def test_remove_diacritics(self):
        data = (
            ('Hernán', 'Hernan'),
            ('schön', 'schon'),
            ('lEçon', 'lEcon'),
            ('áíóúý', 'aiouy'),
            ('Año 3', 'Ano 3'),
            ('Conceição', 'Conceicao'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools.remove_diacritics(entry), expected)


    def test_sorted_alphabetically(self):
        data = (
            (['Hernu', 'Hernandez', 'Hernán'], ['Hernán', 'Hernandez', 'Hernu']),
            # Normal sort: ['Hernandez', 'Hernu', 'Hernán']

            (['schön', 'schons', 'schun'], ['schön', 'schons', 'schun']),
            # Normal sort: ['schons', 'schun', 'schön']
        )
        for lst, expected in data:
            with self.subTest(lst=lst, expected=expected):
                lst.sort(key=tools.sorted_alphabetically)
                self.assertEqual(lst, expected)


    def test_pseudo_text_length(self):
        data = (                    # total = norm + slim (slim // 2)
            ('test', 3),            #     4      2      2           1
            ('trial option', 8),    #    12      5      7           3
            ('signalled opt', 10),  #    13      8      5           2
            ('test (a.b)', 7),      #    10      4      6           3
        )
        # length = norm + slim // 2
        for text, length in data:
            with self.subTest(text=text, length=length):
                self.assertEqual(tools.pseudo_text_length(text), length)


    def test_get_indexes(self):
        kb_wide = 30
        kb_isep = 5

        # All in a single row
        data = (
            ['A'],
            ['ABC'],
            ['YES', 'NO'],
            ['A-Z', 'Z-A', 'RANDOM'],
            ['a', 'b' * (kb_wide - kb_isep - 9)],
            ['a', 'b' * (kb_wide - kb_isep - 1)],
        )
        for texts in data:
            expected = [slice(0, len(texts), None)]
            with self.subTest(texts=texts, expected=expected):
                indexes = tools.get_indexes(texts, kb_wide, kb_isep)
                self.assertEqual(indexes, expected)

        # One short and one long item, two rows
        expected = [slice(0, 1, None), slice(1, 2, None)]
        data = (
            ['a', 'b' * (kb_wide - kb_isep)],
            ['a', 'b' * (kb_wide)],
            ['a', 'b' * (kb_wide + kb_isep)]
        )
        for texts in data:
            with self.subTest(texts=texts, expected=expected):
                indexes = tools.get_indexes(texts, kb_wide, kb_isep)
                self.assertEqual(indexes, expected)

        # Ensure 2 rows for 6 numbers of one or two characters
        expected = [slice(0, 3, None), slice(3, 6, None)]
        for texts in itertools.product(('a', 'ab'), repeat=6):
            with self.subTest(texts=texts, expected=expected):
                indexes = tools.get_indexes(texts, kb_wide, kb_isep)
                self.assertEqual(indexes, expected)

        # Short elements with a long one
        for iwide in range(1, 4):
            for lwide in range(1, kb_wide):
                # Conditions:

                # The long item (lwide) can not go next to the shorts
                # [num_min]: num * iwide + num * kb_isep + lwide > kb_wide
                num_min = math.ceil((kb_wide - lwide) / (iwide + kb_isep))

                # The short items (iwide) must fit in a row
                # [num_max]: num * iwide + (num - 1) * kb_isep < kb_wide
                num_max = math.floor((kb_wide + kb_isep) / (iwide + kb_isep))

                # Two or more short items
                if num_min > 1:
                    for num in range(num_min, num_max + 1):

                        # There must be a good difference between short and long items
                        cond1 = iwide * (num - 1) < lwide

                        # By rounding off (floor) can get all the items in one row
                        cond2 = iwide * num + lwide + kb_isep * num > kb_wide

                        if cond1 and cond2:

                            texts = ['a' * iwide] * num + ['b' * lwide]
                            expected = [slice(0, num, None), slice(num, num + 1, None)]

                            with self.subTest(texts=texts, expected=expected):
                                indexes = tools.get_indexes(texts, kb_wide, kb_isep)
                                self.assertEqual(indexes, expected)


    def test_set_inlinekeyboardbutton(self):
        items = [('a', 'A'), ('b', None, 'B')]
        texts = []
        objs = []
        tools.set_inlinekeyboardbutton(items, texts, objs)
        self.assertEqual(texts, ['a', 'b'])

        self.assertEqual(objs[0].text, 'a')
        self.assertEqual(objs[0].callback_data, 'A')
        self.assertEqual(objs[0].url, None)

        self.assertEqual(objs[1].text, 'b')
        self.assertEqual(objs[1].callback_data, None)
        self.assertEqual(objs[1].url, 'B')


    def test_get_keyboard(self):
        long = 'd' * 16
        data = (
            (('a', 'b', 'c'),
             None,
             {},
             {'keyboard': [['a', 'b', 'c']],
              'resize_keyboard': False,
              'one_time_keyboard': False,
              'selective': False}),

            (('aaaa', 'bbbb', 'cccc', long),
             ('d',),
             {'selective': True},
             {'keyboard': [['aaaa', 'bbbb', 'cccc'], [long], ['d']],
              'resize_keyboard': False,
              'one_time_keyboard': False,
              'selective': True}),

            ((('a', 'A'), ('b', 'B')),
             None,
             {},
             {'inline_keyboard': [[{'text': 'a', 'callback_data': 'A'},
                                   {'text': 'b', 'callback_data': 'B'}]]}),

            ((('a', 'A', None), ('b', None, 'B')),
             None,
             {},
             {'inline_keyboard': [[{'text': 'a', 'callback_data': 'A'},
                                   {'text': 'b', 'url': 'B'}]]}),
        )
        for main, footer, params, expected in data:
            with self.subTest(main=main, footer=footer, params=params):
                dic = tools.get_keyboard(main, footer, **params).to_dict()
                self.assertEqual(dic, expected)
