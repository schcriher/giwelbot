# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import datetime

from environ import unittest, mock, os, config, CFG

USERNAME = 'GiWelBot'

BID = 111
FID = 222

TOKEN = f'{BID}:abc'
BOT_URL = f'https://t.me/{USERNAME}'

ENVIRON = {
    'TELEGRAM_USERNAME': USERNAME,
    'TELEGRAM_TOKEN': TOKEN,
    'TELEGRAM_FID': str(FID),
    'TELEGRAM_DID': str(FID),
    'a': '1.0',
    'b': '1.5',
    'c': 'true',
    'd': '10.0',
    'e': '20.0',
    'f': 'no',
    'h': '1;2;3',
    'i': 'a;b; 3',
    'wrong_boolean': 'text',    # must be: true, 1, yes, false, 0, no (case-insensitive)
    'key_without_default': 'text',
}

DEFAULT = {
    'TELEGRAM_USERNAME': str,
    'TELEGRAM_TOKEN': str,
    'TELEGRAM_FID': int,
    'TELEGRAM_DID': int,
    'LOG_TIME': 'LOG_TIME',
    'LOG_LINE': 'LOG_LINE',
    'a': int,
    'b': float,
    'c': bool,
    'd': 1,
    'e': 2.0,
    'f': True,
    'g': 'no environ',
    'h': [int],
    'i': ['aa', 'bb', '33'],
    'wrong_boolean': bool,
    'type_without_environ': str,    # as it is not present in ENVIRON
                                    # it must have a value and not a type
    'A_TIMER': 1,                   # *_TIMER is read as a delta in seconds
    'B_RESTRICTION': 2,             # *_RESTRICTION is read as a delta in seconds
    'C_DAYS': 3,                    # *_DAYS is read as a delta in days
}

class TestConfig(unittest.TestCase):

    def setUp(self):
        # pylint: disable=protected-access
        CFG.__dict__.clear()

    def tearDown(self):
        # pylint: disable=protected-access
        CFG.__dict__.clear()

    @mock.patch.object(os, 'environ', ENVIRON)
    @mock.patch.object(config, 'DEFAULT', DEFAULT)
    def test_cfg(self):

        self.assertEqual(CFG.bot_url, BOT_URL)
        self.assertEqual(CFG.bid, BID)
        self.assertEqual(CFG.fid, FID)
        self.assertEqual(CFG.did, FID)

        key = CFG.signature_key
        self.assertEqual(len(key), 64)
        self.assertIsInstance(key, bytes)
        self.assertGreater(len(set(key)), 32)

        CFG.DATETIME_IN_LOG = True
        self.assertEqual(CFG.log_format, 'LOG_TIME LOG_LINE')
        CFG.DATETIME_IN_LOG = False
        self.assertEqual(CFG.log_format, 'LOG_LINE')

        self.assertEqual(CFG.TELEGRAM_USERNAME, USERNAME)
        self.assertEqual(CFG.TELEGRAM_TOKEN, TOKEN)
        self.assertEqual(CFG.TELEGRAM_FID, FID)
        self.assertEqual(CFG.TELEGRAM_DID, FID)

        data = (
            (CFG.a, 1),
            (CFG.b, 1.5),
            (CFG.c, True),
            (CFG.d, 10),
            (CFG.e, 20.0),
            (CFG.f, False),
            (CFG.g, 'no environ'),
            (CFG.h, [1, 2, 3]),
            (CFG.i, ['a', 'b', '3']),
            (CFG.A_TIMER, datetime.timedelta(seconds=1)),
            (CFG.B_RESTRICTION, datetime.timedelta(seconds=2)),
            (CFG.C_DAYS, datetime.timedelta(days=3)),
        )
        for value, expected in data:
            with self.subTest(value=value, expected=expected):
                self.assertEqual(value, expected)
                self.assertEqual(type(value), type(expected))

        with self.assertRaises(ValueError):
            _ = CFG.wrong_boolean

        with self.assertRaises(KeyError):
            _ = CFG.key_without_default

        with self.assertRaises(KeyError):
            _ = CFG.type_without_environ
