# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

# pylint: disable=too-many-lines

# https://core.telegram.org/bots/api
# https://github.com/python-telegram-bot/python-telegram-bot/wiki/Webhooks

import re
import sys
import copy
import json
import time
import queue
import inspect
import logging
import operator
import itertools
import functools
import traceback
import collections

from telegram import Bot, Update
from telegram.ext import Dispatcher, JobQueue
from telegram.error import TelegramError, BadRequest, Unauthorized, ChatMigrated

BOTID = 11111
DEVID = 22222
ADMID = 33333
USRID = 44444

BOT1ID = 1011
BOT2ID = 1022
BOT3ID = 1033
DEV1ID = 2011
DEV2ID = 2022
DEV3ID = 2033
ADM1ID = 3011
ADM2ID = 3022
ADM3ID = 3033
USR1ID = 4011
USR2ID = 4022
USR3ID = 4033

G1ID = -10011
G2ID = -10012
G3ID = -10013
S1ID = -10021
S2ID = -10022
S3ID = -10023
C1ID = -10031
C2ID = -10032
C3ID = -10033

TOKEN = f'{BOTID}:abcdefghijklmnopqrstuvwxyz'

USER_HEAD = ('id', 'is_bot', 'username', 'first_name', 'last_name')
USER_DATA = ((BOTID, True, 'bot', 'Bot', 'Bot'),
             (DEVID, False, 'dev', 'Dev', 'Dev'),
             (ADMID, False, 'adm', 'Adm', 'Adm'),
             (USRID, False, 'usr', 'Usr', 'Usr'),
             (BOT1ID, True, 'bot1', 'Bot1', 'Bot1'),
             (BOT2ID, True, 'bot2', 'Bot2', 'Bot2'),
             (BOT3ID, True, 'bot3', 'Bot3', 'Bot3'),
             (DEV1ID, False, 'dev1', 'Dev1', 'Dev1'),
             (DEV2ID, False, 'dev2', 'Dev2', 'Dev2'),
             (DEV3ID, False, 'dev3', 'Dev3', 'Dev3'),
             (ADM1ID, False, 'adm1', 'Adm1', 'Adm1'),
             (ADM2ID, False, 'adm2', 'Adm2', 'Adm2'),
             (ADM3ID, False, 'adm3', 'Adm3', 'Adm3'),
             (USR1ID, False, 'usr1', 'Usr1', 'Usr1'),
             (USR2ID, False, 'usr2', 'Usr2', 'Usr2'),
             (USR3ID, False, 'usr3', 'Usr3', 'Usr3'))

CHAT_HEAD = ('id', 'title', 'username', 'type')
CHAT_DATA = ((G1ID, 'G1', 'g1', 'group'),
             (G2ID, 'G2', 'g2', 'group'),
             (G3ID, 'G3', 'g3', 'group'),
             (S1ID, 'S1', 's1', 'supergroup'),
             (S2ID, 'S2', 's2', 'supergroup'),
             (S3ID, 'S3', 's3', 'supergroup'),
             (C1ID, 'C1', 'c1', 'channel'),
             (C2ID, 'C2', 'c2', 'channel'),
             (C3ID, 'C3', 'c3', 'channel'))

EXTRACT_ENTITIES_HTML = {
    'bold': re.compile(r'<(b|strong)>(?P<text>[^<]+)</\1>'),
    'italic': re.compile(r'<(i|em)>(?P<text>[^<]+)</\1>'),
    'code': re.compile(r'<(code)>(?P<text>[^<]+)</\1>'),
    'pre': re.compile(r'<(pre)>(?P<text>[^<]+)</\1>'),
    'bot_command': re.compile(r'/\w+'),
    'mention': re.compile(r'@\w+'),
    'hashtag': re.compile(r'#\w+'),
}

INTRODUCE_ENTITIES_HTML = {
    'bold': '<b>{}</b>',
    'italic': '<i>{}</i>',
    'code': '<code>{}</code>',
    'pre': '<pre>{}</pre>',
}

STATUS_LETTER = {
    'a': 'administrator',
    'c': 'creator',
    'm': 'member',
    'l': 'left',
    'k': 'kicked',
    'r': 'restricted',
}


# ---


BASE0 = {'file_id': 'FILE_ID', 'file_size': 123}

BASE1 = BASE0.copy()
BASE1['height'] = 123
BASE1['width'] = 123

BASE2 = BASE0.copy()
BASE2['thumb'] = BASE1.copy()

PHOTO = [BASE1.copy(), BASE1.copy()]

STICKER = BASE1.copy()
STICKER['emoji'] = '👍'
STICKER['is_animated'] = False
STICKER['set_name'] = 'SET_NAME'
STICKER['thumb'] = BASE1.copy()

PDF = BASE2.copy()
PDF['file_name'] = 'FILE_NAME.pdf'
PDF['mime_type'] = 'document/pdf'

GIF_DOCUMENT = BASE2.copy()
GIF_DOCUMENT['file_name'] = 'FILE_NAME.mp4'
GIF_DOCUMENT['mime_type'] = 'video/mp4'

GIF_ANIMATION = GIF_DOCUMENT.copy()
GIF_ANIMATION['duration'] = 1
GIF_ANIMATION['height'] = 123
GIF_ANIMATION['width'] = 123

MEDIA_MAP = {
    'photo': {'photo': PHOTO},
    'sticker': {'sticker': STICKER},
    'document': {'document': PDF},
    'animation': {'document': GIF_DOCUMENT, 'animation': GIF_ANIMATION},
}


# ---


def flogger(func):
    logger = logging.getLogger(func.__module__)

    @functools.wraps(func)
    def flogger_wrapper(self, *args, **kwargs):
        logger.debug('→ %s: %s, %s', func.__name__, args, kwargs)
        result = func(self, *args, **kwargs)
        logger.debug('← %s: %s', func.__name__, result)
        return result

    return flogger_wrapper


def is_channel(dic):
    """ Determines if a chat or message is or comes from a channel """
    if 'chat' in dic:
        return dic['chat']['type'] == 'channel'
    if 'type' in dic:
        return dic['type'] == 'channel'
    raise ValueError('cannot get the chat type')


def adjust_offsets(entities, end, add):
    """ Adjusts the positions by inserting the format entities """
    for entity in entities:
        if entity['offset'] >= end:
            entity['offset'] += add


def extract_entities_html(text):
    """ Extracts the formatting entities from the text """
    entities = []
    for typ, pattern in EXTRACT_ENTITIES_HTML.items():
        obj = pattern.search(text)
        while obj:
            start, end = obj.span()
            for entity in entities:
                _start = entity['offset']
                _end = _start + entity['length']
                if not (start >= _end or end <= _start):
                    break  # tags must not be nested (CHANGE in API 4.5)
            else:
                components = obj.groupdict()
                if components:
                    num = len(text)
                    text = text[:start] + components['text'] + text[end:]
                    num = len(text) - num  # if text is shorter the num is negative
                    end += num
                    adjust_offsets(entities, end, num)

                length = end - start
                entities.append({'type': typ, 'offset': start, 'length': length})
            obj = pattern.search(text, end)
    entities.sort(key=operator.itemgetter('offset'))
    return text, entities


def introduce_entities_html(text, entities):
    """ Introduces formatting entities to the text """
    entities = copy.deepcopy(entities or [])
    entities.sort(key=operator.itemgetter('offset'))
    while entities:
        entity = entities.pop()
        template = INTRODUCE_ENTITIES_HTML.get(entity['type'])
        if template:
            start = entity['offset']
            end = start + entity['length']
            num = len(text)
            text = text[:start] + template.format(text[start:end]) + text[end:]
            num = len(text) - num  # if text is longer the num is positive
            end += num
            adjust_offsets(entities, end, num)
    return text


def extract_markup(markup):
    """ Extracts the texts from the keyboard markup """
    texts = []

    if 'keyboard' in markup:
        for row in markup['keyboard']:
            for col in row:
                texts.append(col)
        return texts

    if 'inline_keyboard' in markup:
        for row in markup['inline_keyboard']:
            for col in row:
                texts.append(col['text'])
        return texts

    raise ValueError('incorrect reply_markup')


def get_callback_data(message, text):
    """ Extract data from a callback, that corresponds to the text """
    ikb = message['reply_markup']['inline_keyboard']
    for row in ikb:
        for col in row:
            if col['text'] == text:
                return col['callback_data']
    raise KeyError(text)


def return_deepcopy(func):
    """ Returns a copy of the result """
    @functools.wraps(func)
    def return_deepcopy_wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return copy.deepcopy(result)
    return return_deepcopy_wrapper


def dict_to_bytes(func):
    """ Decorator to convert a dictionary to a small json encoded in bytes """
    @functools.wraps(func)
    def dict_to_bytes_wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return json.dumps(result).encode()
    return dict_to_bytes_wrapper


def remove_keyboard(obj):
    """ Decorator to remove the "keyboard" from the reply markup """

    # Decorator
    if inspect.isfunction(obj):
        @functools.wraps(obj)
        def remove_keyboard_wrapper(*args, **kwargs):
            result = obj(*args, **kwargs)
            result = remove_keyboard(result)
            return result
        return remove_keyboard_wrapper

    # Removal function
    if isinstance(obj, dict):

        markup = obj.get('reply_markup')
        if markup and 'keyboard' in markup:
            obj.pop('reply_markup')

        for value in obj.values():
            remove_keyboard(value)

    return obj


def make_dict(head, row):
    """ Create a dictionary with lists of names (head) and values (row) """
    data = {}
    for param, value in zip(head, row):
        data[param] = value
    return data


def get_int(dic, var):
    """ Returns an integer of the 'var' variable present in 'dic'
        If it does not exist returns None
    """
    if dic and var in dic:
        return int(dic[var])
    return None


def get_author_signature(user):
    """ Returns the signature of the author of a channel """
    return f'{user["first_name"] or ""} {user["last_name"] or ""}'.strip()


def check_message(messages):
    """ Checks that messages are in ascending order and are not duplicated """
    indexes = [m['message_id'] for m in messages]

    if indexes != sorted(indexes):
        raise IndexError('library error, disordered messages')

    if indexes != sorted(set(indexes)):
        raise IndexError('library error, duplicate messages')


# ---


USERS = {}      # {user_id: dict}
CHATS = {}      # {chat_id: dict}
NICKS = {}      # {username: chat_id/user_id}
STATUS = {}     # {chat_id: {user_id: status}}
CONSENT = {}    # {chat_id: True/False}             Can the bot send message?

_status = {}    # pylint: disable=invalid-name

for _row in USER_DATA:
    _data = make_dict(USER_HEAD, _row)
    _author = get_author_signature(_data)

    assert _data['id'] not in USERS
    assert _data['id'] not in CHATS
    assert _data['id'] not in CONSENT
    assert _data['username'] not in NICKS
    assert _author not in NICKS

    USERS[_data['id']] = copy.deepcopy(_data)
    NICKS[_data['username']] = _data['id']
    NICKS[_author] = _data['id']
    CONSENT[_data['id']] = False

    if _data['is_bot']:
        _status[_data['id']] = 'member'
    else:
        _status[_data['id']] = 'left'
        _data.pop('is_bot')
        _data['type'] = 'private'
        CHATS[_data['id']] = _data

for _row in CHAT_DATA:
    _data = make_dict(CHAT_HEAD, _row)

    assert _data['id'] not in CHATS
    assert _data['id'] not in STATUS
    assert _data['id'] not in CONSENT
    assert _data['username'] not in NICKS

    CHATS[_data['id']] = _data
    STATUS[_data['id']] = copy.deepcopy(_status)
    NICKS[_data['username']] = _data['id']
    CONSENT[_data['id']] = True

del _row
del _data
del _author
del _status


# ---


class DispatcherHandlersDict(dict):

    def __getitem__(self, key):
        handlers = super().__getitem__(key)
        for handler in handlers:
            if not hasattr(handler, "__wrapped__"):
                handler.handle_update = self.handle_update(handler.handle_update)
                handler.__wrapped__ = True
        return handlers

    @classmethod
    def handle_update(cls, method):
        @functools.wraps(method)
        def handle_update_wrapper(*args, **kwargs):
            # pylint: disable=bare-except
            try:
                return method(*args, **kwargs)
            except BadRequest:
                raise
            except:
                # Allows to stop the execution of the tests
                # if there is a bug in the bot code.
                sys.exit('bot execution error')
        return handle_update_wrapper


class FakeJobQueue(JobQueue):
    """ Modification of the tick method for manual operation on a single thread."""

    def tick(self):
        self.logger.debug('Ticking jobs with t=%f', time.time())

        try:
            next_t, job = self._queue.get(False)
        except queue.Empty:
            return

        self.logger.debug('Peeked at %s with t=%f', job.name, next_t)

        if job.removed:
            self.logger.debug('Removing job %s', job.name)
            return

        if job.enabled:
            try:
                self.logger.debug('Running job %s', job.name)
                job.run(self.bot)

            except Exception:  # pylint: disable=broad-except
                self.logger.exception('An uncaught error was raised while '
                                      'executing job %s',
                                      job.name)
        else:
            self.logger.debug('Skipping disabled job %s', job.name)

        if job.repeat and not job.removed:
            self._put(job, last_t=next_t)
        else:
            self.logger.debug('Dropping non-repeating or removed job %s', job.name)


class TelegramServer:
    """ Flow of requests and responses:

        Real    user    telegram    bot
                 |         |<-------a|
                 |         |b------->|
                 |         |         |
                 |c------->|d------->|
                 |<------f1|<-------e|
                 |         |f2------>|
        Fake  req/res    (TGS)
    """
    # pylint: disable=too-many-instance-attributes

    BASE_URL = 'https://api.telegram.org/bot'
    CMD_URL = re.compile(rf'{BASE_URL}(?P<TOKEN>(?P<ID>\d+):[\w-]+)/(?P<CMD>\w+)')

    def __init__(self):
        """ Fake Telegram Server Constructor """
        self.logger = logging.getLogger(__name__)

        self.tick = 0       # mark of runs of request
        self.unique_id = 0  # for chat_instance and id in callbacks, and update_id
        self.messages = {}  # all messages, {chat_id: [message,]}

        self.users = copy.deepcopy(USERS)       # {user_id: dict}
        self.chats = copy.deepcopy(CHATS)       # {chat_id: dict}
        self.nicks = copy.deepcopy(NICKS)       # {username: chat_id/user_id}
        self.status = copy.deepcopy(STATUS)     # {chat_id: {user_id: status}}
        self.consent = copy.deepcopy(CONSENT)   # {chat_id: True/False}
        self.migrations = {}                    # {old_chat_id: new_chat_id}

        self.responses = collections.deque()
        self.next_sends = collections.deque()
        self.exceptions = {}

        self.bot = None
        self.job_queue = None
        self.dispatcher = None


    def add_handlers(self, add_handlers):
        """ Set the handlers (from the bot) on the dispatcher """
        self.bot = Bot(TOKEN)
        self.job_queue = FakeJobQueue(self.bot)
        self.dispatcher = Dispatcher(self.bot,
                                     update_queue=None,
                                     workers=0,
                                     job_queue=self.job_queue)
        self.dispatcher.handlers = DispatcherHandlersDict()

        add_handlers(self.dispatcher)

        # Patch bot actions
        # pylint: disable=protected-access
        request = self.bot._request
        request._request_wrapper = self._request(request._request_wrapper)


    @flogger
    def add_exception_throwing(self, exception, tick=None, cmd=None):
        if tick is None:
            abs_tick = self.tick + 1
            tick = 1
            while abs_tick in self.exceptions:
                abs_tick += 1
                tick += 1
        else:
            tick = int(tick)
            if tick < 1:
                raise ValueError('will never be executed, must be tick >= 1')
            abs_tick = self.tick + tick
            if abs_tick in self.exceptions:
                raise ValueError('already existing tick value')
        self.exceptions[abs_tick] = (exception, cmd.lower() if cmd else None, tick)


    def _request(self, func):
        """ Bot request patcher """
        @flogger
        @functools.wraps(func)
        def _request_wrapper(_method, url, **kwargs):
            """ Request method replacement `_request_wrapper` from `bot._request`
                is a wrap of `urllib3.request.RequestMethods`
            """
            self.tick += 1
            try:
                match = self.CMD_URL.match(url)
                command = match.group('CMD').lower()
                body = kwargs.get('body')
                body = json.loads(body) if body else None

                if self.tick in self.exceptions:
                    exc, cmd, tick = self.exceptions.pop(self.tick)
                    if cmd is None or cmd == command:
                        raise exc
                    exc_typ = exc if exc.__class__ is type else exc.__class__
                    sys.exit(f'{exc_typ.__name__} should have been thrown, '
                             f'but the command ({cmd}) does not match '
                             f'in tick {tick}|{self.tick}')

                return self.request(command, body)
            except TelegramError:
                raise
            except:
                traceback.print_exc()
                raise
        return _request_wrapper


    @dict_to_bytes
    @remove_keyboard
    @return_deepcopy
    @flogger
    def request(self, cmd, body):
        """ Server Request """
        # pylint: disable=too-many-branches,too-many-locals,too-many-statements

        message = None
        response = None

        cid = get_int(body, 'chat_id')
        uid = get_int(body, 'user_id')
        mid = get_int(body, 'message_id')

        if cid is not None:
            self._check_chat_settings(cid)

        if cmd == 'getme':
            response = {'ok': True, 'result': self.get_user('bot')}

        elif cmd in ('answercallbackquery', 'sendchataction'):
            response = {'ok': True, 'result': True}

        elif cmd == 'leavechat':
            self.status[cid][BOTID] = 'left'
            response = {'ok': True, 'result': True}

        elif cmd in ('sendmessage', 'editmessagetext'):
            message = self.process_message(body)
            response = {'ok': True, 'result': message}

        elif cmd == 'deletemessage':
            self.del_message(cid, mid)
            response = {'ok': True, 'result': True}

        elif cmd == 'forwardmessage':
            message = self.process_message(body)
            response = {'ok': True, 'result': message}

        elif cmd in ('sendphoto', 'sendsticker', 'senddocument', 'sendanimation'):
            message = self.generate_message(BOTID, cid, cmd[4:])
            response = {'ok': True, 'result': message}

        elif cmd == 'getchatmember':
            member = {
                'user': self.get_user(uid),
                'status': self.status[cid][uid]
            }
            response = {'ok': True, 'result': member}

        elif cmd == 'getchat':
            chat = self.get_chat(cid)
            response = {'ok': True, 'result': chat}

        elif cmd == 'getchatadministrators':
            result = []
            for user_id, status in self.status[cid].items():
                if status in ('creator', 'administrator'):
                    user = self.get_user(user_id)
                    data = {'status': status, 'user': user}
                    result.append(data)
            response = {'ok': True, 'result': result}

        elif cmd == 'restrictchatmember':
            response = self._check_member_edit_permission(cmd, cid, uid)
            unrestrict = all((
                body.get('can_send_messages', False),
                body.get('can_send_media_messages', False),
                body.get('can_send_other_messages', False),
                body.get('can_add_web_page_previews', False),
            ))
            status = 'member' if unrestrict else 'restricted'
            self.status[cid][uid] = status

        elif cmd == 'kickchatmember':
            response = self._check_member_edit_permission(cmd, cid, uid)
            self.status[cid][uid] = 'kicked'
            self.next_sends.append({'user': BOTID,
                                    'chat': cid,
                                    'content': f'¢:members:-{uid}'})

        if response:
            if message:
                text = message.get('text', '')
                if len(text) > 4096:
                    raise BadRequest(f'Message is too long')

                self.add_message(message)       # store
                self.responses.append(message)  # send

            return response

        raise NotImplementedError(cmd)


    @flogger
    def _check_chat_settings(self, cid):
        if cid == BOTID:
            raise ValueError('bot not sent things to itself')

        # If cid not exists can be a proof of non-existent chat

        if cid not in self.chats:
            raise BadRequest('chat not found')

        status = self.status.get(cid, {}).get(BOTID)

        if status == 'left':
            raise Unauthorized('Forbidden: bot is not a member of the chat')

        if status == 'kicked':
            raise Unauthorized('Forbidden: bot was kicked from the chat')

        if cid in self.migrations:
            raise ChatMigrated(self.migrations[cid])

        if not self.consent.get(cid):
            raise Unauthorized('Forbidden: bot was blocked by the user')


    @flogger
    def _check_member_edit_permission(self, cmd, cid, uid):
        chat = self.get_chat(cid)
        status = self.status[cid]
        reasons = (
            'only available in supergroups',
            'creator cannot be kicked out',
            'administrator cannot be kicked out',
            'bot must be an administrator',
        )
        cannot = (
            cmd == 'restrictchatmember' and chat['type'] != 'supergroup',
            status[uid] == 'creator',
            status[uid] == 'administrator',
            status[BOTID] != 'administrator',
        )
        if any(cannot):
            reason = ' and '.join(itertools.compress(reasons, cannot))
            raise BadRequest(f'Cannot: cmd={cmd}, cid={cid}, uid={uid}, reason={reason}')
        return {'ok': True, 'result': True}


    @flogger
    def process_message(self, body):
        """ Processes the messages, prepares the response """
        chat_id = int(body['chat_id'])

        if 'from_chat_id' in body:
            # content = 'forward:CID:MID'
            content = ['forward', body['from_chat_id'], body['message_id']]

        elif 'reply_to_message_id' in body:
            # content = 'reply:MID:text'
            content = ['reply', body['reply_to_message_id'], body['text']]

        elif 'message_id' in body:
            # content = 'edit:MID:text'
            content = ['edit', body['message_id'], body['text']]

        else:
            # content = 'text'
            content = [body['text']]

        message = self.generate_message(BOTID, chat_id, ':'.join(content))

        markup = body.get('reply_markup')
        if markup:
            message['reply_markup'] = json.loads(markup)
        else:
            message.pop('reply_markup', None)

        return message


    @flogger
    def generate_message(self, user, chat, content):
        """ Compose a message, chat is None "private message" is created """
        # pylint: disable=too-many-branches,too-many-locals,too-many-statements

        chat = self.get_chat(chat or user)
        user = self.get_user(user) if user else {}

        set_user = True
        set_author = True

        if content.startswith('edit:'):
            # content = 'edit:MID:text'
            idx = content.index(':', 5)  # second ":"
            mid = int(content[5:idx])
            message = self.get_message(chat['id'], mid)
            message['edit_date'] = int(time.time())
            content = content[idx + 1:]
        else:
            message = {'chat': chat,
                       'date': int(time.time()),
                       'message_id': self.next_message_id(chat['id'])}

        if content in MEDIA_MAP:
            # content = photo|sticker|document|animation
            message.update(MEDIA_MAP[content])

        elif content.startswith('title:'):
            # content = 'title:text'
            message['new_chat_title'] = content[6:]
            set_author = False

        elif content.startswith('members:'):
            # content = 'members:[-]u1,u2'
            status = self.status[chat['id']]
            if content[8] == '-':
                if ',' in content:
                    raise ValueError('removal must be of a single member')

                member = self.get_user(content[9:])
                message['left_chat_member'] = member
                status[member['id']] = ('left'
                                        if member['id'] == user.get('id') else
                                        'kicked')
            else:
                members = [self.get_user(u) for u in content[8:].split(',')]
                message['new_chat_members'] = members
                for member in members:
                    status[member['id']] = 'member'
            set_author = False

        elif content.startswith('migrate:'):
            # content = 'migrate:new_chat_id'
            new_chat = self.get_chat(content[8:])  # for simplicity should already exist

            old_id = chat['id']
            new_id = new_chat['id']

            if chat['type'] != 'group':
                raise ValueError(f'chat {old_id} is not a group')

            if new_chat['type'] != 'supergroup':
                raise ValueError(f'chat {new_id} is not a supergroup')

            self.status[new_id] = self.status[old_id]
            self.consent[new_id] = self.consent[old_id]
            self.migrations[old_id] = new_id

            messages = self.messages.pop(old_id, None)
            if messages:
                self.messages[new_id] = messages
                for msg in messages:
                    msg['chat'] = new_chat

            message['migrate_to_chat_id'] = new_id
            set_author = False

        elif content.startswith('forward:'):
            # content = 'forward:CID:MID'
            _cid, _mid = content.split(':')[1:3]
            _cid = self.get_chat_id(_cid)
            _mid = int(_mid)
            _message = self.get_message(_cid, _mid)
            message['forward_date'] = _message.pop('date')
            message['forward_from'] = _message.pop('from')

            for key in MEDIA_MAP:
                if key in _message:
                    message.update(MEDIA_MAP[key])
                    break
            else:
                message['text'] = _message.pop('text')
                message['entities'] = _message.pop('entities', [])

        else:
            if content.startswith('reply:'):
                # content = 'reply:MID:text'
                idx = content.index(':', 6)  # second ":"
                _mid = int(content[6:idx])
                message['reply_to_message'] = self.get_message(chat['id'], _mid)
                content = content[idx + 1:]

            text, entities = extract_entities_html(content)
            message['text'] = text
            message['entities'] = entities

        if user:
            if is_channel(chat):
                if set_author:
                    message['author_signature'] = get_author_signature(user)
            else:
                if set_user:
                    message['from'] = user

        if not message.get('entities'):
            message.pop('entities', None)

        return message


    @flogger
    def next_unique_id(self):
        """ Returns the next unique id """
        self.unique_id += 1
        return self.unique_id


    @flogger
    def next_message_id(self, chat_id):
        """ Returns the next message_id of the chat_id """
        messages = self.messages.get(chat_id)
        if messages:
            check_message(messages)
            return messages[-1]['message_id'] + 1
        return 1


    @return_deepcopy
    @flogger
    def get_message(self, chat_id, message_id=None, throw_error=True):
        """ Returns the message_id from the chat_id,
            or the last one if the message_id is None
        """
        messages = self.messages.get(chat_id)

        if messages is None:
            if throw_error:
                raise BadRequest(f'chat {chat_id} not found')
            return {}

        check_message(messages)
        try:
            if message_id is None:
                return messages[-1]
            for message in messages:
                if message['message_id'] == message_id:
                    return message
        except IndexError:
            pass

        if throw_error:
            raise BadRequest(f'message {message_id} not found')
        return {}


    @flogger
    def add_message(self, message):
        """ Add or update a message """
        if isinstance(message, dict):
            messages = self.messages.setdefault(message['chat']['id'], [])

            # If updated, the old must be deleted
            for _message in messages:
                if _message['message_id'] == message['message_id']:
                    messages.remove(_message)
                    break

            messages.append(message)
            messages.sort(key=operator.itemgetter('message_id'))
        else:
            raise ValueError('is not message')


    @flogger
    def del_message(self, chat_id, message_id):
        """ Delete a message_id from the chat_id """
        messages = self.messages.get(chat_id, [])
        for message in messages:
            if message['message_id'] == message_id:
                messages.remove(message)
                return
        raise BadRequest(f'message {message_id} not found')


    @flogger
    def set_states(self, table):
        """ Setting the states """
        head, *data = table.strip().split('\n')
        head = head.split()
        data = [d.split() for d in data]
        for row in data:
            user, *chat_status = row
            user_id = self.get_user_id(user)
            for chat, status in zip(head, chat_status):
                chat_id = self.get_chat_id(chat)
                self.status[chat_id][user_id] = STATUS_LETTER.get(status, 'left')


    #@flogger
    def __get_id(self, key, dic, typ):
        """ Returns a user/chat id, key can be id or username """
        try:
            key = int(key)
        except ValueError:
            pass

        if key in dic:
            return key
        if key in self.nicks:
            return self.nicks[key]

        raise BadRequest(f'{typ} not found')


    @flogger
    def get_user_id(self, key):
        """ Returns a user_id, key can be id, username """
        return self.__get_id(key, self.users, 'user')


    @flogger
    def get_chat_id(self, key):
        """ Returns a chat_id, key can be id, username """
        return self.__get_id(key, self.chats, 'chat')


    @flogger
    def get_user(self, key):
        """ Returns a user's data, key can be id, username """
        user_id = self.__get_id(key, self.users, 'user')
        return self.users[user_id]


    @flogger
    def get_chat(self, key):
        """ Returns a chat's data, key can be id, username """
        chat_id = self.__get_id(key, self.chats, 'chat')
        return self.chats[chat_id]


    def get_emitter_username(self, message):
        """ Returns the username of the sender """
        if 'from' in message:
            return message['from']['username']

        uid = self.nicks[message['author_signature']]
        return self.get_user(uid)['username']


    @flogger
    def empty(self):
        return len(self.responses) == 0


    @flogger
    def send(self, data):
        """ Adapts the data to be sent to the server """
        # pylint: disable=too-many-branches,too-many-locals,too-many-statements
        user = data['user']
        chat = data['chat']
        content = data['content']

        if not user and not chat:
            raise ValueError('user and/or chat must be set')

        callback = True
        callback_data = None

        if content.startswith('¢:'):
            content = content[2:]
            callback = False
        elif content.startswith('ð:'):
            callback_data = content[2:]
            content = ''

        # Exception to checking that the user is in the group
        is_members = content.startswith('members:')
        is_add_one = is_members and '-' not in content and ',' not in content

        if not user and not (is_members or content.startswith('migrate:')):
            raise ValueError('only with "members:" and "migrate:" can omit user')

        uid = self.get_user_id(user) if user else None

        if chat and chat != user:
            cid = self.get_chat_id(chat)
            if not is_add_one and uid and self.status[cid][uid] in ('kicked', 'left'):
                raise ValueError(f'"{user}" is not in "{chat}"')
        else:
            cid = uid

        self.consent[cid] = True

        if callback:
            # callback_query are automatic, so you can't send a message without replying
            # to the last inline_keyboard unless you explicitly cancel it with "¢:"
            last_message = self.get_message(cid, throw_error=False)
            callback = bool(last_message.get('reply_markup', {}).get('inline_keyboard'))

        requests = []

        if callback:
            requests.append({'callback_query': {
                'chat_instance': self.next_unique_id(),
                'data': callback_data or get_callback_data(last_message, content),
                'from': self.get_user(user),
                'id': self.next_unique_id(),
                'message': last_message}})
        else:
            message = self.generate_message(user, chat, content)

            if 'caption' in data:
                message['caption'] = data['caption']

            var = 'channel_post' if is_channel(message) else 'message'
            if content.startswith('edit:'):
                var = f'edited_{var}'

            messages = [message]

            if 'migrate_to_chat_id' in message:
                # Two service messages are sent
                new_chat = self.get_chat(message['migrate_to_chat_id'])
                message_2 = copy.deepcopy(message)
                message_2.pop('migrate_to_chat_id', None)
                message_2['chat'] = new_chat
                message_2['migrate_from_chat_id'] = message['chat']['id']
                messages.append(message_2)

            for message in messages:
                self.add_message(message)

                # Out service messages are not issued in supergroups
                if (message.get('left_chat_member') and
                        message['chat']['type'] == 'supergroup'):
                    continue

                requests.append({var: message})

        for request in requests:
            request['update_id'] = self.next_unique_id()
            self.dispatcher.process_update(Update.de_json(request, self.bot))


    @flogger
    def job_tick(self, num=1):
        for _ in range(num):
            self.job_queue.tick()


    @flogger
    def recv(self):
        """ Adapts the data received from the server """
        message = self.responses.popleft()

        response = {
            'user': self.get_emitter_username(message),
            'chat': message['chat']['username'],
            'content': None,
        }

        if 'text' in message:
            text = message.get('text')
            entities = message.get('entities')

            if entities:
                text = introduce_entities_html(text, entities)

            response['content'] = text
        else:
            for media in ('photo', 'sticker', 'animation', 'document'):
                if media in message:
                    response['content'] = media
                    break

        caption = message.get('caption')
        if caption:
            response['caption'] = caption

        markup = message.get('reply_markup')
        if markup and not markup.get('remove_keyboard'):
            response['markup'] = extract_markup(markup)

        return response
