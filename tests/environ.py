# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

# pylint: disable=unused-import,wrong-import-position,wrong-import-order

import sys
assert sys.hexversion >= 0x03060000, 'requires python 3.6 or higher'

import os
import re
import pprint
import logging
import datetime

import unittest
from unittest import mock, skipIf

from tgs import DEVID, TOKEN

UNTESTED_BOT = os.environ.get('UNTESTED_BOT', 'false').lower() == 'true'

ENVIRON = {
    'TELEGRAM_USERNAME': 'bot',
    'TELEGRAM_TOKEN': TOKEN,
    'TELEGRAM_FID': DEVID,
    'TELEGRAM_DID': DEVID,
    'DATABASE_URL': 'sqlite://',
    'HOST': 'https://bot.io',
    'PORT': '443',
    'BIND': '0.0.0.0',
    'I18N_LOCALE': 'locale',
    'I18N_DOMAIN': 'giwel',
    'DEFAULT_LANG': 'xxx',
    'DATETIME_IN_LOG': 'true',
    'WAIT_FOR_ANOTHER': '0.3',
    'JOB_DELAY_TIME': '1',
    'CACHING_ADM_TIME': '0.1',
    'TEMPORARY_RESTRICTION': '0.3',
    'EXPULSION_HISTORY_DAYS': '1',
}
mock.patch.object(os, 'environ', ENVIRON).start()

import telegram.ext
mock.patch.object(telegram.ext, 'run_async', lambda func: func).start()

sys.path.insert(0, '..')
from giwel import config
from giwel import debug
from giwel import context
from giwel import core
from giwel import database
from giwel import texts
from giwel import tools
from giwel import control
from giwel import captcha

CFG = config.CFG

logging.disable()


def mocked_get_captcha_text(*items):
    return ' '.join(map(str, items))


class AttributeDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class EmptyGenericClass:
    # pylint: disable=too-few-public-methods

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class BotIOAdapter:
    """ Adapts test data to TelegramServer system """

    UN = r'[a-zA-Z]\w*'  # "UserName" for user and chats
    HEADER = re.compile(fr'^(?P<user>{UN})?(:(?P<chat>{UN}))?$')

    def __init__(self, test, tgs):
        self.last_chat = None  # the test set is in series (no multi-threading)
        self.bookmark = 0      # useful for finding the origin of one error in dev
        self.history = []      # current execution sequence (from the last bookmark)
        self.test = test       # unittest.TestCase class instance
        self.tgs = tgs         # TelegramServer class instance

    # INTERACTION:
    #
    # Format: header, content[, caption][, markup]
    #  header:  STR   'user:chat'             # user username and/or chat username
    #  content: STR   'photo'                 # sendPhoto
    #                 'sticker'               # sendSticker
    #                 'document'              # sendDocument
    #                 'animation'             # sendAnimation
    #                 'title:text'            # new_chat_title
    #                 'members:[-]UID1,UID2'  # left_chat_member, new_chat_members
    #                 'migrate:CID'           # migrate_to_chat_id, migrate_from_chat_id
    #                 'forward:CID:MID'       # forwardMessage
    #                 'reply:MID:text'        # sendMessage (reply_to_message)
    #                 'edit:MID:text'         # editMessageText
    #                 'text'                  # sendMessage
    #                 # callback is automatic except if the content start with "¢:",
    #                 # to force a data value for callback use "ð:" (in inline_keyboard)
    #  caption: STR   'text'
    #  markup:  LIST  ['options']

    def run(self, interactions):
        """ Execution of integration tests """
        interaction = None
        try:
            for interaction in interactions:

                if callable(interaction):
                    interaction(test=self.test, tgs=self.tgs)
                    self.history.append(f'RUN:{self.get_name_call(interaction)}')

                elif isinstance(interaction, (int, float)):
                    self.check(interaction)

                    # Avoid typing errors in the numerical sequence of tests
                    delta = interaction - self.bookmark
                    if not 0 <= delta < 2:
                        raise ValueError('errors in bookmark')

                    self.bookmark = interaction
                    self.history.clear()

                elif isinstance(interaction, str) and interaction.startswith('tick'):
                    self.tgs.job_tick(int(interaction[4:] or 1))
                    self.history.append(f'RUN:{interaction}')

                else:
                    header, content, *optionals = interaction
                    header = self.HEADER.match(header).groupdict()

                    if not isinstance(content, re.Pattern):
                        content = str(content or '')

                    user = header['user']
                    chat = header['chat']
                    data = {'user': user, 'chat': chat, 'content': content}
                    self.set_optionals(data, optionals)

                    if user == 'bot':
                        # Bot Response
                        response = self.tgs.recv()
                        self.adjust(data, response)
                        self.test.assertEqual(data, response)
                    else:
                        # User request
                        self.tgs.send(data)
                        self.last_chat = chat or user

                    self.history.append(interaction)

                while self.tgs.next_sends:
                    self.tgs.send(self.tgs.next_sends.popleft())

            self.check()

        except:
            print(f'\nbookmark {self.bookmark}')
            pprint.pprint(self.history)
            print('\n', len(self.history))
            pprint.pprint(interaction)
            for index in range(1, len(self.tgs.responses) + 1):
                print(f'\ntgs.recv {index}')
                pprint.pprint(self.tgs.recv())
            print('\n')
            raise


    def check(self, interaction=1):
        # pylint: disable=protected-access
        pending_jobs = self.tgs.job_queue._queue.queue
        section = int(interaction) == interaction
        if section and pending_jobs:
            threads = '\n » '.join(data[1].callback.__name__
                                   for data in sorted(pending_jobs))
            raise ValueError(f'are still pending job:\n\n » {threads}')

        if not self.tgs.empty():
            raise ValueError('messages remain unprocessed')


    @staticmethod
    def set_optionals(data, optionals):
        for opt in optionals:
            if isinstance(opt, str):
                data['caption'] = opt
            elif isinstance(opt, list):
                data['markup'] = opt


    def adjust(self, data, response):

        if not data['chat']:
            data['chat'] = self.last_chat

        for key, value in data.items():
            if isinstance(value, re.Pattern):
                if value.match(response[key]):
                    data[key] = ''
                    response[key] = ''


    @staticmethod
    def get_name_call(obj):
        error = f'object {obj} has no name'
        while hasattr(obj, 'func'):
            obj = obj.func
        if hasattr(obj, '__name__'):
            return obj.__name__
        raise ValueError(error)


REAL_DATETIME_CLASS = datetime.datetime

def mocked_datetime_now(datetime_module, target):
    """Override ``datetime.datetime.now()`` with a custom target value.
    This creates a new datetime.datetime class and alters its now/utcnow methods.

    Returns:
        A mock.patch context, can be used as a decorator or in a with.

    https://gist.github.com/rbarrois/5430921
    """

    # http://bugs.python.org/msg68532
    # http://docs.python.org/reference/datamodel.html
    # → customizing-instance-and-subclass-checks
    class DatetimeSubclassMeta(type):
        """We need to customize the __instancecheck__ method for isinstance().
        This must be performed at a metaclass level.
        """
        @classmethod
        def __instancecheck__(cls, obj):
            return isinstance(obj, REAL_DATETIME_CLASS)

    class BaseMockedDatetime(REAL_DATETIME_CLASS):
        @classmethod
        def now(cls, tz=None):
            return target.replace(tzinfo=tz)

        @classmethod
        def utcnow(cls):
            return target

    # Python2 & Python3-compatible metaclass
    MockedDatetime = DatetimeSubclassMeta('datetime', (BaseMockedDatetime,), {})

    return mock.patch.object(datetime_module, 'datetime', MockedDatetime)
