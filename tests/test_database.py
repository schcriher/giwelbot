# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import time
import zlib
import datetime

from sqlalchemy.exc import IntegrityError

from environ import unittest, mock, mocked_datetime_now, CFG, ENVIRON, database

from tgs import FakeJobQueue


TD_HOUR = datetime.timedelta(hours=1)
TD_DAY = datetime.timedelta(days=1)
NOW = datetime.datetime.now()

TN3D = NOW - 3 * TD_DAY
TN2D = NOW - 2 * TD_DAY
TN1D = NOW - 1 * TD_DAY
#
TN3H = NOW - 3 * TD_HOUR
TN2H = NOW - 2 * TD_HOUR
TN1H = NOW - 1 * TD_HOUR
# NOW
TP1M = NOW + 1 * TD_HOUR
TP2M = NOW + 2 * TD_HOUR
TP3M = NOW + 3 * TD_HOUR
#
TP1D = NOW + 1 * TD_DAY
TP2D = NOW + 2 * TD_DAY
TP3D = NOW + 3 * TD_DAY


CHATS = (
    {'id': 0, 'language': 'es', 'interacts': 0},
    {'id': 1, 'language': 'en', 'interacts': 1},
    {'id': 2, 'language': None, 'interacts': 2},
    {'id': 3, 'language': 'xx', 'interacts': 0},
)
for _row in CHATS:
    _row['title'] = f'T{_row["id"]}'

SANCTIONS = (
    {'id': 0, 'chat_id': 0, 'user_id': 0, 'strikes': 0},
    {'id': 1, 'chat_id': 0, 'user_id': 1, 'strikes': 1},
    {'id': 2, 'chat_id': 1, 'user_id': 2, 'strikes': 2},
    {'id': 3, 'chat_id': 2, 'user_id': 4, 'strikes': 3},
    {'id': 4, 'chat_id': 2, 'user_id': 5, 'strikes': 0},
    {'id': 5, 'chat_id': 3, 'user_id': 5, 'strikes': 0},
)

ADMISSIONS = (
    {'id': 0, 'chat_id': 0, 'user_id': 0, 'join_message_date': TN3D},
    {'id': 1, 'chat_id': 0, 'user_id': 1, 'join_message_date': TN2D},
    {'id': 2, 'chat_id': 1, 'user_id': 2, 'join_message_date': TN1D},
    {'id': 3, 'chat_id': 2, 'user_id': 4, 'join_message_date': TN3H},
    {'id': 4, 'chat_id': 2, 'user_id': 5, 'join_message_date': TN2H},
    {'id': 5, 'chat_id': 3, 'user_id': 5, 'join_message_date': TN1H},
    {'id': 6, 'chat_id': 4, 'user_id': 5, 'join_message_date': NOW},
)
for _row in ADMISSIONS:
    _row['join_message_id'] = _row['id']
    _row['dispose_in_thread'] = 'greeting'

# CaptchaStatus: 0:WAITING, 1:SOLVED, 2:WRONG
# CaptchaLocation: 0:GROUP, 1:PRIVATE
CAPTCHAS = (
    {'id': 0, 'location_id': 0, 'status_id': 1, 'admission_id': 0},
    {'id': 1, 'location_id': 0, 'status_id': 2, 'admission_id': 1},
    {'id': 2, 'location_id': 0, 'status_id': 2, 'admission_id': 3},
    {'id': 3, 'location_id': 1, 'status_id': 0, 'admission_id': 3},
    {'id': 4, 'location_id': 0, 'status_id': 0, 'admission_id': 4},
    {'id': 5, 'location_id': 0, 'status_id': 1, 'admission_id': 5},
)
for _row in CAPTCHAS:
    _row['message_id'] = _row['id']
    _row['correct_token'] = f'CT{_row["id"]}'
    _row['another_token'] = f'AT{_row["id"]}'

RESTRICTIONS = (
    {'id': 0, 'chat_id': 0, 'user_id': 0, 'until': TN3D},
    {'id': 1, 'chat_id': 0, 'user_id': 4, 'until': TP1M},
    {'id': 2, 'chat_id': 0, 'user_id': 5, 'until': TP2M},
    {'id': 3, 'chat_id': 1, 'user_id': 6, 'until': TP3M},
    {'id': 4, 'chat_id': 3, 'user_id': 6, 'until': TP1D},
)

EXPULSIONS = (
    {'id': 0, 'chat_id': 0, 'user_id': 0, 'until': TN3D},
    {'id': 1, 'chat_id': 0, 'user_id': 0, 'until': TN2D},
    {'id': 2, 'chat_id': 0, 'user_id': 0, 'until': TN1D},
    {'id': 3, 'chat_id': 0, 'user_id': 3, 'until': TP1D},
    {'id': 4, 'chat_id': 1, 'user_id': 4, 'until': TP2D},
    {'id': 5, 'chat_id': 2, 'user_id': 5, 'until': TP3D},
    {'id': 6, 'chat_id': 2, 'user_id': 1, 'until': TP3D},
)
for _row in EXPULSIONS:
    _row['reason'] = f'R{_row["id"]}'


def fake_callback(*_):
    """Fake callback function for test"""


class TestDatabase(unittest.TestCase):


    def setUp(self):
        self.dbs = database.get_session_maker(create_all=True, drop_all=True)()
        glo = globals()

        for var in 'CHATS SANCTIONS ADMISSIONS CAPTCHAS RESTRICTIONS EXPULSIONS'.split():
            for data in glo[var]:
                self.dbs.add(getattr(database, var.capitalize()[:-1])(**data))

        self.dbs.commit()


    # ---


    def check_obj_dic(self, obj, dic):
        self.assertEqual(bool(obj), bool(dic))
        for key, val in (dic or {}).items():

            if key in ('created_at', 'updated_at'):
                continue

            self.assertEqual(getattr(obj, key), val, key)


    def test_get_chat(self):
        # get_chat:
        #   Add one to interacts
        #   Update updated_at
        #   Work with get_or_create

        chat_id = 0
        chat_dic = CHATS[chat_id]

        # Get
        chat_obj = database.get_chat(self.dbs, id=chat_id)
        chat_obj.interacts -= 1  # get_chat add one
        self.check_obj_dic(chat_obj, chat_dic)

        # Mod
        old_title = chat_dic['title']
        new_title = f'new_title_{old_title}'

        chat_dic['title'] = new_title
        chat_obj = database.get_chat(self.dbs, id=chat_id, title=new_title)
        chat_obj.interacts -= 1  # get_chat add one
        self.check_obj_dic(chat_obj, chat_dic)

        # New
        chat_dic = {'id': 9, 'title': None, 'greet_give': True, 'language': None,
                    'interacts': 0, 'created_at': NOW, 'updated_at': NOW,
                    'prev_greet_users': None, 'prev_greet_message_id': None}  # default
        chat_obj = database.get_chat(self.dbs, id=9)
        chat_obj.interacts -= 1  # get_chat add one
        self.check_obj_dic(chat_obj, chat_dic)


    def test_get_chat_lang(self):
        for chat_id, chat_dic in enumerate(CHATS):
            with self.subTest(chat_id=chat_id):
                lang = database.get_chat_lang(self.dbs, chat_id)
                self.assertEqual(lang, chat_dic['language'])


    # ---


    def _check_ids(self, obj, expected_ids, one=False):
        if obj is None:
            obtained_ids = []
        elif hasattr(obj, '__iter__') or '_sa_instance_state' in vars(obj):
            if one:
                obtained_ids = [obj.id]
            else:
                obtained_ids = sorted(o.id for o in obj)
        else:
            obtained_ids = sorted(o.id for o in self.dbs.query(obj))
        self.assertEqual(obtained_ids, expected_ids)


    def _check_gets(self, var, chat_id, user_id, expected_ids, **params):
        obj = getattr(database, f'get_{var}s')(self.dbs, chat_id, user_id, **params)
        one = chat_id is not None and user_id is not None
        self._check_ids(obj, expected_ids, one)


    def test_get_sanctions(self):
        # get_sanctions:
        #   Work with get_query
        data = (
            (0, 0, [0]),
            (0, None, [0, 1]),
            (None, 2, [2]),
            (None, 5, [4, 5]),
            (None, None, [0, 1, 2, 3, 4, 5]),
            #
            (9, None, []),
            (None, 9, []),
            (0, 2, [6]),    # creates a new record (id=6)
            (9, 0, [7]),    # creates a new record (id=7)
            (0, 9, [8]),    # creates a new record (id=8)
            (9, 9, [9]),    # creates a new record (id=9)
            #
            (None, None, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]),
        )
        for chat_id, user_id, expected_ids in data:
            with self.subTest(chat_id=chat_id, user_id=user_id):
                self._check_gets('sanction', chat_id, user_id, expected_ids)


    def test_get_admissions(self):
        # get_admissions:
        #   Work with get_query
        data = (
            (0, 0, [0]),
            (0, None, [0, 1]),
            (None, 2, [2]),
            (None, 5, [4, 5, 6]),
            (None, None, [0, 1, 2, 3, 4, 5, 6]),
            #
            (0, 2, []),
            (9, 0, []),
            (9, None, []),
            (0, 9, []),
            (None, 9, []),
            (9, 9, []),
        )
        for chat_id, user_id, expected_ids in data:
            with self.subTest(chat_id=chat_id, user_id=user_id):
                self._check_gets('admission', chat_id, user_id, expected_ids)

        data = (
            (2, 4, [3]),
            (2, None, [3]),
            (None, 4, [3]),
            #
            (0, 2, []),
            (9, 0, []),
            (9, None, []),
            (0, 9, []),
            (None, 9, []),
            (9, 9, []),
        )
        for chat_id, user_id, expected_ids in data:
            with self.subTest(chat_id=chat_id, user_id=user_id, case='with_pcap=True'):
                self._check_gets('admission', chat_id, user_id, expected_ids,
                                 with_pcap=True)


    def test_get_restrictions(self):
        # get_restrictions:
        #   Work with get_query
        data = (
            (0, 0, [0]),
            (0, None, [0, 1, 2]),
            (None, 4, [1]),
            (None, 6, [3, 4]),
            (None, None, [0, 1, 2, 3, 4]),
            #
            (0, 6, []),
            (9, 0, []),
            (9, None, []),
            (0, 9, []),
            (None, 9, []),
            (9, 9, []),
        )
        for chat_id, user_id, expected_ids in data:
            with self.subTest(chat_id=chat_id, user_id=user_id):
                self._check_gets('restriction', chat_id, user_id, expected_ids)


    def test_get_expulsions(self):
        # get_expulsions:
        #   Work with get_query
        data = (
            (0, 0, [2]),
            (0, 3, [3]),
            (0, None, [2, 3]),
            (None, 4, [4]),
            (None, None, [2, 3, 4, 5, 6]),
            #
            (0, 4, []),
            (9, 0, []),
            (9, None, []),
            (0, 9, []),
            (None, 9, []),
            (9, 9, []),
        )
        for chat_id, user_id, expected_ids in data:
            with self.subTest(chat_id=chat_id, user_id=user_id):
                self._check_gets('expulsion', chat_id, user_id, expected_ids)


    def test_unique_constraint(self):
        data = (
            (database.Sanction, SANCTIONS[0]),
            (database.Admission, ADMISSIONS[0]),
            (database.Restriction, RESTRICTIONS[0]),
        )
        for model, dic in data:
            dic['id'] = 999
            with self.subTest(model=model):
                with self.assertRaises(IntegrityError):
                    self.dbs.add(model(**dic))
                    self.dbs.commit()
                self.dbs.rollback()

        # Expulsions are stored, therefore repeated chat_id<>user_id are allowed
        expulsion = EXPULSIONS[0]
        expulsion['id'] = 999
        self.dbs.add(database.Expulsion(**expulsion))
        self.dbs.commit()


    def test_state(self):
        obj = (1, 2, 3)
        state = database.PyObj('name', obj)

        self.assertEqual(state.name, 'name')
        self.assertIsInstance(state.data, bytes)
        self.assertIsInstance(state.sign, bytes)

        self.assertTrue(state.is_valid)
        self.assertEqual(state.obj, obj)

        state.sign = state.sign[1:]

        self.assertFalse(state.is_valid)
        self.assertEqual(state.obj, obj)

        state.data = state.data[1:]
        with self.assertRaises(zlib.error):
            _ = state.obj


    # ---


    @mock.patch('giwel.database.logger')
    def test_migrate_chat(self, logger):
        old_id = 1
        new_id = 9

        def get(cid):
            return self.dbs.query(database.Chat).filter_by(id=cid).first()

        def exists(cid):
            return bool(get(cid))

        def check(cid, msg=None):
            data = (
                # for chat_id 1
                ('admissions', [2]),
                ('restrictions', [3]),
                ('expulsions', [4]),
            )
            chat = get(cid)
            for col, ids in data:
                self.assertEqual(sorted(c.id for c in getattr(chat, col)), ids, msg)

        self.assertTrue(exists(old_id), 'internal error, must exist')
        self.assertFalse(exists(new_id), 'internal error, must not exist')
        check(old_id, 'internal error')

        # They produce calls to the logging methods
        database.event.remove(database.Engine, 'before_cursor_execute',
                              database.before_cursor_execute)
        database.event.remove(database.Engine, 'after_cursor_execute',
                              database.after_cursor_execute)

        for num_debug, num_warning in ((1, 0), (0, 1)):
            logger.reset_mock()

            database.migrate_chat(self.dbs, old_id, new_id)

            self.assertFalse(exists(old_id))
            self.assertTrue(exists(new_id))
            check(new_id)

            self.assertEqual(logger.debug.call_count, num_debug)
            self.assertEqual(logger.warning.call_count, num_warning)


    @mocked_datetime_now(datetime, NOW)
    def test_clean_database(self):
        # pylint: disable=protected-access

        environ = {
            'GREETING_TIMER': '3600',       # 1h
            'EXPULSION_HISTORY_DAYS': '1',  # 1d
        }
        sentinel = object()
        for var, new_val in environ.items():
            old_val = ENVIRON.get(var, sentinel)
            ENVIRON[var] = new_val
            environ[var] = old_val
        CFG.__dict__.clear()

        expected_messages_first = [# (chat_id, join_message_id)
            # old admissions
            (0, 0),
            (2, 4),
            # old captchas
            (0, 0),
            (0, 1),
            (2, 2),
            (2, 4),
        ]
        for expected_messages in (expected_messages_first, [], []):
            messages = database.clean_database(self.dbs)
            self.assertEqual(sorted(messages), sorted(expected_messages))
            data = (
                (database.Sanction, [1, 2, 3]),
                (database.Admission, [5, 6]),
                (database.Captcha, [5]),
                (database.Restriction, [1, 2, 3, 4]),
                (database.Expulsion, [2, 3, 4, 5, 6]),
                (database.Chat, [c['id'] for c in CHATS]),  # never deleted
            )
            for model, expected_ids in data:
                with self.subTest(model=model, expected_ids=expected_ids):
                    self._check_ids(model, expected_ids)

        for var, old_val in environ.items():
            if old_val is sentinel:
                ENVIRON.pop(var, None)
            else:
                ENVIRON[var] = old_val
        CFG.__dict__.clear()


    def test_clear_jobs(self):
        names = ('job', 'job', 'dat', 'job', 'row')
        #           1      2      3      4      5

        self._check_ids(database.PyObj, [])

        for name in names:
            self.dbs.add(database.PyObj(name, name))
        self.dbs.commit()

        self._check_ids(database.PyObj, [1, 2, 3, 4, 5])

        database.clear_jobs(self.dbs)
        self.dbs.commit()

        self._check_ids(database.PyObj, [3, 5])


    def test_save_and_load_state(self):
        # pylint: disable=protected-access
        data = (
            (20, False, False),
            (30, False, True),
            (10, True, False),
            (40, True, True),
        )
        for n_tick in range(len(data) + 1):
            with self.subTest(n_tick=n_tick):

                job_queue1 = FakeJobQueue(None)

                for next_t, enabled, removed in data:
                    job_1 = job_queue1.run_once(fake_callback, next_t)
                    job_1.enabled = enabled
                    if removed:
                        job_1.schedule_removal()

                for _ in range(n_tick):
                    job_queue1.tick()  # 'n_tick' jobs are executed

                database.save_jobs(self.dbs, job_queue1)

                job_queue2 = FakeJobQueue(None)

                njobs = database.load_jobs(self.dbs, job_queue2)

                self.assertEqual(njobs, len(data) - n_tick)
                self.assertEqual(len(job_queue1._queue.queue), njobs)
                self.assertEqual(len(job_queue2._queue.queue), njobs)

                for next_t, enabled, removed in sorted(data)[n_tick:]:

                    next_t_1, job_1 = job_queue1._queue.get(False)
                    next_t_2, job_2 = job_queue2._queue.get(False)

                    self.assertLess(next_t_1 - next_t, time.time())
                    self.assertLess(next_t_2 - next_t, time.time())

                    self.assertEqual(enabled, job_1.enabled)
                    self.assertEqual(enabled, job_2.enabled)

                    self.assertEqual(removed, job_1.removed)
                    self.assertEqual(removed, job_2.removed)


    def test_load_jobs_delay(self):
        # pylint: disable=protected-access,too-many-locals
        CFG.JOB_DELAY_TIME = 0.050

        # timeline    0     20     40     60     80     ms
        #             +------+------+------+------+------+--
        # jobs               a             b          c
        # sleep       ░░░░░░░░░░░                           t=30
        # re-job                 ░░░░░░░░░░░░░░░░░░         Δ=50
        #   ├── a:               [-10]────────────a         shift=60 (runs first)
        #   ├── b:                         [30]───b         shift=20
        #   └── c:                                    [60]  shift=0
        #                         \___________/
        #                               ↓
        #                          -10 and 30 are less than the delay (50)
        #
        # Note: A and B are displaced at the same time, but A will be executed first
        # because it is processed first, leaving a next_t less than B (for thousandths
        # of a second or less)

        job_queue1 = FakeJobQueue(None)
        job_queue2 = FakeJobQueue(None)

        job_queue1.run_once(fake_callback, 0.020)  # job a
        job_queue1.run_once(fake_callback, 0.060)  # job b
        job_queue1.run_once(fake_callback, 0.090)  # job c

        _t1 = time.time()
        database.save_jobs(self.dbs, job_queue1)
        _t2 = time.time()

        time.sleep(0.030)

        njobs = database.load_jobs(self.dbs, job_queue2)
        offset = time.time() - (_t2 + _t1) / 2 - 0.030

        (next_t_a1, _), (next_t_b1, _), (next_t_c1, _) = job_queue1._queue.queue
        (next_t_a2, _), (next_t_b2, _), (next_t_c2, _) = job_queue2._queue.queue

        time_shift_a = next_t_a2 - next_t_a1 - offset
        time_shift_b = next_t_b2 - next_t_b1 - offset

        # 'time_shift_c' without offset because the start is not shifted
        time_shift_c = next_t_c2 - next_t_c1

        self.assertEqual(njobs, 3)

        self.assertGreater(next_t_a2, next_t_a1)
        self.assertGreater(next_t_b2, next_t_b1)
        self.assertGreater(next_t_c2, next_t_c1)

        self.assertGreater(next_t_b1, next_t_a1)
        self.assertGreater(next_t_b2, next_t_a2)

        self.assertGreater(next_t_c1, next_t_b1)
        self.assertGreater(next_t_c2, next_t_b2)

        self.assertLess(abs(time_shift_a - 0.060), 0.006)  # \  inside of 10% of
        self.assertLess(abs(time_shift_b - 0.020), 0.002)  # /  the expected value
        self.assertLess(abs(time_shift_c), 0.001)

        CFG.__dict__.clear()


    def test_save_and_load_state_with_error(self):
        # pylint: disable=protected-access

        job_queue1 = FakeJobQueue(None)
        job_queue1.run_once(fake_callback, 1)  # state_id = 1
        job_queue1.run_once(fake_callback, 2)  # state_id = 2
        job_queue1.run_once(fake_callback, 3)  # state_id = 3

        self._check_ids(database.PyObj, [])
        database.save_jobs(self.dbs, job_queue1)
        self._check_ids(database.PyObj, [1, 2, 3])

        # Wrong sign
        state = self.dbs.query(database.PyObj).filter_by(id=1).first()
        state.sign = state.sign[1:]

        # Wrong data
        state = self.dbs.query(database.PyObj).filter_by(id=2).first()
        state.data = state.data[1:]

        self.dbs.commit()

        job_queue2 = FakeJobQueue(None)

        database.load_jobs(self.dbs, job_queue2)
        self._check_ids(database.PyObj, [])

        self.assertEqual(len(job_queue1._queue.queue), 3)
        self.assertEqual(len(job_queue2._queue.queue), 1)


    # ---


    def test___repr__(self):
        models = 'Sanction Admission Captcha Restriction Expulsion Chat'.split()
        for model in models:
            for obj_id in range(3):
                with self.subTest(model=model, obj_id=obj_id):
                    typ = getattr(database, model)
                    obj = self.dbs.query(typ).filter_by(id=obj_id).first()
                    tex = repr(obj)
                    self.assertIsInstance(tex, str)
                    self.assertIn(model, tex)
                    self.assertIn(str(obj.id), tex)
                    self.assertGreater(len(tex), len(f'{model}{obj.id}'))


    def test_admission_delete_orphan(self):
        self._check_ids(database.Captcha, [0, 1, 2, 3, 4, 5])

        adm0 = self.dbs.query(database.Admission).filter_by(id=0).first()
        adm3 = self.dbs.query(database.Admission).filter_by(id=3).first()

        self.dbs.delete(adm0)  # with cap0
        self.dbs.delete(adm3)  # with cap2, cap3

        self.dbs.commit()

        self._check_ids(database.Captcha, [1, 4, 5])


    def test_admission_and_captcha_attributes(self):
        adm0 = self.dbs.query(database.Admission).filter_by(id=0).first()
        cap0 = self.dbs.query(database.Captcha).filter_by(id=0).first()
        self.assertEqual(adm0.group_captcha, cap0)
        self.assertIsNone(adm0.private_captcha)

        adm3 = self.dbs.query(database.Admission).filter_by(id=3).first()
        cap2 = self.dbs.query(database.Captcha).filter_by(id=2).first()
        cap3 = self.dbs.query(database.Captcha).filter_by(id=3).first()
        self.assertEqual(adm3.group_captcha, cap2)
        self.assertEqual(adm3.private_captcha, cap3)
        self.assertEqual(adm3.captchas[database.CaptchaLocation.GROUP], cap2)
        self.assertEqual(adm3.captchas[database.CaptchaLocation.PRIVATE], cap3)

        self.assertEqual(cap0.location, database.CaptchaLocation.GROUP)
        self.assertEqual(cap2.location, database.CaptchaLocation.GROUP)
        self.assertEqual(cap3.location, database.CaptchaLocation.PRIVATE)

        self.assertEqual(cap0.status, database.CaptchaStatus.SOLVED)
        self.assertEqual(cap2.status, database.CaptchaStatus.WRONG)
        self.assertEqual(cap3.status, database.CaptchaStatus.WAITING)

        for cap in (cap0, cap2, cap3):
            self.assertTrue(cap.is_correct(f'CT{cap.id}'))
            self.assertFalse(cap.is_correct(f'--{cap.id}'))
            self.assertTrue(cap.is_another(f'AT{cap.id}'))
            self.assertFalse(cap.is_another(f'--{cap.id}'))

        cap0.location = database.CaptchaLocation.PRIVATE
        self.assertEqual(cap0.location, database.CaptchaLocation.PRIVATE)

        cap0.status = database.CaptchaStatus.WAITING
        self.assertEqual(cap0.status, database.CaptchaStatus.WAITING)

        adm = database.Admission(id=9,
                                 chat_id=9,
                                 user_id=9,
                                 join_message_date=NOW,
                                 join_message_id=9)
        self.assertIsNone(adm.group_captcha)
        self.assertIsNone(adm.private_captcha)
