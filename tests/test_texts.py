# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import gettext
import datetime

from environ import unittest, mock, ENVIRON, texts


TIMES = {
    'tA_TIMER': 0.5,
    'tB_RESTRICTION': datetime.timedelta(seconds=120),
}

TEXTS = {
    'A': 'Text A',
    'B': ('Text B {}', 'Texts B {}'),
    'C': 'Text C {tA_TIMER}',
    'D': 'Text D {tB_RESTRICTION}',
    'E': 'Text E {tB_RESTRICTION}',
    'F': 'Text F {OTHER_VAR}',
    'G': 'Text G {A:s}',
    'H': 'Text G {B:s}',  # nest plurals expressions → TypeError
    #
    'LIST_SEP': ', ',
    'LIST_END': '{} AND {}',
    'DAYS': ('{:g} DAY', '{:g} DAYS'),
    'HOURS': ('{:g} HOUR', '{:g} HOURS'),
    'MINUTES': ('{:g} MINUTE', '{:g} MINUTES'),
    'SECONDS': ('{:g} SECOND', '{:g} SECONDS'),
}


class TestTexts(unittest.TestCase):


    @mock.patch.object(texts, 'TIMES', TIMES)
    @mock.patch.object(texts, 'TEXTS', TEXTS)
    @mock.patch('gettext.translation')
    def test_translator(self, translation):

        self.assertFalse(translation.called)
        lang = 'NULL'
        trans = texts.Translator(lang)
        self.assertTrue(translation.called)
        params = {
            'domain': ENVIRON['I18N_DOMAIN'],
            'localedir': ENVIRON['I18N_LOCALE'],
            'languages': [lang],
        }
        self.assertEqual(translation.call_args_list, [mock.call(**params)])

        trans.trans = gettext.NullTranslations()

        # Test __getattr__
        self.assertEqual(trans.A, 'Text A')
        self.assertEqual(trans.B(0), 'Texts B 0')
        self.assertEqual(trans.B(1), 'Text B 1')
        self.assertEqual(trans.B(2), 'Texts B 2')
        self.assertEqual(trans.C, 'Text C 0.5 SECONDS')
        self.assertEqual(trans.D, 'Text D 2 MINUTES')
        self.assertEqual(trans.E, 'Text E 2 MINUTES')
        self.assertEqual(trans.F, 'Text F {OTHER_VAR}')
        self.assertEqual(trans.G, 'Text G Text A')

        # Test __getitem__
        self.assertEqual(trans['A'], 'Text A')
        self.assertEqual(trans['B'](0), 'Texts B 0')
        self.assertEqual(trans['B'](1), 'Text B 1')
        self.assertEqual(trans['B'](2), 'Texts B 2')
        self.assertEqual(trans['C'], 'Text C 0.5 SECONDS')
        self.assertEqual(trans['D'], 'Text D 2 MINUTES')
        self.assertEqual(trans['E'], 'Text E 2 MINUTES')
        self.assertEqual(trans['F'], 'Text F {OTHER_VAR}')
        self.assertEqual(trans['G'], 'Text G Text A')

        with self.assertRaises(TypeError):
            _ = trans.H  # nest plurals expressions

        data = (
            (0.1, '0.1 SECONDS'),
            (0, '0 SECONDS'),
            (1, '1 SECOND'),
            (2, '2 SECONDS'),
            (60, '1 MINUTE'),
            (120, '2 MINUTES'),
            (121, '2 MINUTES AND 1 SECOND'),
            (3600, '1 HOUR'),
            (3610, '1 HOUR AND 10 SECONDS'),
            (3699, '1 HOUR, 1 MINUTE AND 39 SECONDS'),
            (3999, '1 HOUR, 6 MINUTES AND 39 SECONDS'),
            (9999, '2 HOURS, 46 MINUTES AND 39 SECONDS'),
            (99999, '1 DAY, 3 HOURS, 46 MINUTES AND 39 SECONDS'),
            (999000, '11 DAYS, 13 HOURS AND 30 MINUTES'),
        )
        for seconds, expected in data:
            with self.subTest(method='sec_to_text', seconds=seconds, expected=expected):
                self.assertEqual(trans.sec_to_text(seconds), expected)


    def test_inexistent_field_unreplacement_formatter(self):
        fmt = texts.InexistentFieldUnreplacementFormatter({'d': 'DD', 'e': '6'}.get)
        data = (
            ('{} {} {}', [], '{0} {1} {2}'),
            ('{:d} {!r} {:>2s}', [], '{0:d} {1!r} {2:>2s}'),
            ('{a} {b} {c}', [], '{a} {b} {c}'),
            ('{a:s} {b!r} {c:>2s}', [], '{a:s} {b!r} {c:>2s}'),
            ('{:s} {b!r} {:>2s}', [], '{0:s} {b!r} {1:>2s}'),
            #
            ('{} {} {}', [10], '10 {1} {2}'),
            ('{:d} {!r} {:>2s}', [10, 11], '10 11 {2:>2s}'),
            ('{a} {d} {c}', [10], '{a} DD {c}'),
            ('{a:s} {b!r} {e:>3s}', [], '{a:s} {b!r}   6'),
            ('{:s} {d!r} {:>3s}', [], "{0:s} 'DD' {1:>3s}"),
            #
            ('{2} {1} {0}', [], '{2} {1} {0}'),
            ('{2} {1} {0}', [10], '{2} {1} 10'),
            ('{0:0>{e}d}', [10], '000010'),
        )
        for format_string, args, expected in data:
            with self.subTest(format_string=format_string, expected=expected):
                self.assertEqual(fmt.vformat(format_string, args, {}), expected)

        self.assertEqual(str(fmt), '«Formatter»')
        self.assertEqual(repr(fmt), '«Formatter»')


    def test_english_translation(self):
        # Translation read from the mo file
        t_real = texts.Translator('en')

        # Without translation, dictionary texts are used
        t_null = texts.Translator('en')
        t_null.trans = gettext.NullTranslations()

        for var in texts.TEXTS:
            with self.subTest(var=var):
                val_real = getattr(t_real, var)
                val_null = getattr(t_null, var)

                if callable(val_real):
                    for num in range(4):
                        self.assertEqual(val_real(num), val_null(num))
                else:
                    self.assertEqual(val_real, val_null)


    def test_real_translation(self):
        self.assertEqual(texts.Translator('en').LANG, 'english')
        self.assertEqual(texts.Translator('es').LANG, 'español')
