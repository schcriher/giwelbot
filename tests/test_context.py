# -*- coding: UTF-8 -*-
# Copyright (C) 2020 Schmidt Cristian Hernán

import time
import pathlib      # pylint: disable=unused-import
import collections

from telegram import Bot, Chat, User, ChatMember, Update
from telegram.ext import Job
from telegram.error import TelegramError, ChatMigrated

from environ import unittest, mock, CFG, context, AttributeDict, EmptyGenericClass


class TestContext(unittest.TestCase):


    def test_adm_cache(self):
        CFG.CACHING_ADM_TIME = 0.005

        run = False

        @context.adm_cache
        def func(_, chat_id):
            nonlocal run
            run = True
            return chat_id

        self.assertFalse(run)
        self.assertEqual(func(None, 1), 1)
        self.assertTrue(run)

        run = False
        self.assertFalse(run)
        self.assertEqual(func(None, 1), 1)
        self.assertFalse(run)
        time.sleep(0.005)
        self.assertEqual(func(None, 1), 1)
        self.assertTrue(run)

        run = False
        func.clear()
        self.assertEqual(func(None, 1), 1)
        self.assertTrue(run)

        run = False
        self.assertEqual(func(None, 2), 2)
        self.assertTrue(run)

        CFG.__dict__.clear()


    def test_get_admins(self):
        chat_id = 1

        bot = mock.Mock(spec=Bot)
        usr = mock.Mock(spec=User)
        adm = mock.Mock(spec=ChatMember)

        adm.user = usr
        bot.get_chat_administrators.return_value = [adm]

        self.assertEqual(context.get_admins(bot, chat_id), [usr])


    def test_all_members_are_administrators(self):
        chat_id = 1

        bot = mock.Mock(spec=Bot)
        chat = mock.Mock(spec=Chat)

        chat.all_members_are_administrators = True
        bot.get_chat.return_value = chat

        self.assertEqual(context.all_members_are_administrators(bot, chat_id), True)


    @mock.patch('pathlib.Path')
    def test_get_available_langs(self, class_path):
        path = class_path.return_value
        path.glob.return_value = [
            EmptyGenericClass(parts=[None, 'ab']),
            EmptyGenericClass(parts=[None, 'ab_CD']),
            EmptyGenericClass(parts=[None, 'abc']),
            EmptyGenericClass(parts=[None, 'abc_DF']),
            #
            EmptyGenericClass(parts=[None, 'ab.UTF-8']),
            EmptyGenericClass(parts=[None, 'ab_CD@piglatin']),
            EmptyGenericClass(parts=[None, 'abc:UTF-8']),
            #
            EmptyGenericClass(parts=[None, 'a_B']),
            EmptyGenericClass(parts=[None, 'a_BC']),
            EmptyGenericClass(parts=[None, 'abcd_EF']),
            EmptyGenericClass(parts=[None, 'abcd_EFGH']),
        ]
        expected = ['ab', 'ab_CD', 'abc', 'abc_DF', 'ab', 'ab_CD', 'abc']
        self.assertEqual(context.get_available_langs(), expected)



class TestContextContext(unittest.TestCase):
    # pylint: disable=no-member


    def setUp(self):
        self.vars = 'bot update job dbs txt cfg cache error test'.split()

        for var in self.vars:
            setattr(self, var, mock.Mock())

        self.job0 = mock.Mock()
        self.job1 = mock.Mock()
        self.job.context = (self.job0, self.job1)

        self.funcs = {
            'send': self.bot.send_message,
            'reply': self.bot.send_message,
            'edit': self.bot.edit_message_text,
            'answer': self.bot.answer_callback_query,
        }


    def test___init__(self):
        data = (
            ('update', False, ('send', 'reply', 'edit', 'answer')),
            ('update', True, ('send', 'reply', 'edit', 'answer')),
            ('job', False, ('send', 'edit')),
        )
        for extra, private, funcs in data:
            with self.subTest(extra=extra, private=private, funcs=funcs):
                params = {
                    'bot': self.bot,
                    'dbs': self.dbs,
                    'txt': self.txt,
                    'cfg': self.cfg,
                    'cache': self.cache,
                    'error': self.error,
                    'test': self.test,
                }
                params[extra] = getattr(self, extra)

                if private:
                    chat = self.update.effective_chat
                    chat.type = chat.PRIVATE
                else:
                    self.update.reset_mock()

                ctx = context.Context(params)

                self.assertIsInstance(repr(ctx), str)

                if 'update' in params:
                    self.assertEqual(ctx.tgc, ctx.update.effective_chat)
                    self.assertEqual(ctx.tgu, ctx.update.effective_user)
                    self.assertEqual(ctx.tgm, ctx.update.effective_message)
                elif 'job' in params:
                    self.assertEqual(ctx.tgc, ctx.job.context[0])
                    self.assertEqual(ctx.tgu, ctx.job.context[1])

                for name in params:
                    self.assertTrue(hasattr(ctx, name))

                for name in funcs:
                    self.assertTrue(hasattr(ctx, name))
                    # Two iterations of __wrapped__ for @flogger
                    self.assertEqual(getattr(ctx, name).__wrapped__.__wrapped__,
                                     self.funcs[name])


    @mock.patch('giwel.context.migrate_chat')
    def test__define(self, migrate_chat):
        # pylint: disable=protected-access
        ctx = context.Context({'bot': self.bot, 'update': self.update})

        obj = object()
        er1 = ChatMigrated(new_chat_id=1)
        er2 = TelegramError('error')

        call = (
            [mock.call(chat_id=2, a=3)],
            [mock.call(chat_id=2, a=3), mock.call(chat_id=1, a=3)],
            [mock.call(None, 2, 1)],
        )
        data = (
            ([obj], obj, call[0]),
            ([er1, obj], obj, call[1]),
            ([er1, er1], None, call[1]),
            ([er1, er2], None, call[1]),
            ([er2], None, call[0]),
        )
        for fun_ret, expected, fcalls in data:
            for throw_exc in (False, True):
                with self.subTest(fun_ret=fun_ret, expected=expected):

                    migrate_chat.reset_mock()

                    func = mock.Mock()
                    func.__name__ = '__name__'
                    func.side_effect = fun_ret
                    dfunc = ctx._define(func, chat_id=2, throw_exc=throw_exc)

                    if throw_exc and expected is None:
                        with self.assertRaises(fun_ret[-1].__class__):
                            dfunc(a=3)

                        if any(isinstance(r, ChatMigrated) for r in fun_ret):
                            self.assertEqual(migrate_chat.call_args_list, call[2])
                        else:
                            self.assertEqual(migrate_chat.call_args_list, [])
                    else:
                        self.assertEqual(dfunc(a=3), expected)
                        self.assertEqual(func.call_args_list, fcalls)


    @mock.patch('giwel.context.get_chat')
    def test___getattr__(self, get_chat):
        self.bot.id = 1

        chat = self.update.effective_chat
        chat.type = chat.GROUP
        chat.id = 2

        user = self.update.effective_user
        user.id = 3

        message = self.update.effective_message
        message.message_id = 4
        message.text = '/cmd text a'
        message.date = 1000

        chat_mock = get_chat.return_value

        ctx = context.Context({'bot': self.bot, 'update': self.update})

        gets = {}
        for name in 'sanction admission restriction expulsion'.split():
            func = mock.Mock()
            gets[name] = func.return_value
            setattr(ctx, f'get_{name}s', func)

        data = (
            ('cid', 2),
            ('uid', 3),
            ('mid', 4),
            ('text', '/cmd text a'),
            ('date', 1000),
            ('arg', 'text a'),
            ('from_bot', False),
            ('is_group', True),
            ('is_private', False),
            ('is_forward', True),
            ('chat', chat_mock),
            ('sanction', gets['sanction']),
            ('admission', gets['admission']),
            ('restriction', gets['restriction']),
            ('expulsion', gets['expulsion']),
        )
        for var, expected in data:
            with self.subTest(var=var, expected=expected):
                if var not in ('cid', 'mid'):  # used in: send, reply, edit, answer
                    self.assertNotIn(var, ctx.__dict__)
                self.assertEqual(getattr(ctx, var), expected)
                self.assertIn(var, ctx.__dict__)

        user.id = 1
        chat.type = chat.PRIVATE
        message.text = 'test text b'
        message.forward_from = None
        message.forward_from_chat = None
        message.forward_from_message_id = None
        message.forward_signature = None
        message.forward_sender_name = None
        message.forward_date = None

        ctx = context.Context({'bot': self.bot, 'update': self.update})

        data = (
            ('uid', 1),
            ('text', 'test text b'),
            ('arg', ''),
            ('from_bot', True),
            ('is_group', False),
            ('is_private', True),
            ('is_forward', False),
            ('chat', None),
        )
        for var, expected in data:
            with self.subTest(var=var, expected=expected):
                self.assertEqual(getattr(ctx, var), expected)

        with self.subTest(case='missing attribute'):
            with self.assertRaises(AttributeError):
                _ = ctx.missing_attribute


    @mock.patch('giwel.context.get_admins')
    @mock.patch('giwel.context.all_members_are_administrators')
    def test_is_admin(self, all_members_are_administrators, get_admins):
        ctx = context.Context({'bot': self.bot, 'update': self.update})

        usr = mock.Mock(spec=User)
        usr.id = 1

        data = (
            (4, 1, False, [[usr]], True),
            (4, 2, False, [[usr]], False),
            (4, 3, True, [[usr]], True),
            (4, 1, True, [TelegramError('error')], False),
            (None, 1, False, [[usr]], True),
            (None, 2, False, [[usr]], False),
            (None, 3, True, [[usr]], True),
            (None, 1, True, [TelegramError('error')], False),
        )
        for chat_id, user_id, all_adm, get_adm, expected in data:
            with self.subTest(chat_id=chat_id, user_id=user_id, all_adm=all_adm):

                self.update.effective_chat.all_members_are_administrators = all_adm
                all_members_are_administrators.return_value = all_adm
                get_admins.side_effect = get_adm

                if chat_id is None:
                    self.update.effective_chat.id = 5

                self.assertEqual(ctx.is_admin(chat_id, user_id), expected)


    @mock.patch('giwel.context.get_admins')
    def test_get_admins(self, get_admins):
        chat_id = 1

        ctx = context.Context({'bot': self.bot, 'update': self.update})

        usr1 = mock.Mock(spec=User)
        usr2 = mock.Mock(spec=User)

        usr1.is_bot = False
        usr2.is_bot = True

        get_admins.side_effect = [[usr1, usr2]]

        self.assertEqual(ctx.get_admins(chat_id, with_bots=False), [usr1])
        self.assertEqual(get_admins.call_args_list, [mock.call(self.bot, chat_id)])

        get_admins.reset_mock()
        get_admins.side_effect = [[usr1, usr2]]

        self.assertEqual(ctx.get_admins(chat_id, with_bots=True), [usr1, usr2])
        self.assertEqual(get_admins.call_args_list, [mock.call(self.bot, chat_id)])

        get_admins.reset_mock()
        get_admins.side_effect = [TelegramError('error')]

        self.assertEqual(ctx.get_admins(chat_id), [])
        self.assertEqual(get_admins.call_args_list, [mock.call(self.bot, chat_id)])


    @mock.patch('giwel.context.get_chat')
    def test_get_tgc_and_chat(self, get_chat):
        chat_id = 1

        tgc_mock = self.bot.get_chat.return_value
        tgc_mock.id = chat_id
        tgc_mock.title = 'title'

        ctx = context.Context({'bot': self.bot, 'update': self.update, 'dbs': self.dbs})

        tgc, chat = ctx.get_tgc_and_chat(chat_id)

        self.assertEqual(tgc, self.bot.get_chat.return_value)
        self.assertEqual(chat, get_chat.return_value)

        self.assertEqual(self.bot.get_chat.call_args_list,
                         [mock.call(chat_id=chat_id)])

        self.assertEqual(get_chat.call_args_list,
                         [mock.call(self.dbs, id=chat_id, title='title')])


    @mock.patch('time.time')
    def test_wait_another(self, _time):
        user_id = 1
        chat_id = 2
        name = 'context'
        wait = 10

        CFG.WAIT_FOR_ANOTHER = wait
        self.update.effective_user.id = user_id
        cache = {}

        ctx = context.Context({'bot': self.bot, 'update': self.update, 'cache': cache})

        now = 100
        _time.return_value = now
        self.assertFalse(ctx.wait_another(chat_id, name))
        self.assertEqual(cache[user_id][chat_id]['another_at'][name], now)

        self.assertTrue(ctx.wait_another(chat_id, name))
        self.assertEqual(cache[user_id][chat_id]['another_at'][name], now)

        now += wait + 1
        _time.return_value = now
        self.assertFalse(ctx.wait_another(chat_id, name))
        self.assertEqual(cache[user_id][chat_id]['another_at'][name], now)

        CFG.__dict__.clear()


    @mock.patch('giwel.context.get_sanctions')
    def test_get_sanctions(self, get_sanctions):
        ctx = context.Context({'bot': self.bot, 'dbs': self.dbs})
        self.assertEqual(get_sanctions.call_args_list, [])
        ctx.get_sanctions(1, 2)
        self.assertEqual(get_sanctions.call_args_list, [mock.call(self.dbs, 1, 2)])


    @mock.patch('giwel.context.get_admissions')
    def test_get_admissions(self, get_admissions):
        ctx = context.Context({'bot': self.bot, 'dbs': self.dbs})
        self.assertEqual(get_admissions.call_args_list, [])
        ctx.get_admissions(1, 2, 3)
        self.assertEqual(get_admissions.call_args_list, [mock.call(self.dbs, 1, 2, 3)])


    @mock.patch('giwel.context.get_restrictions')
    def test_get_restrictions(self, get_restrictions):
        ctx = context.Context({'bot': self.bot, 'dbs': self.dbs})
        self.assertEqual(get_restrictions.call_args_list, [])
        ctx.get_restrictions(1, 2)
        self.assertEqual(get_restrictions.call_args_list, [mock.call(self.dbs, 1, 2)])


    @mock.patch('giwel.context.get_expulsions')
    def test_get_expulsions(self, get_expulsions):
        ctx = context.Context({'bot': self.bot, 'dbs': self.dbs})
        self.assertEqual(get_expulsions.call_args_list, [])
        ctx.get_expulsions(1, 2)
        self.assertEqual(get_expulsions.call_args_list, [mock.call(self.dbs, 1, 2)])



class TestContextConfigMenu(unittest.TestCase):


    def setUp(self):
        # pylint: disable=protected-access
        CFG.__dict__.clear()
        self.cfg = context.ConfigMenu()
        self.config_menu_list = list(self.cfg.data)
        self.num_total = len(self.config_menu_list)
        extra_texts = ['CURRENT', 'YES', 'NO', 'DISABLED']
        self.txt = AttributeDict({i:i for i in self.config_menu_list + extra_texts})


    def tearDown(self):
        # pylint: disable=protected-access
        CFG.__dict__.clear()


    def test_menu_integrity(self):
        self.assertEqual(self.cfg.init, self.config_menu_list[0])
        self.assertIsInstance(repr(self.cfg), str)


    def test_get_prev(self):
        key = self.config_menu_list[-1]  # last
        lst = [key]
        num = 1
        while True:
            key = self.cfg.get_prev(key)
            if key:
                lst.append(key)
                num += 1
            else:
                break
        self.assertEqual(num, self.num_total)
        self.assertEqual(lst[::-1], self.config_menu_list)


    def test_get_next(self):
        key = self.config_menu_list[0]  # first
        lst = [key]
        num = 1
        while True:
            key = self.cfg.get_next(key, value=True)
            if key:
                lst.append(key)
                num += 1
            else:
                break
        self.assertEqual(num, self.num_total)
        self.assertEqual(lst, self.config_menu_list)


    def test_get_menu(self):
        self.txt.CURRENT = '{subject}·{value}'
        data = (
            ('LANGUAGE', 'es', 'es', [
                ['es§config:LANGUAGE:es',
                 'en§config:LANGUAGE:en'],
                ['🆗§config:stop:',
                 '➡️§config:LANGUAGE:']]),

            ('LANGUAGE', 'en', 'en', [
                ['es§config:LANGUAGE:es',
                 'en§config:LANGUAGE:en'],
                ['🆗§config:stop:',
                 '➡️§config:LANGUAGE:']]),


            ('MAX_STRIKES', 0, 'DISABLED', [
                ['1§config:MAX_STRIKES:1',
                 '2§config:MAX_STRIKES:2',
                 '3§config:MAX_STRIKES:3',
                 '4§config:MAX_STRIKES:4',
                 '5§config:MAX_STRIKES:5'],
                ['DISABLED§config:MAX_STRIKES:0'],
                ['⬅️§config:prev:LANGUAGE',
                 '🆗§config:stop:',
                 '➡️§config:MAX_STRIKES:']]),

            ('MAX_STRIKES', 3, '3', [
                ['1§config:MAX_STRIKES:1',
                 '2§config:MAX_STRIKES:2',
                 '3§config:MAX_STRIKES:3',
                 '4§config:MAX_STRIKES:4',
                 '5§config:MAX_STRIKES:5'],
                ['DISABLED§config:MAX_STRIKES:0'],
                ['⬅️§config:prev:LANGUAGE',
                 '🆗§config:stop:',
                 '➡️§config:MAX_STRIKES:']]),


            ('CALL_ADMINS', False, 'NO', [
                ['YES§config:CALL_ADMINS:True',
                 'NO§config:CALL_ADMINS:False'],
                ['⬅️§config:prev:MAX_STRIKES',
                 '🆗§config:stop:',
                 '➡️§config:CALL_ADMINS:']]),

            ('CALL_ADMINS', True, 'YES', [
                ['YES§config:CALL_ADMINS:True',
                 'NO§config:CALL_ADMINS:False'],
                ['⬅️§config:prev:MAX_STRIKES',
                 '🆗§config:stop:',
                 '➡️§config:CALL_ADMINS:']]),


            ('GREET_GIVE', False, 'NO', [
                ['YES§config:GREET_GIVE:True',
                 'NO§config:GREET_GIVE:False'],
                ['⬅️§config:prev:CALL_ADMINS',
                 '🆗§config:stop:']]),

            ('GREET_GIVE', True, 'YES', [
                ['YES§config:GREET_GIVE:True',
                 'NO§config:GREET_GIVE:False'],
                ['⬅️§config:prev:CALL_ADMINS',
                 '🆗§config:stop:',
                 '➡️§config:GREET_GIVE:']]),


            ('GREET_TIMER', False, 'NO', [
                ['YES§config:GREET_TIMER:True',
                 'NO§config:GREET_TIMER:False'],
                ['⬅️§config:prev:GREET_GIVE',
                 '🆗§config:stop:',
                 '➡️§config:GREET_TIMER:']]),

            ('GREET_TIMER', True, 'YES', [
                ['YES§config:GREET_TIMER:True',
                 'NO§config:GREET_TIMER:False'],
                ['⬅️§config:prev:GREET_GIVE',
                 '🆗§config:stop:',
                 '➡️§config:GREET_TIMER:']]),


            ('GREET_ADMIN_SHOW', False, 'NO', [
                ['YES§config:GREET_ADMIN_SHOW:True',
                 'NO§config:GREET_ADMIN_SHOW:False'],
                ['⬅️§config:prev:GREET_TIMER',
                 '🆗§config:stop:']]),

            ('GREET_ADMIN_SHOW', True, 'YES', [
                ['YES§config:GREET_ADMIN_SHOW:True',
                 'NO§config:GREET_ADMIN_SHOW:False'],
                ['⬅️§config:prev:GREET_TIMER',
                 '🆗§config:stop:',
                 '➡️§config:GREET_ADMIN_SHOW:']]),


            ('GREET_ADMIN_CITE', True, 'YES', [
                ['YES§config:GREET_ADMIN_CITE:True',
                 'NO§config:GREET_ADMIN_CITE:False'],
                ['⬅️§config:prev:GREET_ADMIN_SHOW',
                 '🆗§config:stop:',
                 '➡️§config:GREET_ADMIN_CITE:']]),

            ('GREET_ADMIN_CITE', False, 'NO', [
                ['YES§config:GREET_ADMIN_CITE:True',
                 'NO§config:GREET_ADMIN_CITE:False'],
                ['⬅️§config:prev:GREET_ADMIN_SHOW',
                 '🆗§config:stop:',
                 '➡️§config:GREET_ADMIN_CITE:']]),


            ('GREET_ADMIN_ORDER', 'A-Z', 'A-Z', [
                ['A-Z§config:GREET_ADMIN_ORDER:A-Z',
                 'Z-A§config:GREET_ADMIN_ORDER:Z-A',
                 'Random§config:GREET_ADMIN_ORDER:Random'],
                ['⬅️§config:prev:GREET_ADMIN_CITE',
                 '🆗§config:stop:']]),

            ('GREET_ADMIN_ORDER', 'Z-A', 'Z-A', [
                ['A-Z§config:GREET_ADMIN_ORDER:A-Z',
                 'Z-A§config:GREET_ADMIN_ORDER:Z-A',
                 'Random§config:GREET_ADMIN_ORDER:Random'],
                ['⬅️§config:prev:GREET_ADMIN_CITE',
                 '🆗§config:stop:']]),
        )
        for item, value, value_str, expected_ikb in data:
            with self.subTest(item=item, value=value, value_str=value_str):
                text, markup = self.cfg.get_menu(item, value, self.txt)
                inline_keyboard = markup.to_dict()['inline_keyboard']

                ikb = [[f'{cel["text"]}§{cel["callback_data"]}' for cel in row]
                       for row in inline_keyboard]

                self.assertEqual(text, f'{item}·{value_str}')
                self.assertEqual(ikb, expected_ikb)


    def test_mapper(self):
        data = (
            ('LANGUAGE', 'en', 'en'),
            ('MAX_STRIKES', '3', 3),
            ('CALL_ADMINS', 'yes', True),
        )
        for item, value, expected in data:
            with self.subTest(item=item, value=value, expected=expected):
                self.assertEqual(self.cfg.mapper(item, value), expected)
        data = (
            ('LANGUAGE', 'xx'),
            ('MAX_STRIKES', '9'),
            ('CALL_ADMINS', 'test'),
        )
        for item, value in data:
            with self.subTest(item=item, value=value):
                with self.assertRaises(ValueError):
                    self.cfg.mapper(item, value)


    def test_to_text(self):
        data = (
            (True, 'YES'),
            (False, 'NO'),
            (0, 'DISABLED'),
            (None, '—'),
            (1, '1'),
            (2, '2'),
            (2., '2.0'),
            (2.0, '2.0'),
            (2.00, '2.0'),
            ('abc', 'abc'),
            ([1, 2], '[1, 2]'),
        )
        for value, expected in data:
            with self.subTest(value=value, expected=expected):
                self.assertEqual(self.cfg.to_text(value, self.txt), expected)



class TestContextContextualizer(unittest.TestCase):


    def setUp(self):
        self.context = context.Contextualizer()

        _context = mock.patch('giwel.context.Context')
        self._context = _context.start()
        self.addCleanup(_context.stop)

        _translator = mock.patch('giwel.context.Translator')
        self._translator = _translator.start()
        self.addCleanup(_translator.stop)

        self.bot = mock.Mock(spec=Bot)
        self.update = mock.Mock(spec=Update)
        self.job = mock.Mock(spec=Job)
        self.job.context = (mock.Mock(), mock.Mock())


    def test_basic(self):
        # pylint: disable=protected-access
        self.assertIsInstance(self.context._locks, collections.defaultdict)
        self.assertIsInstance(self.context.config, context.ConfigMenu)
        self.assertEqual(self.context.dbsmk, None)
        self.assertEqual(self.context.trans, {})
        self.assertEqual(self.context.cache, {})
        self.assertIsInstance(repr(self.context), str)


    @mock.patch('giwel.context.get_chat_lang')
    def test___call__(self, get_chat_lang):
        # pylint: disable=too-many-function-args
        lang = 'en'

        get_chat_lang.return_value = lang
        trans = self._translator.return_value
        ctx = self._context.return_value

        error = mock.Mock()
        dbsmk = mock.Mock()
        dbs = dbsmk.return_value
        self.context.dbsmk = dbsmk
        self.update.effective_user.language_code = lang

        ctx.raise_value_error = False

        @self.context
        def func(ctx):
            if ctx.raise_value_error:
                raise ValueError('error')
            return ctx

        params = {
            'bot': self.bot,
            'dbs': dbsmk.return_value,
            'txt': trans,
            'cfg': self.context.config,
            'cache': {},
        }
        data = (
            ({'update': self.update}, (self.update,)),
            ({'job': self.job}, (self.job,)),
            ({'error': error}, (None, error)),
        )
        for param, args in data:
            with self.subTest(case=next(iter(param))):

                self._context.reset_mock()
                dbs.reset_mock()
                params['update'] = None
                params['job'] = None
                params['error'] = None

                params.update(param)

                self.assertEqual(func(self.bot, *args), ctx)
                self.assertEqual(self._context.call_args_list, [mock.call(params)])
                self.assertEqual(dbs.rollback.call_args_list, [])
                self.assertEqual(dbs.commit.call_args_list, [mock.call()])
                self.assertEqual(dbs.close.call_args_list, [mock.call()])

        dbs.reset_mock()
        self._context.reset_mock()
        ctx.raise_value_error = True

        with self.subTest(case='error'):
            with self.assertRaises(ValueError):
                func(self.bot, self.job)

            self.assertEqual(dbs.rollback.call_args_list, [mock.call()])
            self.assertEqual(dbs.commit.call_args_list, [])
            self.assertEqual(dbs.close.call_args_list, [mock.call()])


    @mock.patch('giwel.context.get_session_maker')
    def test_initialize(self, _get_session_maker):
        trans = self._translator.return_value
        dbsmk = _get_session_maker.return_value

        CFG.DEFAULT_LANG = 'xx'
        params = {'create_all': 1, 'url': 2}
        calls = [mock.call(**params)]

        self.assertEqual(self.context.trans, {})
        self.assertEqual(self.context.dbsmk, None)

        self.context.initialize(**params)

        self.assertEqual(self.context.trans, {'xx': trans, 'default': trans})
        self.assertEqual(self.context.dbsmk, dbsmk)
        self.assertEqual(_get_session_maker.call_args_list, calls)

        self.context.trans.clear()
        _get_session_maker.reset_mock()
        self._translator.reset_mock()
        self._translator.side_effect = [FileNotFoundError('error'), trans]

        self.context.initialize(create_all=1, url=2)

        self.assertEqual(self.context.trans, {'en': trans, 'default': trans})
        self.assertEqual(self.context.dbsmk, dbsmk)
        self.assertEqual(_get_session_maker.call_args_list, calls)

        CFG.__dict__.clear()


    def test_get_translator(self):
        trans1 = self._translator.return_value
        trans2 = mock.Mock()

        self.assertEqual(self.context.trans, {})

        trans = self.context.get_translator('x')

        self.assertEqual(trans, trans1)
        self.assertEqual(self.context.trans, {'x': trans})

        self.context.trans = {'default': trans2}
        self._translator.side_effect = [FileNotFoundError('error')]

        trans = self.context.get_translator('y')

        self.assertEqual(trans, trans2)
        self.assertEqual(self.context.trans, {'default': trans2, 'y': trans2})
