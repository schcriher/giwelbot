# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

# pylint: disable=too-many-lines,too-many-public-methods

import re
import copy
import json
import time
import unittest

from unittest import mock

from telegram.error import TelegramError, BadRequest, Unauthorized, ChatMigrated

from tgs import (TOKEN, BOTID, DEVID, ADMID, USRID, G1ID, S1ID, C1ID, G2ID, S2ID,
                 C2ID, G3ID, S3ID, C3ID, is_channel, adjust_offsets,
                 extract_entities_html, introduce_entities_html, extract_markup,
                 get_callback_data, return_deepcopy, dict_to_bytes, remove_keyboard,
                 make_dict, get_int, get_author_signature, check_message,
                 STATUS, USERS, CHATS, NICKS, CONSENT, PHOTO, MEDIA_MAP,
                 DispatcherHandlersDict, FakeJobQueue, TelegramServer)

BOT_G1 = {'from': USERS[BOTID], 'chat': CHATS[G1ID]}
BOT_DEV = {'from': USERS[BOTID], 'chat': CHATS[DEVID]}
DEV_DEV = {'from': USERS[DEVID], 'chat': CHATS[DEVID]}
DEV_G1 = {'from': USERS[DEVID], 'chat': CHATS[G1ID]}
DEV_S1 = {'from': USERS[DEVID], 'chat': CHATS[S1ID]}
DEV_G2 = {'from': USERS[DEVID], 'chat': CHATS[G2ID]}
DEV_S2 = {'from': USERS[DEVID], 'chat': CHATS[S2ID]}
DEV_C1 = {'author_signature': get_author_signature(USERS[DEVID]), 'chat': CHATS[C1ID]}
USR_G1 = {'from': USERS[USRID], 'chat': CHATS[G1ID]}
USR_S1 = {'from': USERS[USRID], 'chat': CHATS[S1ID]}

MARKUP_KB_TEXT = ['1', '2', '3', '4']
MARKUP_KB_DICT = {'keyboard': [['1', '2'], ['3', '4']],
                  'resize_keyboard': True,
                  'one_time_keyboard': True,
                  'selective': False}

MARKUP_IKB_TEXT = ['A1', 'A2', 'A3']
MARKUP_IKB_DATA = ['01', '02', '03']
MARKUP_IKB_DICT = {'inline_keyboard': [[{'text': 'A1', 'callback_data': '01'},
                                        {'text': 'A2', 'callback_data': '02'}],
                                       [{'text': 'A3', 'callback_data': '03'}]]}

ENTITIES_DATA = (
    ('@text', '@text', [{'type': 'mention', 'offset': 0, 'length': 5}]),
    ('#text', '#text', [{'type': 'hashtag', 'offset': 0, 'length': 5}]),
    ('/text', '/text', [{'type': 'bot_command', 'offset': 0, 'length': 5}]),
    ('<b>text</b>', 'text', [{'type': 'bold', 'offset': 0, 'length': 4}]),
    ('<strong>text</strong>', 'text', [{'type': 'bold', 'offset': 0, 'length': 4}]),
    ('<i>text</i>', 'text', [{'type': 'italic', 'offset': 0, 'length': 4}]),
    ('<em>text</em>', 'text', [{'type': 'italic', 'offset': 0, 'length': 4}]),
    ('<code>text</code>', 'text', [{'type': 'code', 'offset': 0, 'length': 4}]),
    ('<pre>text</pre>', 'text', [{'type': 'pre', 'offset': 0, 'length': 4}]),
    ('@aaa#bbb', '@aaa#bbb', [{'type': 'mention', 'offset': 0, 'length': 4},
                              {'type': 'hashtag', 'offset': 4, 'length': 4}]),
    ('/a @b <b>c</b> #d', '/a @b c #d',
     [{'type': 'bot_command', 'offset': 0, 'length': 2},
      {'type': 'mention', 'offset': 3, 'length': 2},
      {'type': 'bold', 'offset': 6, 'length': 1},
      {'type': 'hashtag', 'offset': 8, 'length': 2}]),
    ('<code>/a</code> @b', '/a @b',
     [{'type': 'code', 'offset': 0, 'length': 2},
      {'type': 'mention', 'offset': 3, 'length': 2}]),
    ('<i>a</i> <i>b</i> <code>c</code> <i>d</i>', 'a b c d',
     [{'type': 'italic', 'offset': 0, 'length': 1},
      {'type': 'italic', 'offset': 2, 'length': 1},
      {'type': 'code', 'offset': 4, 'length': 1},
      {'type': 'italic', 'offset': 6, 'length': 1}]),
)


def not_doing_anything(_):
    pass


class FakeDispatcher:
    # pylint: disable=too-few-public-methods
    updates = []
    def process_update(self, update):
        self.updates.append(update)


class TestTgsFunctions(unittest.TestCase):

    maxDiff = None  # pylint: disable=invalid-name


    def test_is_channel(self):
        data = (
            # message
            ({'chat': {'type': 'channel'}}, True),
            ({'chat': {'type': 'group'}}, False),
            ({'chat': {'type': 'supergroup'}}, False),
            # chat
            ({'type': 'channel'}, True),
            ({'type': 'group'}, False),
            ({'type': 'supergroup'}, False),
        )
        for dic, expected, in data:
            with self.subTest(dic=dic, expected=expected):
                self.assertEqual(is_channel(dic), expected)
        data = (
            ({'chat': {'id': 'channel'}}, KeyError),
            ({'chat': {'id': 'group'}}, KeyError),
            ({'chat': {'id': 'channel'}}, KeyError),
            ({'id': 'channel'}, ValueError),
            ({'id': 'group'}, ValueError),
            ({'id': 'channel'}, ValueError),
            ({}, ValueError),
            (123, TypeError),
            (object(), TypeError),
        )
        for obj, error in data:
            with self.subTest(obj=obj):
                with self.assertRaises(error):
                    is_channel(obj)


    def test_adjust_offsets(self):
        entities = [
            {'type': 'typ', 'offset': 0, 'length': 3},
            {'type': 'typ', 'offset': 8, 'length': 3},
        ]
        expected_entities = [
            {'type': 'typ', 'offset': 0, 'length': 3},
            {'type': 'typ', 'offset': 9, 'length': 3},
        ]
        adjust_offsets(entities, 5, 1)
        self.assertEqual(entities, expected_entities)


    def test_extract_entities_html(self):
        for text, expected_text, expected_entities in ENTITIES_DATA:
            with self.subTest(text=text):
                obtained_text, obtained_entities = extract_entities_html(text)
                self.assertEqual(obtained_text, expected_text)
                self.assertEqual(obtained_entities, expected_entities)


    def test_introduce_entities_html(self):
        fix_bold = re.compile(r'<(/)?strong>')
        fix_italic = re.compile(r'<(/)?em>')
        for expected_text, text, entities in ENTITIES_DATA:
            expected_text = fix_bold.sub(r'<\1b>', expected_text)
            expected_text = fix_italic.sub(r'<\1i>', expected_text)
            with self.subTest(expected_text=expected_text):
                obtained_text = introduce_entities_html(text, entities)
                self.assertEqual(obtained_text, expected_text)


    def test_extract_markup(self):
        self.assertEqual(extract_markup(MARKUP_KB_DICT), MARKUP_KB_TEXT)
        self.assertEqual(extract_markup(MARKUP_IKB_DICT), MARKUP_IKB_TEXT)

        with self.assertRaises(ValueError):
            extract_markup({'a': 1})


    def test_get_callback_data(self):
        message = {'reply_markup': MARKUP_IKB_DICT}
        for text, data in zip(MARKUP_IKB_TEXT, MARKUP_IKB_DATA):
            with self.subTest(text=text):
                self.assertEqual(get_callback_data(message, text), data)

        with self.assertRaises(KeyError):
            get_callback_data(message, 'a')

        with self.assertRaises(KeyError):
            message = {'a': 1}
            get_callback_data(message, 'a')

        with self.assertRaises(KeyError):
            message = {'reply_markup': {'inline_keyboard': 1}}
            get_callback_data({'a': 1}, 'a')


    def test_return_deepcopy(self):
        dic0 = {'list': [1, 2], 'dict': {'key': 'val'}}

        @return_deepcopy
        def func():
            return dic0

        dic1 = func()
        self.assertIsNot(dic1, dic0)
        self.assertIsNot(dic1['list'], dic0['list'])
        self.assertIsNot(dic1['dict'], dic0['dict'])


    def test_dict_to_bytes(self):
        dic = {'a': 1, 'b': [2, 3]}
        out = b'{"a": 1, "b": [2, 3]}'

        @dict_to_bytes
        def func():
            return dic

        self.assertEqual(func(), out)


    def test_remove_keyboard(self):
        data = (
            (123,
             123),

            (['reply_markup', 'keyboard'],
             ['reply_markup', 'keyboard']),

            ({'a': 1, 'reply_markup': {'b': 2}},
             {'a': 1, 'reply_markup': {'b': 2}}),

            ({'a': 1, 'reply_markup': {'keyboard': 2}},
             {'a': 1}),

            ({'a': 1, 'reply_markup': {'inline_keyboard': 2}},
             {'a': 1, 'reply_markup': {'inline_keyboard': 2}}),

            ({'a': 1, 'b': {'reply_markup': {'keyboard': 2}}},
             {'a': 1, 'b': {}}),

            ({'a': 1, 'b': {'reply_markup': {'inline_keyboard': 2}}},
             {'a': 1, 'b': {'reply_markup': {'inline_keyboard': 2}}}),
        )

        # Decorator
        for index, (in_obj, out_obj) in enumerate(data):
            with self.subTest(index=index):

                @remove_keyboard
                def func(obj):
                    return copy.deepcopy(obj)

                self.assertEqual(func(in_obj), out_obj)

        # Removal function
        for index, (in_obj, out_obj) in enumerate(data):
            with self.subTest(index=index):
                self.assertEqual(remove_keyboard(in_obj), out_obj)


    def test_make_dict(self):
        head = ('a', 'b', 'id')
        row = ('4', 3.0, 99999)
        expected = {'a': '4', 'b': 3.0, 'id': 99999}
        self.assertEqual(make_dict(head, row), expected)

        head = ('a', 'b', 'id')
        row = (True, None, 1)
        expected = {'a': True, 'b': None, 'id': 1}
        self.assertEqual(make_dict(head, row), expected)


    def test_get_int(self):
        data = (
            ('a', {'a': 1}, 1),
            ('a', {'a': '1'}, 1),
            ('b', {'a': 0}, None),
            ('c', None, None),
        )
        for var, dic, expected in data:
            with self.subTest(var=var):
                self.assertEqual(get_int(dic, var), expected)


    def test_get_author_signature(self):
        data = (
            ({'first_name': 'aaa', 'last_name': 'bbb', 'id': 1}, 'aaa bbb'),
            ({'first_name': 'aaa', 'last_name': None, 'id': 2}, 'aaa'),
            ({'first_name': None, 'last_name': 'bbb', 'id': 3}, 'bbb'),
            ({'first_name': 'aaa b', 'last_name': 'cC', 'id': 4}, 'aaa b cC'),
            ({'first_name': None, 'last_name': None, 'id': 5}, ''),
        )
        for dic, expected in data:
            with self.subTest(dic=dic):
                self.assertEqual(get_author_signature(dic), expected)

        data = (
            {'last_name': 'bbb', 'id': 6},
            {'first_name': 'aaa', 'id': 7},
            {'id': 8},
        )
        for dic in data:
            with self.subTest(dic=dic):
                with self.assertRaises(KeyError):
                    get_author_signature(dic)


    def test_check_message(self):
        msg_1 = {'message_id': 1}
        msg_2 = {'message_id': 2}
        msg_4 = {'message_id': 4}

        with self.subTest(dic='sorted messages'):
            self.assertIsNone(check_message([msg_1, msg_2, msg_4]))

        with self.subTest(dic='unsorted messages'):
            with self.assertRaises(IndexError):
                check_message([msg_1, msg_4, msg_2])

        with self.subTest(dic='duplicate messages'):
            with self.assertRaises(IndexError):
                check_message([msg_1, msg_2, msg_2])


    def test_global_variables(self):
        # pylint: disable=too-many-locals
        self.assertRegex(TOKEN, r'\d+:[\w+-]+')

        status = {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left',
                  1011: 'member', 1022: 'member', 1033: 'member',
                  2011: 'left', 2022: 'left', 2033: 'left',
                  3011: 'left', 3022: 'left', 3033: 'left',
                  4011: 'left', 4022: 'left', 4033: 'left'}

        self.assertEqual(STATUS, {
            -10011: status,
            -10012: status,
            -10013: status,
            -10021: status,
            -10022: status,
            -10023: status,
            -10031: status,
            -10032: status,
            -10033: status,
        })

        for dic in (USERS, CHATS):
            for did, data in dic.items():
                self.assertEqual(did, data['id'])

        users = {}
        chats = {}
        nicks = {}
        consent = {}

        for num1, name1a in enumerate(('bot', 'dev', 'adm', 'usr'), 1):
            name1b = name1a.capitalize()
            uid1 = int(str(num1) * 5)
            bot = num1 == 1

            users[uid1] = {'id': uid1, 'is_bot': bot, 'username': name1a,
                           'first_name': name1b, 'last_name': name1b}

            if not bot:
                chats[uid1] = {'id': uid1, 'type': 'private', 'username': name1a,
                               'first_name': name1b, 'last_name': name1b}

            nicks[name1a] = uid1
            nicks[f'{name1b} {name1b}'] = uid1
            consent[uid1] = False

            for num2 in range(1, 4):
                name2a = f'{name1a}{num2}'
                name2b = name2a.capitalize()
                uid2 = int(f'{num1}0{int(str(num2) * 2)}')

                users[uid2] = {'id': uid2, 'is_bot': bot, 'username': name2a,
                               'first_name': name2b, 'last_name': name2b}

                if not bot:
                    chats[uid2] = {'id': uid2, 'type': 'private', 'username': name2a,
                                   'first_name': name2b, 'last_name': name2b}

                nicks[name2a] = uid2
                nicks[f'{name2b} {name2b}'] = uid2
                consent[uid2] = False

        for num1, typ in enumerate(('group', 'supergroup', 'channel'), 1):
            for num2 in range(1, 4):
                cid = int(f'-100{num1}{num2}')
                name = f'{typ[0]}{num2}'
                title = name.capitalize()
                chats[cid] = {'id': cid, 'title': title, 'username': name, 'type': typ}
                nicks[name] = cid
                consent[cid] = True

        self.assertEqual(USERS, users)
        self.assertEqual(CHATS, chats)
        self.assertEqual(NICKS, nicks)
        self.assertEqual(CONSENT, consent)

        base = {'file_id': 'FILE_ID', 'file_size': 123}
        size = {'height': 123, 'width': 123}
        baze = {**base, **size}
        self.assertEqual(MEDIA_MAP, {
            'photo': {'photo': [baze, baze]},
            'sticker': {'sticker': {
                **baze,
                'emoji': '👍', 'is_animated': False, 'set_name': 'SET_NAME',
                'thumb': baze,
            }},
            'document': {'document': {
                **base,
                'thumb': baze,
                'file_name': 'FILE_NAME.pdf',
                'mime_type': 'document/pdf',
            }},
            'animation': {
                'document': {
                    **base,
                    'thumb': baze,
                    'file_name': 'FILE_NAME.mp4',
                    'mime_type': 'video/mp4'
                },
                'animation': {
                    **baze,
                    'thumb': baze,
                    'file_name': 'FILE_NAME.mp4',
                    'mime_type': 'video/mp4',
                    'duration': 1
                }
            },
        })


    @mock.patch('sys.exit')
    def test_dispatcher_handlers_dict(self, _exit):
        dic = DispatcherHandlersDict()

        self.assertEqual(dic, {})

        handler_0 = mock.Mock()
        handler_0.handle_update.side_effect = KeyError('KeyError')

        handler_1 = mock.Mock()
        handler_1.handle_update.side_effect = BadRequest('BadRequest')

        dic[0] = [handler_0, handler_1]

        self.assertEqual(dic, {0: [handler_0, handler_1]})
        self.assertIn(0, dic)

        lst = dic[0]

        lst[0].handle_update()

        with self.assertRaises(BadRequest):
            lst[1].handle_update()

        self.assertIs(handler_0.__wrapped__, True)
        self.assertIs(handler_1.__wrapped__, True)
        self.assertEqual(_exit.call_args_list, [mock.call('bot execution error')])


    def test_fakejobqueue(self):
        func_runs = []

        def func(bot, job):
            func_runs.append(f'{bot}|{job.context}')

        # ---

        job_queue = FakeJobQueue('bøt')
        data = (
            ('once', 40000),
            ('repeating', 19000),
            ('once', 90000),
            ('once', 1000),
            ('once', 7000),
            ('once', 2000),
            ('once', 18000),
            ('daily', 5000),
        )
        for method, secs in data:
            context = f'{method[0]}{secs:0>5}'
            job = getattr(job_queue, f'run_{method}')(func, secs, context=context)
            self.assertEqual(job.name, 'func')

        func_runs_expected = [
            #                    0
            'bøt|o01000',  #  1000
            'bøt|o02000',  #  2000
            'bøt|d05000',  #  5000 +
            'bøt|o07000',  #  7000
            'bøt|o18000',  # 18000
            'bøt|r19000',  # 19000 ·
            'bøt|r19000',  # 38000 ·
            'bøt|o40000',  # 40000
            'bøt|r19000',  # 57000 ·
            'bøt|r19000',  # 76000 ·
            #                86400 1 day
            'bøt|o90000',  # 90000
            'bøt|d05000',  # 91400 +
            'bøt|r19000',  # 95000 ·
        ]

        for _ in range(len(func_runs_expected)):
            job_queue.tick()

        self.assertEqual(func_runs, func_runs_expected)

        # ---

        def func_error(bot, job):
            raise RuntimeError('test')

        func_runs.clear()

        job_queue = FakeJobQueue('bøt')

        job1 = job_queue.run_once(func, 15, context='job1')
        job2 = job_queue.run_repeating(func, 10, first=0, context='job2')
        job_queue.run_once(func_error, 26, context='job3')

        func_runs_expected = [
            'bøt|job2',  #  0
            'bøt|job2',  # 10
            # job1       # 15 not enabled
            'bøt|job2',  # 20
            # job2       # 26 raise RuntimeError
            'bøt|job2',  # 30
        ]

        job_queue.tick()
        job1.enabled = False
        job_queue.tick()
        job_queue.tick()    # return job1
        job_queue.tick()
        job_queue.tick()    # raise job2
        job_queue.tick()
        job2.schedule_removal()
        job_queue.tick()    # return job2
        job_queue.tick()    # empty
        job_queue.tick()    # empty

        self.assertEqual(func_runs, func_runs_expected)

        # ---

        with self.subTest(case='next_t is None'):
            with self.assertRaises(ValueError):
                job_queue.run_once(func, None)


class TestTgsTelegramServer(unittest.TestCase):

    maxDiff = None  # pylint: disable=invalid-name

    def setUp(self):
        self.ini = int(time.time())
        self.tgs = TelegramServer()
        self.burl = f'{self.tgs.BASE_URL}{TOKEN}/'


    def _check_message(self, message, expected, message_id=1):
        # If the message is not stored, the message_id is always 1

        self.assertEqual(message.pop('message_id'), message_id)

        now = int(time.time())
        for key in ('date', 'edit_date', 'forward_date'):
            diff = now - message.pop(key, now)
            self.assertGreaterEqual(diff, 0)
            self.assertLessEqual(diff, now - self.ini)

        # The rest must be the same
        self.assertEqual(message, expected)


    def _check_update(self, update, expected, variable):
        full_dic = update.to_dict()
        self.assertIn(variable, full_dic)
        dic = {key: full_dic[variable].get(key, object()) for key in expected}
        self.assertEqual(dic, expected)


    def test_get_user_and_user_id(self):
        data = (
            ('bot', 11111),
            (11111, 11111),
            ('dev', 22222),
            (22222, 22222),
        )
        for key, uid in data:
            with self.subTest(key=key, uid=uid):
                self.assertEqual(self.tgs.get_user_id(key), uid)
                self.assertEqual(self.tgs.get_user(key)['id'], uid)
        data = (
            0,
            99999,
            'Dev',
            'fake',
            # chat
            -10011,
        )
        for key in data:
            with self.subTest(key=key):
                with self.assertRaises(BadRequest):
                    self.tgs.get_user_id(key)
                with self.assertRaises(BadRequest):
                    self.tgs.get_user(key)


    def test_get_chat_and_chat_id(self):
        data = (
            ('g1', -10011),
            (-10011, -10011),
            ('s2', -10022),
            (-10022, -10022),
            ('c3', -10033),
            (-10033, -10033),
        )
        for key, cid in data:
            with self.subTest(key=key, cid=cid):
                self.assertEqual(self.tgs.get_chat_id(key), cid)
                self.assertEqual(self.tgs.get_chat(key)['id'], cid)
        data = (
            0,
            99999,
            'G1',
            'fake',
            # users are private chats (valid data)
        )
        for key in data:
            with self.subTest(key=key):
                with self.assertRaises(BadRequest):
                    self.tgs.get_chat_id(key)
                with self.assertRaises(BadRequest):
                    self.tgs.get_chat(key)


    def test_set_states(self):
        status_users = {'member': ('bot',), 'left': ('dev', 'adm', 'usr')}
        chats = [f'{chat}{index}' for chat in 'gsc' for index in range(1, 4)]
        data = {}
        for status, users in status_users.items():
            for user in users:
                for chat in chats:
                    data.setdefault(user, {})[chat] = status

        for user, chat_status in data.items():
            for chat, status in chat_status.items():
                with self.subTest(user=user, chat=chat, status=status):
                    uid = self.tgs.get_user_id(user)
                    cid = self.tgs.get_chat_id(chat)
                    self.assertEqual(self.tgs.status[cid][uid], status)
        # a: administrator
        # c: creator
        # m: member
        # l: left
        # k: kicked
        # r: restricted
        table = ('    g1 g2 s2 c3\n'
                 'bot  a  -  m  m\n'
                 'dev  c  -  a  a\n'
                 'usr  l  m  k  r')
        self.tgs.set_states(table)
        data['bot']['g1'] = 'administrator'
        data['bot']['g2'] = 'left'
        data['dev']['g1'] = 'creator'
        data['dev']['s2'] = 'administrator'
        data['dev']['c3'] = 'administrator'
        data['usr']['g2'] = 'member'
        data['usr']['s2'] = 'kicked'
        data['usr']['c3'] = 'restricted'
        for user, chat_status in data.items():
            for chat, status in chat_status.items():
                with self.subTest(user=user, chat=chat, status=status):
                    uid = self.tgs.get_user_id(user)
                    cid = self.tgs.get_chat_id(chat)
                    self.assertEqual(self.tgs.status[cid][uid], status)


    def test_next_unique_id(self):
        self.assertEqual(self.tgs.next_unique_id(), 1)
        self.assertEqual(self.tgs.next_unique_id(), 2)


    def test_next_message_id(self):
        chat_id = 1

        # With no messages always returns 1
        self.assertEqual(self.tgs.next_message_id(chat_id), 1)
        self.assertEqual(self.tgs.next_message_id(chat_id), 1)

        data = (
            ([{'message_id': 1}, {'message_id': 2}, {'message_id': 3}], 4),
            ([{'message_id': 1}, {'message_id': 5}, {'message_id': 6}], 7),
            ([{'message_id': 1}, {'message_id': 5}, {'message_id': 8}], 9),
        )
        for messages, next_id in data:
            with self.subTest(messages=messages, next_id=next_id):
                self.tgs.messages = {chat_id: messages}
                self.assertEqual(self.tgs.next_message_id(chat_id), next_id)
                # Repeated calls do not change the result
                self.assertEqual(self.tgs.next_message_id(chat_id), next_id)

        with self.subTest(case='unsorted messages'):
            with self.assertRaises(IndexError):
                messages = [{'message_id': 1}, {'message_id': 4}, {'message_id': 2}]
                self.tgs.messages = {chat_id: messages}
                self.tgs.next_message_id(chat_id)


    def test_get_message(self):
        chat_id = 1

        with self.subTest(case='chat not found'):
            self.assertEqual(self.tgs.get_message(chat_id, None, False), {})
            with self.assertRaises(BadRequest):
                self.tgs.get_message(chat_id)

        self.tgs.messages = {chat_id: []}  # e.g. all messages have been deleted

        with self.subTest(case='message not found'):
            self.assertEqual(self.tgs.get_message(chat_id, None, False), {})
            with self.assertRaises(BadRequest):
                self.tgs.get_message(chat_id, None)

            self.assertEqual(self.tgs.get_message(chat_id, 1, False), {})
            with self.assertRaises(BadRequest):
                self.tgs.get_message(chat_id, 1)

        msg_1 = {'message_id': 1}
        msg_2 = {'message_id': 2}
        msg_4 = {'message_id': 4}
        self.tgs.messages = {chat_id: [msg_1, msg_2, msg_4]}

        self.assertEqual(self.tgs.get_message(chat_id), msg_4)
        self.assertEqual(self.tgs.get_message(chat_id, 1), msg_1)
        self.assertEqual(self.tgs.get_message(chat_id, 2), msg_2)
        self.assertEqual(self.tgs.get_message(chat_id, 4), msg_4)

        with self.subTest(case='message not found'):
            self.assertEqual(self.tgs.get_message(chat_id, 5, False), {})
            with self.assertRaises(BadRequest):
                self.tgs.get_message(chat_id, 5)

        with self.subTest(case='unsorted messages'):
            with self.assertRaises(IndexError):
                self.tgs.messages = {chat_id: [msg_1, msg_4, msg_2]}
                self.tgs.get_message(chat_id)


    def test_add_message(self):
        chat_id = 1

        msg_1 = {'message_id': 1, 'chat': {'id': chat_id}, 'key': '1'}
        msg_2 = {'message_id': 2, 'chat': {'id': chat_id}, 'key': '2'}
        msg_2b = {'message_id': 2, 'chat': {'id': chat_id}, 'key': '2b'}
        msg_3 = {'message_id': 3, 'chat': {'id': chat_id}, 'key': '3'}

        self.tgs.messages = {chat_id: [msg_1, msg_2]}

        with self.subTest(case='is not message'):
            with self.assertRaises(ValueError):
                self.tgs.add_message(5)

        self.tgs.add_message(msg_3)
        self.assertEqual(self.tgs.messages, {chat_id: [msg_1, msg_2, msg_3]})

        self.tgs.add_message(msg_2b)
        self.assertEqual(self.tgs.messages, {chat_id: [msg_1, msg_2b, msg_3]})


    def test_del_message(self):
        chat_id = 1
        msg_1 = {'message_id': 1}
        msg_2 = {'message_id': 2}
        self.tgs.messages = {chat_id: [msg_1, msg_2]}

        with self.subTest(case='message not found'):
            with self.assertRaises(BadRequest):
                self.tgs.del_message(chat_id, 3)
            self.assertEqual(self.tgs.messages, {chat_id: [msg_1, msg_2]})

        self.tgs.del_message(chat_id, 2)
        self.assertEqual(self.tgs.messages, {chat_id: [msg_1]})


    def test_get_emitter_username(self):
        data = (
            # This does not use self.nicks or self.users
            ({'from': {'username': 'aaa', 'id': 1}}, 'aaa'),
            #
            ({'author_signature': 'bot'}, 'bot'),
            ({'author_signature': 'Bot Bot'}, 'bot'),
        )
        for dic, expected in data:
            with self.subTest(dic=dic):
                self.assertEqual(self.tgs.get_emitter_username(dic), expected)

        data = (
            ({'from': {'id': 1}}, KeyError),
            ({'username': 'Cupidus'}, KeyError),
        )
        for dic, error in data:
            with self.subTest(dic=dic):
                with self.assertRaises(error):
                    self.tgs.get_emitter_username(dic)


    def test_generate_message(self):
        data = (
            (None, 'g1', f'migrate:{G1ID}'),
            (None, 'g1', f'migrate:{C1ID}'),
            (None, 's1', f'migrate:{G1ID}'),
            (None, 's1', f'migrate:{S1ID}'),
            (None, 's1', f'migrate:{C1ID}'),
            (None, 'c1', f'migrate:{G1ID}'),
            (None, 'c1', f'migrate:{S1ID}'),
            (None, 'c1', f'migrate:{C1ID}'),
        )
        for user, chat, content in data:
            with self.subTest(user=user, chat=chat, content=content):
                with self.assertRaises(ValueError):
                    self.tgs.generate_message(user, chat, content)

        cmd = {'length': 4, 'offset': 0, 'type': 'bot_command'}
        dev = USERS[DEVID]

        msg_1 = {**DEV_G1, 'date': int(time.time()), 'message_id': 1, 'text': 'text'}
        msg_2 = {**DEV_G1, 'date': int(time.time()), 'message_id': 2, 'photo': PHOTO}
        self.tgs.messages[G1ID] = [msg_1, msg_2]

        data = (
            ('dev', None, 'text', {**DEV_DEV, 'text': 'text'}),
            ('dev', None, '/cmd', {**DEV_DEV, 'text': '/cmd', 'entities': [cmd]}),
            ('dev', 'g1', 'photo', {**DEV_G1, 'photo': PHOTO}),
            ('dev', 'g1', 'title:GG1', {**DEV_G1, 'new_chat_title': 'GG1'}),
            ('dev', 'g1', 'edit:1:tex', {**DEV_G1, 'text': 'tex'}),
            ('dev', 'g1', 'reply:1:tex', {**DEV_G1,
                                          'text': 'tex',
                                          'reply_to_message': msg_1}),
            ('dev', None, f'forward:{G1ID}:1', {**DEV_DEV,
                                                'text': 'text',
                                                'forward_from': DEV_G1['from']}),
            ('dev', None, f'forward:{G1ID}:2', {**DEV_DEV,
                                                'photo': PHOTO,
                                                'forward_from': DEV_G1['from']}),
            ('dev', 'g1', 'members:dev', {**DEV_G1, 'new_chat_members': [dev]}),
            ('dev', 'g1', 'members:-dev', {**DEV_G1, 'left_chat_member': dev}),
            ('dev', 'g1', f'migrate:{S1ID}', {**DEV_G1, 'migrate_to_chat_id': S1ID}),
        )
        for user, chat, content, expected in data:
            with self.subTest(user=user, chat=chat, content=content):
                message = self.tgs.generate_message(user, chat, content)

                message_id = 3 if chat == 'g1' else 1
                if content == 'edit:1:tex':
                    message_id = 1

                self._check_message(message, expected, message_id)


    def test_process_message(self):
        msg_a = {**BOT_DEV, 'date': int(time.time()), 'message_id': 1, 'text': 'text'}
        msg_b = {**BOT_G1, 'date': int(time.time()), 'message_id': 1, 'text': 'text'}
        msg_c = {**BOT_G1, 'date': int(time.time()), 'message_id': 2, 'photo': PHOTO}
        self.tgs.messages[DEVID] = [msg_a]
        self.tgs.messages[G1ID] = [msg_b, msg_c]
        data = (
            # New message
            (2,
             {'text': 'text', 'chat_id': DEVID, 'reply_markup': '{"a": "b"}'},
             {**BOT_DEV, 'text': 'text', 'reply_markup': {'a': 'b'}}),

            # Edit message
            (1,
             {'text': 'text', 'chat_id': DEVID, 'message_id': '1'},
             {**BOT_DEV, 'text': 'text'}),

            # Reply to message
            (2,
             {'text': 'text', 'chat_id': DEVID, 'reply_to_message_id': '1'},
             {**BOT_DEV, 'text': 'text', 'reply_to_message': msg_a}),

            # Forward text message
            (2,
             {'chat_id': DEVID, 'from_chat_id': str(G1ID), 'message_id': '1'},
             {**BOT_DEV, 'text': msg_b['text'], 'forward_from': msg_b['from']}),

            # Forward media message
            (2,
             {'chat_id': DEVID, 'from_chat_id': str(G1ID), 'message_id': '2'},
             {**BOT_DEV, 'photo': PHOTO, 'forward_from': msg_c['from']}),
        )
        for message_id, body, expected in data:
            with self.subTest(body=body, expected=expected):
                message = self.tgs.process_message(body)
                self._check_message(message, expected, message_id)


    def test__check_member_edit_permission(self):
        # pylint: disable=protected-access
        status = {
            'c': 'creator',
            'a': 'administrator',
            'm': 'member',
            'r': 'restrict',
        }
        data = (
            ('restrictchatmember', G1ID, 'c', 'm', True),
            ('restrictchatmember', G1ID, 'c', 'a', True),
            ('restrictchatmember', G1ID, 'a', 'm', True),
            ('restrictchatmember', G1ID, 'a', 'a', True),
            ('restrictchatmember', G1ID, 'm', 'm', True),
            ('restrictchatmember', G1ID, 'm', 'a', True),
            ('restrictchatmember', G1ID, 'r', 'a', True),

            ('restrictchatmember', C1ID, 'c', 'm', True),
            ('restrictchatmember', C1ID, 'c', 'a', True),
            ('restrictchatmember', C1ID, 'a', 'm', True),
            ('restrictchatmember', C1ID, 'a', 'a', True),
            ('restrictchatmember', C1ID, 'm', 'm', True),
            ('restrictchatmember', C1ID, 'm', 'a', True),
            ('restrictchatmember', C1ID, 'r', 'a', True),

            ('restrictchatmember', S1ID, 'c', 'm', True),
            ('restrictchatmember', S1ID, 'c', 'a', True),
            ('restrictchatmember', S1ID, 'a', 'm', True),
            ('restrictchatmember', S1ID, 'a', 'a', True),
            ('restrictchatmember', S1ID, 'm', 'm', True),
            ('restrictchatmember', S1ID, 'm', 'a', False),
            ('restrictchatmember', S1ID, 'r', 'a', False),

            ('kickchatmember', G1ID, 'c', 'm', True),
            ('kickchatmember', G1ID, 'c', 'a', True),
            ('kickchatmember', G1ID, 'a', 'm', True),
            ('kickchatmember', G1ID, 'a', 'a', True),
            ('kickchatmember', G1ID, 'm', 'm', True),
            ('kickchatmember', G1ID, 'm', 'a', False),
            ('kickchatmember', G1ID, 'r', 'a', False),

            ('kickchatmember', C1ID, 'c', 'm', True),
            ('kickchatmember', C1ID, 'c', 'a', True),
            ('kickchatmember', C1ID, 'a', 'm', True),
            ('kickchatmember', C1ID, 'a', 'a', True),
            ('kickchatmember', C1ID, 'm', 'm', True),
            ('kickchatmember', C1ID, 'm', 'a', False),
            ('kickchatmember', C1ID, 'r', 'a', False),

            ('kickchatmember', S1ID, 'c', 'm', True),
            ('kickchatmember', S1ID, 'c', 'a', True),
            ('kickchatmember', S1ID, 'a', 'm', True),
            ('kickchatmember', S1ID, 'a', 'a', True),
            ('kickchatmember', S1ID, 'm', 'm', True),
            ('kickchatmember', S1ID, 'm', 'a', False),
            ('kickchatmember', S1ID, 'r', 'a', False),
        )
        for cmd, cid, dev, bot, error in data:
            with self.subTest(cmd=cmd, cid=cid, dev=dev, bot=bot, error=error):

                self.tgs.status[cid][DEVID] = status[dev]
                self.tgs.status[cid][BOTID] = status[bot]

                if error:
                    with self.assertRaises(BadRequest):
                        self.tgs._check_member_edit_permission(cmd, cid, DEVID)
                else:
                    response = self.tgs._check_member_edit_permission(cmd, cid, DEVID)
                    self.assertEqual(response, {'ok': True, 'result': True})


    def test__check_chat_settings(self):
        # pylint: disable=protected-access

        # By default in all chat (except private) can be sent
        data = (G1ID, S1ID, C1ID, G2ID, S2ID, C2ID, G3ID, S3ID, C3ID)
        for cid in data:
            with self.subTest(cid=cid):
                self.assertIsNone(self.tgs._check_chat_settings(cid))

        # ... in private chat cannot
        data = (DEVID, ADMID, USRID)
        for cid in data:
            with self.subTest(cid=cid):
                with self.assertRaises(Unauthorized):
                    self.tgs._check_chat_settings(cid)

        # ... and send himself in a meaningless
        with self.subTest(cid=BOTID):
            with self.assertRaises(ValueError):
                self.tgs._check_chat_settings(BOTID)

        # Also, groups can migrate
        with self.subTest(case='migrated group'):
            self.tgs.migrations[G1ID] = S1ID
            with self.assertRaises(ChatMigrated):
                self.tgs._check_chat_settings(G1ID)

        # ... or not exist
        with self.subTest(case='chat not found'):
            with self.assertRaises(BadRequest):
                self.tgs._check_chat_settings(0)

        chat_id = G2ID

        # left
        with self.subTest(case='bot is not a member of the chat'):
            self.tgs.status[chat_id][BOTID] = 'left'
            with self.assertRaises(Unauthorized):
                self.tgs._check_chat_settings(chat_id)

        # kicked
        with self.subTest(case='bot was kicked from the chat'):
            self.tgs.status[chat_id][BOTID] = 'kicked'
            with self.assertRaises(Unauthorized):
                self.tgs._check_chat_settings(chat_id)


    def test_content_members(self):
        with self.subTest(case='removal must be of a single member'):
            with self.assertRaises(ValueError):
                self.tgs.generate_message(DEVID, G1ID, f'members:-adm,usr')

        data = (
            (DEVID, G1ID, 'members:dev', [DEVID]),
            (DEVID, G1ID, 'members:usr', [USRID]),
            (DEVID, S2ID, 'members:adm,usr', [ADMID, USRID]),
            (DEVID, S2ID, 'members:-dev', [DEVID]),
            (DEVID, S2ID, 'members:-usr', [USRID]),
            (DEVID, G1ID, f'members:{DEVID}', [DEVID]),
            (None, G1ID, 'members:dev', [DEVID]),
            (None, G1ID, 'members:-dev', [DEVID]),
        )
        for user_id, chat_id, content, user_ids in data:
            with self.subTest(user_id=user_id, chat_id=chat_id, content=content):

                if '-' in content:
                    status_1 = 'member'
                    status_2 = 'left' if user_id in user_ids else 'kicked'
                else:
                    status_1 = 'left'
                    status_2 = 'member'

                for uid in user_ids:
                    self.tgs.status[chat_id][uid] = status_1

                self.tgs.generate_message(user_id, chat_id, content)

                for uid in user_ids:
                    self.assertEqual(self.tgs.status[chat_id][uid], status_2)


    def test_content_migrate(self):
        msg_a = {**DEV_G1, 'date': 1, 'message_id': 1, 'text': 'text1'}
        msg_b = {**DEV_S1, 'date': 1, 'message_id': 1, 'text': 'text1'}
        msg_c = {**DEV_S2, 'date': 2, 'message_id': 2, 'text': 'text2'}

        before = {G1ID: [msg_a], S2ID: [msg_c]}
        after = {S1ID: [msg_b], S2ID: [msg_c]}

        self.tgs.messages = before

        self.assertIn(G1ID, self.tgs.chats)
        self.assertNotIn(G1ID, self.tgs.migrations)
        self.assertEqual(self.tgs.messages, before)

        self.tgs.generate_message(DEVID, G1ID, 'migrate:s1')

        self.assertIn(G1ID, self.tgs.chats)
        self.assertIn(G1ID, self.tgs.migrations)
        self.assertEqual(self.tgs.migrations[G1ID], S1ID)
        self.assertEqual(self.tgs.messages, after)


    # --- More integral tests


    @mock.patch('sys.exit')
    @mock.patch('tgs.Bot')
    @mock.patch('tgs.Dispatcher')
    @mock.patch('traceback.print_exc')
    def test_request(self, _print_exc, _dispatcher, _bot, _exit):
        # pylint: disable=protected-access,too-many-locals,too-many-statements

        self.tgs.add_handlers(not_doing_anything)

        request = self.tgs.bot._request._request_wrapper

        message = {**BOT_DEV, 'text': 'text'}
        forward = {**BOT_G1, 'text': 'text', 'forward_from': message['from']}

        result_ok = {'ok': True, 'result': True}

        self.tgs.status = {
            G2ID: {BOTID: 'member'},
            S1ID: {BOTID: 'administrator',
                   DEVID: 'creator'},
            S2ID: {BOTID: 'administrator',
                   USRID: 'member'},
            S3ID: {BOTID: 'administrator',
                   USRID: 'member'},
        }
        self.tgs.consent[DEVID] = True

        s1_dev_status = {'user': USERS[DEVID], 'status': 'creator'}
        s1_bot_status = {'user': USERS[BOTID], 'status': 'administrator'}

        self.tgs.add_exception_throwing(TelegramError('error'), tick=2, cmd='getme')
        self.tgs.add_exception_throwing(Unauthorized('error'), tick=None, cmd='getme')
        self.tgs.add_exception_throwing(BadRequest('error'), tick=5, cmd=None)
        self.tgs.add_exception_throwing(ChatMigrated(G1ID), tick=None, cmd=None)

        data = (
            (2, TelegramError, 'getme'),
            (1, Unauthorized, 'getme'),
            (5, BadRequest, None),
            (3, ChatMigrated, None),
        )
        self.assertEqual(len(self.tgs.exceptions), len(data))
        for tick, exception, cmd in data:
            exception_stored, cmd_stored, tick_stored = self.tgs.exceptions[tick]
            self.assertEqual(cmd, cmd_stored)
            self.assertEqual(tick, tick_stored)
            self.assertEqual(exception, exception_stored.__class__)

        data = (
            # tick=1
            ('getme',
             None,
             Unauthorized),

            # tick=2
            ('getme',
             None,
             TelegramError),

            # tick=3
            ('getme',
             None,
             ChatMigrated),

            # tick=4
            ('getme',
             None,
             {'ok': True, 'result': USERS[BOTID]}),

            # tick=5
            ('answercallbackquery',
             None,
             BadRequest),

            # tick=6
            ('getme',
             None,
             {'ok': True, 'result': USERS[BOTID]}),

            ('sendmessage',
             {'text': 't' * 4097, 'chat_id': DEVID},
             BadRequest),

            # -----------

            ('getme',
             None,
             {'ok': True, 'result': USERS[BOTID]}),

            ('answercallbackquery',
             None,
             result_ok),

            ('sendchataction',
             None,
             result_ok),

            ('leavechat',
             {'chat_id': G2ID},
             result_ok),


            ('sendmessage',
             {'text': 'text', 'chat_id': DEVID},
             {'ok': True, 'result': message}),

            ('editmessagetext',
             {'text': 'text', 'chat_id': DEVID, 'message_id': '1'},
             {'ok': True, 'result': message}),

            ('forwardmessage',
             {'chat_id': G1ID, 'from_chat_id': str(DEVID), 'message_id': '1'},
             {'ok': True, 'result': forward}),

            ('deletemessage',
             {'chat_id': DEVID, 'message_id': '1'},
             result_ok),


            ('sendphoto',
             {'text': 'text', 'chat_id': DEVID},
             {'ok': True, 'result': {**BOT_DEV, 'photo': PHOTO}}),


            ('getchatmember',
             {'chat_id': S1ID, 'user_id': DEVID},
             {'ok': True, 'result': s1_dev_status}),

            ('getchat',
             {'chat_id': G1ID},
             {'ok': True, 'result': CHATS[G1ID]}),

            ('getchatadministrators',
             {'chat_id': S1ID},
             {'ok': True, 'result': [s1_bot_status, s1_dev_status]}),


            ('restrictchatmember',
             {'chat_id': S2ID, 'user_id': USRID},
             {'ok': True, 'result': True}),

            ('kickchatmember',
             {'chat_id': S3ID, 'user_id': USRID},
             {'ok': True, 'result': True}),
        )
        for cmd, body, expected in data:
            url = self.burl + cmd
            body = json.dumps(body) if body else ''
            with self.subTest(cmd=cmd, body=body, expected=expected):
                if isinstance(expected, dict):
                    response = request('GET', url, body=body)
                    response = json.loads(response)
                    result = response['result']
                    if isinstance(result, dict) and 'message_id' in result:
                        self._check_message(result, expected['result'])
                        self.assertEqual(response['ok'], expected['ok'])
                    else:
                        self.assertEqual(response, expected)
                else:
                    with self.assertRaises(expected):
                        request('GET', url, body=body)
                self.assertEqual(_exit.call_count, 0)

        self.assertEqual(self.tgs.status, {
            G2ID: {BOTID: 'left'},
            S1ID: {BOTID: 'administrator',
                   DEVID: 'creator'},
            S2ID: {BOTID: 'administrator',
                   USRID: 'restricted'},
            S3ID: {BOTID: 'administrator',
                   USRID: 'kicked'},
        })

        self.tgs.exceptions.clear()

        self.tgs.add_exception_throwing(TelegramError('error'), cmd='getme') # tick=1
        with self.subTest(case='unreleased error, "getme" != "sendmessage"'):
            self.assertEqual(_exit.call_count, 0)
            url = self.burl + 'sendmessage'
            body = json.dumps({'text': 'text', 'chat_id': DEVID})
            request('GET', url, body=body)
            self.assertEqual(_exit.call_count, 1)

        self.tgs.exceptions.clear()

        # add_exception_throwing will never be executed
        self.tgs.add_exception_throwing(TelegramError('error')) # tick=1
        with self.subTest(case='add_exception_throwing will never be executed'):
            with self.assertRaises(ValueError):
                self.tgs.add_exception_throwing(TelegramError('error'), tick=0)

        self.tgs.exceptions.clear()

        # add_exception_throwing with existing tick
        self.tgs.add_exception_throwing(TelegramError('error')) # tick=1
        with self.subTest(case='add_exception_throwing with existing tick'):
            with self.assertRaises(ValueError):
                self.tgs.add_exception_throwing(TelegramError('error'), tick=1)

        self.tgs.exceptions.clear()

        # Bot without permission
        cmd = 'sendphoto'
        body = {'text': 'text', 'chat_id': USRID}
        with self.subTest(case='Bot without permission'):
            with self.assertRaises(Unauthorized):
                request('GET', self.burl + cmd, body=json.dumps(body))

        # Commands not implemented
        data = (
            'getUpdates',
            'setWebhook',
            'deleteWebhook',
            'getWebhookInfo',
        )
        for cmd in data:
            with self.subTest(cmd=cmd):
                _print_exc.reset_mock()

                with self.assertRaises(NotImplementedError):
                    request('GET', self.burl + cmd)

                self.assertEqual(_print_exc.call_count, 1)

        # Invalid values
        valid_url = self.burl + 'cmd'
        data = (
            ({'url': object()}, TypeError),
            ({'url': 'url'}, AttributeError),
            ({'url': valid_url, 'body': 'text'}, json.JSONDecodeError),
            ({'url': valid_url, 'body': object()}, TypeError),
        )
        for params, error in data:
            with self.subTest(params=params):
                _print_exc.reset_mock()

                with self.assertRaises(error):
                    request('METHOD', **params)

                self.assertEqual(_print_exc.call_count, 1)


    def test_empty(self):
        self.assertTrue(self.tgs.empty())
        self.tgs.responses.append('a')
        self.assertFalse(self.tgs.empty())
        self.tgs.responses.popleft()
        self.assertTrue(self.tgs.empty())


    def test_send(self):
        self.tgs.dispatcher = FakeDispatcher()
        self.tgs.status[G1ID][DEVID] = 'member'
        self.tgs.status[C1ID][DEVID] = 'member'
        self.tgs.status[G1ID][USRID] = 'member'
        self.tgs.status[S1ID][USRID] = 'member'

        cb_text = MARKUP_IKB_DICT['inline_keyboard'][0][0]['text']
        cb_data = MARKUP_IKB_DICT['inline_keyboard'][0][0]['callback_data']
        msgs = (
            {**DEV_G1, 'message_id': 1, 'date': 1, 'reply_markup': MARKUP_IKB_DICT},
            {**DEV_C1, 'message_id': 1, 'date': 1, 'text': 'text'},
        )
        data = (
            ({'user': 'dev', 'chat': None, 'content': 'text a'},
             {**DEV_DEV, 'text': 'text a'},
             'message',
             {}),

            ({'user': 'dev', 'chat': 'dev', 'content': 'text b', 'caption': 'c'},
             {**DEV_DEV, 'text': 'text b', 'caption': 'c'},
             'message',
             {}),

            ({'user': 'dev', 'chat': 'c1', 'content': 'text b'},
             {**DEV_C1, 'text': 'text b'},
             'channel_post',
             {}),

            ({'user': 'dev', 'chat': 'c1', 'content': 'edit:1:text c'},
             {**DEV_C1, 'text': 'text c'},
             'edited_channel_post',
             {C1ID: [msgs[1]]}),

            ({'user': 'usr', 'chat': 'g1', 'content': 'members:-usr'},
             {**USR_G1, 'left_chat_member': USERS[USRID]},
             'message',
             {}),

            ({'user': 'usr', 'chat': 's1', 'content': 'members:-usr'},
             {**USR_S1, 'left_chat_member': USERS[USRID]},
             'message',
             {}),

            # This should be the last test, used later: in "changed callback data"
            ({'user': 'dev', 'chat': 'g1', 'content': cb_text},
             {'from': USERS[DEVID], 'data': cb_data},
             'callback_query',
             {G1ID: [msgs[0]]}),
        )
        for in_dic, expected, variable, messages in data:
            with self.subTest(in_dic=in_dic):
                self.tgs.messages = messages
                self.tgs.send(in_dic)

                if in_dic['chat'] == 's1' and in_dic['content'] == 'members:-usr':
                    # Are not issued in supergroups
                    self.assertFalse(bool(self.tgs.dispatcher.updates))
                else:
                    update = self.tgs.dispatcher.updates.pop()
                    self._check_update(update, expected, variable)

        self.assertFalse(bool(self.tgs.dispatcher.updates))

        with self.subTest(case='changed callback data'):
            _cb_data = 'fake callback data'
            _in_dic = {'user': 'dev', 'chat': 'g1', 'content': f'ð:{_cb_data}'}
            _expected = {'from': USERS[DEVID], 'data': _cb_data}

            self.tgs.send(_in_dic)
            _full_dic = self.tgs.dispatcher.updates.pop().to_dict()

            self.assertNotEqual(_cb_data, cb_data)
            self.assertEqual(_full_dic['callback_query']['data'], _cb_data)


    def test_send_not_call_callback(self):
        self.tgs.dispatcher = FakeDispatcher()
        self.tgs.status[G1ID][DEVID] = 'member'
        msg = {**DEV_G1, 'message_id': 1, 'date': 1, 'reply_markup': MARKUP_IKB_DICT}
        in_dic = {'user': 'dev', 'chat': 'g1', 'content': '¢:text'}
        expected = {'from': USERS[DEVID], 'text': 'text'}
        self.tgs.messages = {G1ID: [msg]}
        self.tgs.send(in_dic)
        update = self.tgs.dispatcher.updates.pop()
        self._check_update(update, expected, 'message')


    def test_send_chat_migration(self):
        self.tgs.dispatcher = FakeDispatcher()
        self.tgs.status[G2ID][DEVID] = 'member'
        self.tgs.status[S2ID][DEVID] = 'member'

        msg = {**DEV_G2, 'date': 1, 'message_id': 1, 'text': 'text'}
        in_dic = {'user': 'dev', 'chat': 'g2', 'content': 'migrate:s2'}

        self.tgs.messages = {G2ID: [msg]}
        self.tgs.send(in_dic)

        update_2 = self.tgs.dispatcher.updates.pop()  # "pop" returns the last
        update_1 = self.tgs.dispatcher.updates.pop()

        self.assertEqual(update_1.message.chat.id, G2ID)
        self.assertEqual(update_1.message.migrate_to_chat_id, S2ID)

        self.assertEqual(update_2.message.chat.id, S2ID)
        self.assertEqual(update_2.message.migrate_from_chat_id, G2ID)


    def test_send_join_user(self):
        self.tgs.dispatcher = FakeDispatcher()
        # simplification: restrictions are not taken in consideration
        # if he/she had been kicked out
        self.assertEqual(len(self.tgs.dispatcher.updates), 0)
        self.tgs.send({'user': 'dev', 'chat': 'g2', 'content': 'members:dev'})
        self.assertEqual(len(self.tgs.dispatcher.updates), 1)


    def test_send_simple_failures(self):

        with self.subTest(case='a user who is not in the chat cannot add other users'):
            with self.assertRaises(ValueError):
                self.tgs.send({'user': 'dev', 'chat': 'g3', 'content': 'members:a,b'})

        with self.subTest(case='user and/or chat must be set'):
            with self.assertRaises(ValueError):
                dic = {'user': None, 'chat': None, 'content': 'text'}
                self.tgs.send(dic)

        with self.subTest(case='only with "members:" and "migrate:" can omit user'):
            with self.assertRaises(ValueError):
                dic = {'user': None, 'chat': 'g1', 'content': 'text'}
                self.tgs.send(dic)

        with self.subTest(case='user is not in chat'):
            with self.assertRaises(ValueError):
                dic = {'user': 'dev', 'chat': 'c2', 'content': 'text'}
                self.tgs.send(dic)


    def test_job_tick(self):
        self.tgs.job_queue = FakeJobQueue(None)

        func_runs = []

        def func(bot, job):
            func_runs.append(f'{bot}|{job.context}')

        self.tgs.job_queue.run_once(func, 0, context='tgs.test.tick')

        self.tgs.job_tick()

        self.assertEqual(func_runs, ['None|tgs.test.tick'])


    def test_recv(self):
        cmd = {'length': 4, 'offset': 0, 'type': 'bot_command'}
        data = (
            ({**DEV_DEV, 'text': '/cmd', 'entities': [cmd]},
             {'user': 'dev', 'chat': None, 'content': '/cmd'}),

            ({**DEV_DEV, 'text': 'text', 'caption': 'c'},
             {'user': 'dev', 'chat': 'dev', 'content': 'text', 'caption': 'c'}),

            ({**DEV_DEV, 'reply_markup': {'keyboard': [['a']]}},
             {'user': 'dev', 'chat': None, 'content': None, 'markup': ['a']}),

            ({**DEV_DEV, 'photo': PHOTO},
             {'user': 'dev', 'chat': None, 'content': 'photo'}),
        )
        for in_dic, expected in data:
            with self.subTest(in_dic=in_dic):
                self.tgs.responses.append(in_dic)
                out_dic = self.tgs.recv()

                for dic in (expected, out_dic):
                    if dic['chat'] == dic['user']:
                        dic['chat'] = None

                self.assertEqual(out_dic, expected)
