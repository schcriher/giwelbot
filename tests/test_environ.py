# -*- coding: UTF-8 -*-
# Copyright (C) 2019,2020 Schmidt Cristian Hernán

import re
import pprint       # pylint: disable=unused-import
import builtins     # pylint: disable=unused-import
import datetime
import unittest
import functools
import collections

from environ import (mock, AttributeDict, mocked_get_captcha_text,
                     EmptyGenericClass, BotIOAdapter, mocked_datetime_now)

FAKE_NOW = datetime.datetime(2019, 12, 19, 5, 36)


class TestEnviron(unittest.TestCase):


    def setUp(self):
        self.maxDiff = None  # pylint: disable=invalid-name

        self.test = None
        self.tgs = None
        self.bio = None

        self.last_chat = None
        self.bookmark = 0
        self.history = []


    def test_mocked_get_captcha_text(self):
        data = (
            ('a', '+', 'b', 'a + b'),
            ('1', '-', '7', '1 - 7'),
            ('9', '*', '2', '9 * 2'),
            ('8', '/', '4', '8 / 4'),
        )
        for *items, expected in data:
            with self.subTest(items=items, expected=expected):
                captcha = mocked_get_captcha_text(*items)
                self.assertEqual(captcha, expected)


    def test_attributedict(self):
        dic = AttributeDict(a=1, b=2)
        self.assertEqual(dic['a'], 1)
        self.assertEqual(dic['b'], 2)
        self.assertEqual(dic.a, 1)
        self.assertEqual(dic.b, 2)
        dic['a'] = 11
        dic.b = 22
        self.assertEqual(dic, {'a': 11, 'b': 22})
        del dic['a']
        self.assertEqual(dic, {'b': 22})
        del dic.b
        self.assertEqual(dic, {})

        with self.subTest(case='key'):
            with self.assertRaises(KeyError):
                _ = dic['a']

        with self.subTest(case='attribute'):
            with self.assertRaises(KeyError):
                _ = dic.b


    def test_emptygenericclass(self):
        # pylint: disable=no-member

        obj = EmptyGenericClass(a=1, b=2, c='C')
        self.assertEqual(obj.a, 1)
        self.assertEqual(obj.b, 2)
        self.assertEqual(obj.c, 'C')
        self.assertFalse(hasattr(obj, 'd'))


    def test_botioadapter_set_optionals(self):
        data = (
            ({}, ['a'], {'caption': 'a'}),
            ({}, [[1]], {'markup': [1]}),
            ({}, ['b', [2]], {'caption': 'b', 'markup': [2]}),
            ({'id': 3}, ['a'], {'id': 3, 'caption': 'a'}),
            ({'id': 3}, [[1]], {'id': 3, 'markup': [1]}),
            ({'id': 3}, ['b', [2]], {'id': 3, 'caption': 'b', 'markup': [2]}),
            ({'id': 3}, ['a', object()], {'id': 3, 'caption': 'a'}),
            ({'id': 3}, [[1], 9], {'id': 3, 'markup': [1]}),
            ({'id': 3}, ['b', [2], {}], {'id': 3, 'caption': 'b', 'markup': [2]}),
        )
        for dic, opts, expected in data:
            with self.subTest(dic=dic, opts=opts, expected=expected):
                BotIOAdapter.set_optionals(dic, opts)
                self.assertEqual(dic, expected)


    def test_botioadapter_adjust(self):
        test = mock.Mock(unsafe=True)
        tgs = mock.Mock()
        bio = BotIOAdapter(test, tgs)
        bio.last_chat = 9
        reg = re.compile(r'\w{3}\.\d{3}')
        data = (
            ({'chat': 1}, {'chat': 1}),
            ({'chat': None}, {'chat': 9}),
            ({'chat': 2, 'user': 'dev'}, {'chat': 2, 'user': 'dev'}),
            ({'chat': None, 'user': 'bot'}, {'chat': 9, 'user': 'bot'}),
            ({'chat': 3, 'tex': reg}, {'chat': 3, 'tex': 'abc.123'}),
        )
        for dic, response in data:
            with self.subTest(dic=dic, response=response):
                bio.adjust(dic, response)
                self.assertEqual(dic, response)


    def test_botioadapter_get_name_call(self):
        get_end = lambda name: EmptyGenericClass(__name__=name)

        obj1 = get_end('Obj1')
        obj2 = EmptyGenericClass(func=get_end('Obj2'))
        obj3 = EmptyGenericClass(func=EmptyGenericClass(func=get_end('Obj3')))
        obj4 = functools.partial(lambda a: None)

        data = (
            (obj1, 'Obj1'),
            (obj2, 'Obj2'),
            (obj3, 'Obj3'),
            (obj4, '<lambda>'),
        )
        for obj, name_expected in data:
            with self.subTest(name_expected=name_expected):
                name = BotIOAdapter.get_name_call(obj)
                self.assertEqual(name, name_expected)

        data = (
            1,
            2.,
            'a',
            [],
            {},
            True,
            object(),
        )
        for obj in data:
            with self.subTest(case='unnamed', obj=obj):
                with self.assertRaises(ValueError):
                    BotIOAdapter.get_name_call(obj)


    def test_botioadapter_header(self):
        data = (
            ('a', {'user': 'a', 'chat': None}),
            ('a:b', {'user': 'a', 'chat': 'b'}),
            (':b', {'user': None, 'chat': 'b'}),
        )
        for header, expected in data:
            with self.subTest(header=header, expected=expected):
                obj = BotIOAdapter.HEADER.match(header).groupdict()
                self.assertEqual(obj, expected)

        data = (
            'a:b:1'
            'a:2',
            'a:b:.3',
            'a:.4',
            ':a:b',
            ':b:1',
            ':2',
            '3:a:b',
            '4:b',
            '5:b:a',
            'a:6:b',
        )
        for header in data:
            with self.subTest(header=header):
                with self.assertRaises(AttributeError):
                    BotIOAdapter.HEADER.match(header).groupdict()


    def _check(self):
        # nonlocal
        self.assertEqual(self.bio.last_chat, self.last_chat)
        self.assertEqual(self.bio.bookmark, self.bookmark)
        self.assertEqual(self.bio.history, self.history)
        # reset
        self.test.reset_mock()
        self.tgs.reset_mock()


    @mock.patch('pprint.pprint')
    @mock.patch('builtins.print')
    def test_botioadapter(self, _print, _pprint):
        # pylint: disable=too-many-statements,protected-access

        self.test = mock.Mock(unsafe=True)
        self.tgs = mock.Mock()
        self.tgs.next_sends = collections.deque()
        self.tgs.job_queue._queue.queue = []
        self.bio = BotIOAdapter(self.test, self.tgs)

        next_sends = object()
        interaction = ['dev:g1', 'content', 'caption', ['markup']]
        data = {'user': 'dev', 'chat': 'g1', 'content': 'content',
                'caption': 'caption', 'markup': ['markup']}
        self.last_chat = 'g1'
        self.history.append(interaction)
        self.tgs.next_sends.append(next_sends)
        self.bio.run([interaction])
        self.assertEqual(self.tgs.send.call_args_list, [mock.call(data),
                                                        mock.call(next_sends)])
        self._check()

        func = mock.Mock(spec=lambda **kwargs: None)
        func.__name__ = 'func'
        self.history.append('RUN:func')
        self.bio.run([func])
        self.assertEqual(func.call_args, mock.call(test=self.test,
                                                   tgs=self.tgs))
        self._check()

        self.bookmark = 1
        self.history.clear()
        self.tgs.next_sends.clear()
        self.bio.run([self.bookmark])
        self._check()

        interaction = ['bot', re.compile('tex')]
        data = {'user': 'bot', 'chat': self.last_chat, 'content': 'tex'}
        self.tgs.recv.return_value = data
        self.history.append(interaction)
        self.bio.run([interaction])
        self.assertEqual(self.test.assertEqual.call_args, mock.call(data, data))
        self._check()

        self.history.append('RUN:tick')
        self.bio.run(['tick'])
        self.assertEqual(self.tgs.job_tick.call_args, mock.call(1))
        self._check()

        self.history.append('RUN:tick2')
        self.bio.run(['tick2'])
        self.assertEqual(self.tgs.job_tick.call_args, mock.call(2))
        self._check()

        # --- The last bookmark was 1

        self.history.clear()
        self.bio.history.clear()

        interaction = 2
        job = mock.Mock(unsafe=True)
        job.callback.__name__ = 'job_name'
        self.tgs.job_queue._queue.queue = [(0, job)]

        self.tgs.responses.__len__ = mock.Mock(return_value=0)
        with self.assertRaises(ValueError):
            self.bio.run([interaction])
        self._check()

        self.assertEqual(_print.call_args_list, [
            mock.call('\nbookmark 1'),
            mock.call('\n', len(self.history)),
            mock.call('\n'),
        ])
        self.assertEqual(_pprint.call_args_list, [
            mock.call(self.history),
            mock.call(interaction),
        ])

        _print.reset_mock()
        _pprint.reset_mock()

        # ---

        interaction = 3
        self.tgs.job_queue._queue.queue = []
        self.tgs.responses.__len__ = mock.Mock(return_value=0)
        with self.assertRaises(ValueError):
            self.bio.run([interaction])
        self._check()

        self.assertEqual(_print.call_args_list, [
            mock.call('\nbookmark 1'),
            mock.call('\n', len(self.history)),
            mock.call('\n'),
        ])
        self.assertEqual(_pprint.call_args_list, [
            mock.call(self.history),
            mock.call(interaction),
        ])

        _print.reset_mock()
        _pprint.reset_mock()

        # ---

        interaction = 'tick'
        self.history.append('RUN:tick')
        self.tgs.empty = mock.Mock(return_value=False)
        self.tgs.job_queue._queue.queue = []
        self.tgs.responses.__len__ = mock.Mock(return_value=0)
        with self.assertRaises(ValueError):
            self.bio.run([interaction])
        self._check()

        self.assertEqual(_print.call_args_list, [
            mock.call('\nbookmark 1'),
            mock.call('\n', len(self.history)),
            mock.call('\n'),
        ])
        self.assertEqual(_pprint.call_args_list, [
            mock.call(self.history),
            mock.call(interaction),
        ])

        _print.reset_mock()
        _pprint.reset_mock()

        # ---

        interaction = ['1IncorrectUser', 'content']

        self.tgs.responses.__len__ = mock.Mock(return_value=2)
        with self.assertRaises(AttributeError):
            self.bio.run([interaction])
        self.tgs.empty.side_effect = None
        self._check()

        self.assertEqual(_print.call_args_list, [
            mock.call('\nbookmark 1'),
            mock.call('\n', len(self.history)),
            mock.call('\ntgs.recv 1'),
            mock.call('\ntgs.recv 2'),
            mock.call('\n'),
        ])
        self.assertEqual(_pprint.call_args_list, [
            mock.call(self.history),
            mock.call(interaction),
            mock.call(data),
            mock.call(data),
        ])


    @mocked_datetime_now(datetime, FAKE_NOW)
    def test_mocked_datetime_now(self):
        now = datetime.datetime.now()
        utcnow = datetime.datetime.utcnow()
        self.assertEqual(now, FAKE_NOW)
        self.assertEqual(utcnow, FAKE_NOW)
        self.assertIsInstance(now, datetime.datetime)
        self.assertIsInstance(utcnow, datetime.datetime)


    def test_not_mocked_datetime(self):
        now = datetime.datetime.now()
        utcnow = datetime.datetime.utcnow()
        self.assertNotEqual(now, FAKE_NOW)
        self.assertNotEqual(utcnow, FAKE_NOW)
