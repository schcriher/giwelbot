.PHONY: install run check task test test-fast pylint bandit lines clean clean-full coverage
_:; @echo "🌿👀🌿"

SHELL    := /bin/bash
MKDIR    := /bin/mkdir --parents
RM       := /bin/rm --verbose --force --recursive
GREP     := /bin/grep --extended-regexp --color --line-number

WC       := /usr/bin/wc --lines --max-line-length
FIND     := /usr/bin/find .

PYTHON   := python3
PYLINT   := $(PYTHON) -m pylint
BANDIT   := $(PYTHON) -m bandit
COVERAGE := $(PYTHON) -m coverage

PDFLATEX := /usr/bin/pdflatex -halt-on-error -file-line-error
MOGRIFY  := /usr/bin/mogrify
OPTIPNG  := /usr/bin/optipng -o7 -zm1-9 -strip all

XGETTEXT := /usr/bin/xgettext
MSGMERGE := /usr/bin/msgmerge --verbose --update
MSGFMT   := /usr/bin/msgfmt --verbose --check

NAME     := giwel
VERSION  := $(shell grep -Eso "VERSION\s*=\s*['\"][0-9.]+['\"]" $(NAME)/version.py \
                  | grep -Eso '[0-9.]+')

SOURCE   := $(NAME)
TESTS    := tests
VENV     := .venv
HTML     := .coverage.html

EXTRAS   := extras
BUILD    := build
LOGO     := logo

DOMAIN   := $(NAME)
LOCALE   := locale
POT_FILE := $(LOCALE)/$(DOMAIN).pot
PO_FILES := $(wildcard $(LOCALE)/*/LC_MESSAGES/$(DOMAIN).po)
MO_FILES := $(PO_FILES:.po=.mo)

NO_VENV  := -depth -path "./$(VENV)/*" -o
COV_RUN  := $(COVERAGE) run --omit=*/__*__.py,$(VENV)/*
DISCOVER := -m unittest discover $(TESTS)

REALIGN  := awk '{printf " %5d  %3d  %s\n", $$1, $$2, $$3}'
SUBTOTAL := sed --regexp-extended 's/(.*)(total)/ --------------------\n\1sub\2/g'
TOTALCOV  = awk --assign n=$(1) '{printf " %-10s %4s\n", n, $$$$4}'

SRC_TGT  := captcha config database control texts tools context core
COV_TGT  := $(SRC_TGT:%=cov-%)

# ---

empty_line:
	@echo

install:
	$(PYTHON) -m venv $(VENV)
	source $(VENV)/bin/activate && \
		$(PYTHON) -m pip install -r requirements.txt -r requirements-dev.txt

run:
	$(PYTHON) -m $(SOURCE) -pcvv
	# Possibilities:
	#   python3 -m giwel -pcvv
	#   python3 giwel -pcvv
	#   python3 giwel/__main__.py -pcvv

check: task test pylint bandit lines empty_line cov-all coverage

task:
	@$(GREP) '(FIXME|TODO|XXX|\?\?\?)' $(SOURCE)/*.py $(TESTS)/*.py || :

test:
	$(PYTHON) $(DISCOVER)
	@echo

test-fast:
	env UNTESTED_BOT=true $(PYTHON) $(DISCOVER)
	@echo

pylint:
	$(PYLINT) */*.py \
		--max-module-lines=1200 \
		--good-names=logger,context \
		--disable=missing-docstring

bandit:
	$(BANDIT) $(SOURCE)/*.py \
		--format custom \
		--msg-template "{relpath:20.20s} {line:>4} {severity:<6} {msg}" \
		2>/dev/null
	@echo

lines:
	@echo "Lines MaxL  Files"
	@echo
	@$(WC) $(SOURCE)/*.py | $(REALIGN) | $(SUBTOTAL)
	@echo
	@$(WC) $(TESTS)/*.py | $(REALIGN) | $(SUBTOTAL)
	@echo
	@$(WC) */*.py | $(REALIGN) | tail -1

clean:
	@$(FIND) $(NO_VENV)  -name "*~"          -type f -print -delete
	@$(FIND) $(NO_VENV)  -name "*.log"       -type f -print -delete
	@$(FIND) $(NO_VENV)  -name "*.py[cod]"   -type f -print -delete
	@$(FIND) $(NO_VENV)  -name ".coverage*"  -type f -print -delete
	@$(FIND) $(NO_VENV)  -name "__pycache__" -type d -print -delete
	@$(FIND) -maxdepth 1 -name $(HTML)       -type d -print -exec rm -rf {} +

clean-full: clean
	@$(FIND)             -name "*.py[cod]"   -type f -print -delete
	@$(FIND)             -name "__pycache__" -type d -print -delete

coverage:
	@echo
	$(COV_RUN) --source=. $(DISCOVER)
	@$(COVERAGE) html -d $(HTML)
	@echo
	@$(COVERAGE) report
	@echo

# ---

i18n-extract:
	$(XGETTEXT) \
		--package-name=$(NAME) \
		--package-version=$(VERSION) \
		--default-domain=$(DOMAIN) \
		--output $(POT_FILE) \
		--language='Python' \
		--from-code='UTF-8' \
		--add-comments='TRANSLATORS:' \
		--add-location='full' \
		--check='ellipsis-unicode' \
		--check='quote-unicode' \
		--check='space-ellipsis' \
		--keyword='_:1' \
		--keyword='__:1,2' \
		$(SOURCE)/*.py

%.po: $(POT_FILE)
	@echo
	@echo
	$(MSGMERGE) $@ $(POT_FILE)

%.mo: %.po
	$(MSGFMT) --output-file=$@ $^

i18n-build: $(MO_FILES)

i18n: i18n-extract i18n-build

logo:
	$(MKDIR) $(BUILD)
	$(PDFLATEX) -output-directory $(BUILD) $(EXTRAS)/$(LOGO).tex
	$(MOGRIFY) -format png -path . -density 600x600 -resize 900x $(BUILD)/$(LOGO).pdf
	$(OPTIPNG) $(LOGO).png
	$(RM) $(BUILD)

.PHONY: i18n i18n-extract i18n-build logo

# ---

define TARGET_template =
cov-$(1):
	@env PYTHONPATH=$(dir $(2)) $(COV_RUN) --include=$(2),$(3) -m unittest $(2) 2>/dev/null
	@$(COVERAGE) html -d $(HTML)
	@$(COVERAGE) report | tail -1 | $(TOTALCOV)
endef

TST = $(TESTS)/test_$(target).py
SRC = $(SOURCE)/$(target).py
$(foreach target,$(SRC_TGT),$(eval $(call TARGET_template,$(target),$(TST),$(SRC))))

$(eval $(call TARGET_template,environ,$(TESTS)/test_environ.py,$(TESTS)/environ.py))
$(eval $(call TARGET_template,tgs,$(TESTS)/test_tgs.py,$(TESTS)/tgs.py))
$(eval $(call TARGET_template,bot,$(TESTS)/test_bot.py,$(SOURCE)/*.py))

cov-all: cov-environ cov-tgs $(COV_TGT) cov-bot
.PHONY: cov-environ cov-tgs $(COV_TGT) cov-bot
