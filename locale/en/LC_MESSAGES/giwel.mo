Þ    :      ì  O   ¼      ø     ù  æ  ü  4   ã
  <     M   U      £  *   Ä  *   ï          )     2     :     R      f  :        Â     Ú  e   Ý     C     P  :   e  p      ,        >     X     n     {  #     P   £     ô               £     ¶     Ë     ä     ý  	     =   &      d  <     x   Â  :   ;  e   v  N   Ü  T   +  9     1   º  a   ì  %   N  *   t  @     /   à  f     d   w     Ü  0   ñ  u  "       æ    4     <   ·  M   ô      B  *   c  *        ¹     È     Ñ     Ù     ñ        :   &     a     y  e   |     â     ï  :     p   ?  ,   °     Ý     ÷              #      P   B            °      :!     B!     U!     j!     !     !  	   »!  =   Å!      "  <   $"  x   a"  :   Ú"  e   #  N   {#  T   Ê#  9   $  1   Y$  a   $  %   í$  *   %  @   >%  /   %  f   ¯%  d   &     {&  0   &                     	       -                       0      ,      '          %                2               6   4   1             :                               $                      7   +   *      
       )   #   3      (   &                8      "       5          /   9   .   !    ,  <b>GiWelBot: Give Welcome Bot</b>

I am a welcome bot and have basic anti-spam tools such as captcha resolution and keyword identification in names and texts.

You can configure (/config) the following parameters:

â¢ Welcome language, the captcha and the configuration menu will be displayed in the user's language if available.
â¢ Maximum number of spam penalties for banning a user.
â¢ Cite the administrators when someone writes @admin or @admins.

â¢ Welcome new users.
â¢ Delay of {GREETING_TIMER:s} in welcoming to allow cancellation if another user gives the greeting.

â¢ Show the list of administrators at the welcome.
â¢ Cite the administrators at the welcome.
â¢ Order of the list of administrators in the welcome.

You can also add a note (/set_greet_note <i>text</i>) to the welcome. Please note that the welcome text plus the note text and the administrator list text (if enabled) cannot exceed 4096 characters.

Welcome are grouped if there is no activity in the group. An accepted user will have to wait {TEMPORARY_RESTRICTION:s} to be able to share audios, documents, photos, videos, GIFs, stickers, web page previews and texts with URLs. 
If a captcha is answered incorrectly, one more captcha can be solved in private. A banned user must wait {BANNED_RESTRICTION:s} before they can try to re-enter unless an administrator manually unbanned them in the group (a telegram management function).

â ï¸ The bot must have permissions to delete messages and suspend users.

By @schcriher Activate the call to the administrators with @admin? Activate the welcome delay? (allows canceling the greeting). Choose the group to solve the captcha. You can stop the process with /cancel. Choose the language for the bot: Choose the limit of strikes for expulsion: Citing administrators on the welcome list? Correct answer Disabled Enabled Finished configuration. Get another captcha Giving a welcome to new members? In what order do I show the administrators at the welcome? List of administrators: No Please {user:s}, solve the following captcha (a simple math operation!):

{captcha:s}

The result is: Private chat Processing: {item:s} Show the list of administrators at the end of the welcome? The presence of the admin is required here: {mention:s} The presence of the admins is required here: {mention:s} Wait a few seconds to order another captcha. Welcome note established. Welcome note removed. Wrong answer Yes You don't have any captcha pending. You have groups with failing captchas, but you must wait to retry:
{chat_list:s} You want to solve a captcha? You will not be able to use the group, contact an administrator if you feel it is a mistake. You can try again in {BANNED_RESTRICTION:s}. english {:g} day {:g} days {:g} hour {:g} hours {:g} minute {:g} minutes {:g} second {:g} seconds {subject:s}
Current: {value:s} {} and {} Â¡We welcome you, {mention:s}! Â¡We welcome you, {mention:s}! â¢ {delta:s} for â{chat:s}â â ï¸ Operation cancelled, you can start again with /start. âï¸ An error has occurred trying to reply to you ð­ my developer will be notified ð sorry for the inconvenience. âï¸ An error occurred, restart the process with /start. âï¸ Error sending the welcome, check the configured note (may be too long or contain html errors). âï¸ This is a function for private use only (not in groups or supergroups). âï¸ This is a function to be used only in groups or supergroups (not in private). âï¸ Time to solve the captcha is over. {CAN_NOT_USE:s} âï¸ Wait a few seconds to show the help again. âï¸ Wrong Captcha {user:s}. In private with <code>/start</code> you can solve another captcha. âï¸ Wrong Captcha. {CAN_NOT_USE:s} âï¸ Wrong option, retry (use keyboard). âï¸ You are no longer a member of this group. {CAN_NOT_USE:s} âï¸ You have not started this configuration. âï¸ {user:s} I deleted his message because it contains spam [strike {strike:d} of {max_strikes:d}]. â Captcha correct {user:s}. Now you can send only text without URLs for {TEMPORARY_RESTRICTION:s}. â Captcha correct. ð¡ This is a function for administrators only. Project-Id-Version: giwel 2.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-01-30 02:32-0300
Last-Translator: Schmidt Cristian HernÃ¡n <schcriher@gmail.com>
Language-Team: English (https://gitlab.com/schcriher/giwelbot/issues)
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 ,  <b>GiWelBot: Give Welcome Bot</b>

I am a welcome bot and have basic anti-spam tools such as captcha resolution and keyword identification in names and texts.

You can configure (/config) the following parameters:

â¢ Welcome language, the captcha and the configuration menu will be displayed in the user's language if available.
â¢ Maximum number of spam penalties for banning a user.
â¢ Cite the administrators when someone writes @admin or @admins.

â¢ Welcome new users.
â¢ Delay of {GREETING_TIMER:s} in welcoming to allow cancellation if another user gives the greeting.

â¢ Show the list of administrators at the welcome.
â¢ Cite the administrators at the welcome.
â¢ Order of the list of administrators in the welcome.

You can also add a note (/set_greet_note <i>text</i>) to the welcome. Please note that the welcome text plus the note text and the administrator list text (if enabled) cannot exceed 4096 characters.

Welcome are grouped if there is no activity in the group. An accepted user will have to wait {TEMPORARY_RESTRICTION:s} to be able to share audios, documents, photos, videos, GIFs, stickers, web page previews and texts with URLs. 
If a captcha is answered incorrectly, one more captcha can be solved in private. A banned user must wait {BANNED_RESTRICTION:s} before they can try to re-enter unless an administrator manually unbanned them in the group (a telegram management function).

â ï¸ The bot must have permissions to delete messages and suspend users.

By @schcriher Activate the call to the administrators with @admin? Activate the welcome delay? (allows canceling the greeting). Choose the group to solve the captcha. You can stop the process with /cancel. Choose the language for the bot: Choose the limit of strikes for expulsion: Citing administrators on the welcome list? Correct answer Disabled Enabled Finished configuration. Get another captcha Giving a welcome to new members? In what order do I show the administrators at the welcome? List of administrators: No Please {user:s}, solve the following captcha (a simple math operation!):

{captcha:s}

The result is: Private chat Processing: {item:s} Show the list of administrators at the end of the welcome? The presence of the admin is required here: {mention:s} The presence of the admins is required here: {mention:s} Wait a few seconds to order another captcha. Welcome note established. Welcome note removed. Wrong answer Yes You don't have any captcha pending. You have groups with failing captchas, but you must wait to retry:
{chat_list:s} You want to solve a captcha? You will not be able to use the group, contact an administrator if you feel it is a mistake. You can try again in {BANNED_RESTRICTION:s}. english {:g} day {:g} days {:g} hour {:g} hours {:g} minute {:g} minutes {:g} second {:g} seconds {subject:s}
Current: {value:s} {} and {} Â¡We welcome you, {mention:s}! Â¡We welcome you, {mention:s}! â¢ {delta:s} for â{chat:s}â â ï¸ Operation cancelled, you can start again with /start. âï¸ An error has occurred trying to reply to you ð­ my developer will be notified ð sorry for the inconvenience. âï¸ An error occurred, restart the process with /start. âï¸ Error sending the welcome, check the configured note (may be too long or contain html errors). âï¸ This is a function for private use only (not in groups or supergroups). âï¸ This is a function to be used only in groups or supergroups (not in private). âï¸ Time to solve the captcha is over. {CAN_NOT_USE:s} âï¸ Wait a few seconds to show the help again. âï¸ Wrong Captcha {user:s}. In private with <code>/start</code> you can solve another captcha. âï¸ Wrong Captcha. {CAN_NOT_USE:s} âï¸ Wrong option, retry (use keyboard). âï¸ You are no longer a member of this group. {CAN_NOT_USE:s} âï¸ You have not started this configuration. âï¸ {user:s} I deleted his message because it contains spam [strike {strike:d} of {max_strikes:d}]. â Captcha correct {user:s}. Now you can send only text without URLs for {TEMPORARY_RESTRICTION:s}. â Captcha correct. ð¡ This is a function for administrators only. 