��    :      �  O   �      �     �  �  �  4   �
  <     M   U      �  *   �  *   �          )     2     :     R      f  :   �     �     �  e   �     C     P  :   e  p   �  ,        >     X     n     {  #     P   �     �  �        �     �     �     �     �     �  	     =   &      d  <   �  x   �  :   ;  e   v  N   �  T   +  9   �  1   �  a   �  %   N  *   t  @   �  /   �  f     d   w     �  0   �  u  "     �  �  �  8   �  D   �  N         O  1   l  ;   �     �     �     �            *   2  ?   ]     �     �  x   �     3      @   @   U   r   �   0   	!  "   :!      ]!     ~!     �!  $   �!  Y   �!     "  �   2"     �"     �"     �"     �"     #     !#     ?#  N   G#  !   �#  ?   �#  v   �#  E   o$  |   �$  ]   2%  ]   �%  D   �%  >   3&  e   r&  *   �&  6   '  6   :'  .   q'  ^   �'  i   �'     i(  4   (                     	       -                       0      ,      '          %                2               6   4   1             :                               $                      7   +   *      
       )   #   3      (   &                8      "       5          /   9   .   !    ,  <b>GiWelBot: Give Welcome Bot</b>

I am a welcome bot and have basic anti-spam tools such as captcha resolution and keyword identification in names and texts.

You can configure (/config) the following parameters:

• Welcome language, the captcha and the configuration menu will be displayed in the user's language if available.
• Maximum number of spam penalties for banning a user.
• Cite the administrators when someone writes @admin or @admins.

• Welcome new users.
• Delay of {GREETING_TIMER:s} in welcoming to allow cancellation if another user gives the greeting.

• Show the list of administrators at the welcome.
• Cite the administrators at the welcome.
• Order of the list of administrators in the welcome.

You can also add a note (/set_greet_note <i>text</i>) to the welcome. Please note that the welcome text plus the note text and the administrator list text (if enabled) cannot exceed 4096 characters.

Welcome are grouped if there is no activity in the group. An accepted user will have to wait {TEMPORARY_RESTRICTION:s} to be able to share audios, documents, photos, videos, GIFs, stickers, web page previews and texts with URLs. 
If a captcha is answered incorrectly, one more captcha can be solved in private. A banned user must wait {BANNED_RESTRICTION:s} before they can try to re-enter unless an administrator manually unbanned them in the group (a telegram management function).

⚠️ The bot must have permissions to delete messages and suspend users.

By @schcriher Activate the call to the administrators with @admin? Activate the welcome delay? (allows canceling the greeting). Choose the group to solve the captcha. You can stop the process with /cancel. Choose the language for the bot: Choose the limit of strikes for expulsion: Citing administrators on the welcome list? Correct answer Disabled Enabled Finished configuration. Get another captcha Giving a welcome to new members? In what order do I show the administrators at the welcome? List of administrators: No Please {user:s}, solve the following captcha (a simple math operation!):

{captcha:s}

The result is: Private chat Processing: {item:s} Show the list of administrators at the end of the welcome? The presence of the admin is required here: {mention:s} The presence of the admins is required here: {mention:s} Wait a few seconds to order another captcha. Welcome note established. Welcome note removed. Wrong answer Yes You don't have any captcha pending. You have groups with failing captchas, but you must wait to retry:
{chat_list:s} You want to solve a captcha? You will not be able to use the group, contact an administrator if you feel it is a mistake. You can try again in {BANNED_RESTRICTION:s}. english {:g} day {:g} days {:g} hour {:g} hours {:g} minute {:g} minutes {:g} second {:g} seconds {subject:s}
Current: {value:s} {} and {} ¡We welcome you, {mention:s}! ¡We welcome you, {mention:s}! • {delta:s} for “{chat:s}” ⚠️ Operation cancelled, you can start again with /start. ⛔️ An error has occurred trying to reply to you 😭 my developer will be notified 😊 sorry for the inconvenience. ⛔️ An error occurred, restart the process with /start. ⛔️ Error sending the welcome, check the configured note (may be too long or contain html errors). ⛔️ This is a function for private use only (not in groups or supergroups). ⛔️ This is a function to be used only in groups or supergroups (not in private). ⛔️ Time to solve the captcha is over. {CAN_NOT_USE:s} ⛔️ Wait a few seconds to show the help again. ⛔️ Wrong Captcha {user:s}. In private with <code>/start</code> you can solve another captcha. ⛔️ Wrong Captcha. {CAN_NOT_USE:s} ⛔️ Wrong option, retry (use keyboard). ⛔️ You are no longer a member of this group. {CAN_NOT_USE:s} ⛔️ You have not started this configuration. ⛔️ {user:s} I deleted his message because it contains spam [strike {strike:d} of {max_strikes:d}]. ✅ Captcha correct {user:s}. Now you can send only text without URLs for {TEMPORARY_RESTRICTION:s}. ✅ Captcha correct. 🛡 This is a function for administrators only. Project-Id-Version: giwel 2.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-01-30 02:34-0300
Last-Translator: Schmidt Cristian Hernán <schcriher@gmail.com>
Language-Team: Spanish (https://gitlab.com/schcriher/giwelbot/issues)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 ,  <b>GiWelBot: Give Welcome Bot</b>

Soy un bot para dar la bienvenida y cuento con medidas básicas contra spam como ser resolución de captchas e identificación de palabras claves en nombres y textos.

Permito configurar (/config) los siguientes parámetros:

• Idioma de las bienvenidas, el captcha y el menú de configuración serán mostrados en el idioma del usuario si está disponible.
• Número máximo de penalizaciones por spam para banear a un usuario.
• Citar a los administradores cuando se escribe @admin o @admins.

• Dar la bienvenida a los nuevos usuarios.
• Retardo de {GREETING_TIMER:s} al dar la bienvenida para permitir cancelarla si otro usuario da el saludo.

• Mostrar la lista de los administradores en la bienvenida.
• Citar a los administradores en la bienvenida.
• Orden de la lista de los administradores en la bienvenida.

Permito además agregar una nota (/set_greet_note <i>texto</i>) a la bienvenida. Tenga en cuenta que el texto de la bienvenida mas el texto de la nota y el texto de la lista de administradores (si está activado) no pueden superar los 4096 caracteres.

Las bienvenidas son agrupadas si no hay actividad en el grupo. Un usuario aceptado tendrá que esperar {TEMPORARY_RESTRICTION:s} para poder compartir audios, documentos, fotos, videos, GIFs, stickers, previsualizaciones de paginas web y textos con URLs.

Si se responde incorrectamente un captcha se podrá resolver uno mas por privado. Un usuario baneado deberá esperar {BANNED_RESTRICTION:s} antes de poder volver a intentar ingresar a menos que un administrador lo desbanee manualmente en el grupo (una función de administración de telegram).

⚠️ El bot debe tener permisos para eliminar mensajes y suspender usuarios.

Por @schcriher ¿Activar las llamadas a los administradores con @admin? ¿Activar el retardo de la bienvenida? (permite cancelar el saludo). Elija el grupo para resolver el captcha. Puede detener el proceso con /cancel. Elija el idioma para el bot: Elija el límite de strikes para las expulsiones: ¿Citar a los administradores en la lista de la bienvenida? Respuesta correcta Desactivado Activado Configuración finalizada. Obtener otro captcha ¿Dar la bienvenida a los nuevos miembros? ¿En qué orden muestro a los administradores en la bienvenida? Lista de administradores: No Por favor {user:s}, resuelve el siguiente captcha (¡una simple operación matemática!):

{captcha:s}

El resultado es: Chat privado Procesando: {item:s} ¿Mostrar la lista de administradores al final de la bienvenida? La presencia del admin es requerida aquí: {mention:s} La presencia de los admines es requerida aquí: {mention:s} Espere algunos segundos para pedir otro captcha. Nota de la bienvenida establecida. Nota de la bienvenida eliminada. Respuesta incorrecta Si No tienes pendiente ningún captcha. Tiene grupos con captchas mal resueltos, pero debe esperar para reintentar:
{chat_list:s} ¿Desea resolver un captcha? No podrá usar el grupo, contacte con un administrador si considera que es un error. Puede volver a intentar en {BANNED_RESTRICTION:s}. español {:g} día {:g} días {:g} hora {:g} horas {:g} minuto {:g} minutos {:g} segundo {:g} segundos {subject:s}
Actual: {value:s} {} y {} ¡Te damos la bienvenida, {mention:s}! ¡Les damos la bienvenida, {mention:s}! • {delta:c} para “{chat:c}” ⚠️ Operación cancelada, puede volver a empezar con /start. ⛔️ Ha ocurrido un error intentando responderle 😭 mi desarrollador será notificado 😊 disculpe las molestias. ⛔️ Ocurrió algún error, vuelva a iniciar el proceso con /start. ⛔️ Error enviando la bienvenida, verificar la nota configurada (puede que sea muy largo o que contenga errores de html). ⛔️ Esta es una función para ser usada solamente en privado (no en grupos o supergrupos). ⛔️ Esta es una función para ser usada solamente en grupos o supergrupos (no en privado). ⛔️ Se acabó el tiempo para resolver el captcha. {CAN_NOT_USE:s} ⛔️ Espere algunos segundos para mostrar la ayuda otra vez. ⛔️ Captcha incorrecto {user:s}. Por privado con <code>/start</code> puedes resolver otro captcha. ⛔️ Captcha incorrecto. {CAN_NOT_USE:s} ⛔️ Opción incorrecta, reintente (use el teclado). ⛔️ Ya no es miembro de este grupo. {CAN_NOT_USE:s} ⛔️ No has iniciado tu esta configuración. ⛔️ {user:s} borré su mensaje porque contiene spam [strike {strike:d} de {max_strikes:d}]. ✅ Captcha correcto {user:s}. Ahora podrá enviar solo texto sin URLs durante {TEMPORARY_RESTRICTION:s}. ✅ Captcha correcto. 🛡 Esta es una función solo para administradores. 